# MyLittleHome

Przygotował Dawid Musialik.

Celem stworzenia tej aplikacji było napisanie pierwszej aplikacji w Xamarin.Forms (Android), nauka języka C# i Xamarin'a. W kodzie aplikacji występują dwa języki Polski i Angielski - jest to błąd, którego nie naprawiałem aby nie tracić czasu w tym projekcie.

Założenia: 
* Zarządzanie zbiorami książek, muzyki, filmów. 
* Zarządzanie budżetem. 
* Pobieranie podstawowych danych z API
	- GoodReads 
	- LastFM 
	- TMDB 