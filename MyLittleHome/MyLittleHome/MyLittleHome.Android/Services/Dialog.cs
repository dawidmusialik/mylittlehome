﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(MyLittleHome.Droid.Services.Dialog))]
namespace MyLittleHome.Droid.Services
{
    class Dialog : MyLittleHome.Interface.IDialog
    {
        public void ShowDialog(string message)
        {
            MainActivity.ShowToast(message);
        }
    }
    
}