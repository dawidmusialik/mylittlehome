﻿using System;
using System.Collections.Generic;
using System.Text;
using TMDbLib.Client;

namespace MyLittleHome.Api
{
    public sealed class ApiKeySingleton
    {

        private static ApiKeySingleton _Instance = null;
        private static readonly object PadLock = new object();
        private string GoodReadsApiKey;
        private string LastFmApiKey;
        private string TMDbApiKey;

        private ApiKeySingleton()
        {
            GoodReadsApiKey = "";
            LastFmApiKey = "";
            TMDbApiKey = "";


        }

        public static ApiKeySingleton Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new ApiKeySingleton();
                    }
                    return _Instance;
                }
            }
        }

        public string getGoodReadsApiKey()
        {
            return GoodReadsApiKey;
        }
        public string getLastFmApiKey()
        {
            return LastFmApiKey;
        }
        public string getTMDbApiKey()
        {
            return TMDbApiKey;
        }
    }
}

