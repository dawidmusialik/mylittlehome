﻿using System;
using System.Collections.Generic;
using System.Text;
using TMDbLib.Client;

namespace MyLittleHome.Api
{
    public sealed class GoodReadsKeySingleton
    {

        private static GoodReadsKeySingleton _Instance = null;
        private static readonly object PadLock = new object();
        private string apiKey;

        private GoodReadsKeySingleton()
        {
            apiKey = "G4cTFRTXf6AqtMcIfwfXg";
        }

        public static GoodReadsKeySingleton Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new GoodReadsKeySingleton();
                    }
                    return _Instance;
                }
            }
        }

        public string getApiKey()
        {
            return apiKey;
        }

    }
}

