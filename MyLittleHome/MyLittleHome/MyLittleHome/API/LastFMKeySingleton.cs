﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.Api
{
   public class LastFMKeySingleton
    {
        private static LastFMKeySingleton _Instance = null;
        private static readonly object PadLock = new object();
        private string apiKey;

        private LastFMKeySingleton()
        {
            apiKey = "a8a72d07690df754cb62522450e5a449";
        }

        public static LastFMKeySingleton Instance
        {
            get
            {
                lock (PadLock)
                {
                    if (_Instance == null)
                    {
                        _Instance = new LastFMKeySingleton();
                    }
                    return _Instance;
                }
            }
        }

        public string getApiKey()
        {
            return apiKey;
        }
    }
}
