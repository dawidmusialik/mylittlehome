﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.Component.Music
{
    public sealed class MusicCustomVisual {
        private static MusicCustomVisual _Instance = null;
        private static readonly object PadLock = new object();
        //Books View
        private Color _colorDescription;
        private Color _colorViewCellBackground;
        private Color _topPanel;
        private Color _colorBackground;
        private Color _colorBar;

        private MusicCustomVisual() {
            _colorDescription = Color.FromHex("CCCCCC");
            _colorViewCellBackground = Color.FromHex("4EAEE6");
            _colorBackground = Color.FromHex("4EAEE6");
            _topPanel = Color.FromHex("343635");
            _colorBar = Color.FromHex("343635");
        }

        public static MusicCustomVisual Instance {
            get {
                lock (PadLock) {
                    if (_Instance == null) {
                        _Instance = new MusicCustomVisual();
                    }
                    return _Instance;
                }
            }
        }

        public Color getDescriptionColor() {
            return _colorDescription;
        }

        public Color getViewCellBackgroundColor() {
            return _colorViewCellBackground;
        }

        public Color getTopPanelColor() {
            return _topPanel;
        }
        public Color getBackgroundColor() {
            return _colorBackground;
        }
        public Color getBarColor() {
            return _colorBar;
        }

    }

    public class MusicCustomButton : Image {

        public MusicCustomButton() {

        }

        public MusicCustomButton(string path) {
            this.Source = path;
        }

        public void setSource(string path) {
            this.Source = path;
        }

        public void Clicked(TapGestureRecognizer tap) {
            this.GestureRecognizers.Add(tap);
        }


    }

}
