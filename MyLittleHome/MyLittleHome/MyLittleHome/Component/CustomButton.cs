﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.Component
{
    public class CustomButton:Image
    {
        public CustomButton()
        {

        }

        public CustomButton(string path)
        {
            this.Source = path;
        }

        public void setSource(string path)
        {
            this.Source = path;
        }

        public void Clicked(TapGestureRecognizer tap)
        {
            this.GestureRecognizers.Add(tap);
        }
    }
}
