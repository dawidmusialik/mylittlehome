﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.Component
{
   public class CustomNavigationPage :NavigationPage
    {
        public CustomNavigationPage(Page view):base(view)
        {
            this.BarBackgroundColor = Color.FromHex("343635");
        }
       
    }
}
