﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.Interface
{
    public interface IDialog
    {
        void ShowDialog(string message);
    }
}
