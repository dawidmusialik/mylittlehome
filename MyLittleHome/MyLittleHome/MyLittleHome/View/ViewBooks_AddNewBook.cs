﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.View
{
	public class ViewBooks_AddNewBook : ContentPage
	{
        ViewHelp.ViewBooks_AddNewBook_HelpView ViewAddNewBook = new ViewHelp.ViewBooks_AddNewBook_HelpView();
       // private StackLayout Stack;
        ContentPage test = new ContentPage();
        public ViewBooks_AddNewBook ()
		{
           // Stack = new StackLayout();

            Title = " Add new books";

            //   Stack.Children.Add(ViewAddNewBook.ViewBooks_AddNewBook());


            //test = ViewAddNewBook.ViewBooks_AddNewBook();
            ScrollView SV = new ScrollView { Content = ViewAddNewBook.ViewBooks_AddNewBook() };

            this.Content = SV;

		}
	}
}
