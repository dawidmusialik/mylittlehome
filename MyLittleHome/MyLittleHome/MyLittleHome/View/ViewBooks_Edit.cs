﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.View
{
	public class ViewBooks_Edit : ContentPage
	{
        private StackLayout BooksViewEdit;
        ViewHelp.ViewBooks_Edit_HelpView BooksEditView;

        public ViewBooks_Edit(int booksid)
        {
            Title = "Books Edit";
            BooksViewEdit = new StackLayout();
            BooksEditView = new ViewHelp.ViewBooks_Edit_HelpView(booksid);

         


            BooksViewEdit.Children.Add(BooksEditView.Listaview());

            this.Content = BooksViewEdit;
        }
    }
}
