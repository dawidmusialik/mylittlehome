﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

//Folders
using MyLittleHome.Helps;

namespace MyLittleHome.View
{
	public class ViewMasterDetailMenu : ContentPage
	{
        HelpMasterDetailMenu_v2 HomeScreen;       //Nowy obiekt klasy dla widoku HomeScreen
        HelpMasterDetailMenu_v2 MovieScreen;      //Nowy obiekt klasy dla widoku Movie
        HelpMasterDetailMenu_v2 BooksScreen;      //Nowy obiekt klasy dla widoku Books
        HelpMasterDetailMenu_v2 MusicScreen;      //Nowy obiekt klasy dla widoku Music
        HelpMasterDetailMenu_v2 BudgetScreen;     //Nowy obiekt klasy dla widoku Budget

        public ViewMasterDetailMenu()
        {
            HomeScreen = new HelpMasterDetailMenu_v2("Home Screen", new ViewHomeScreen(), new Image() {Source = "iconHome.png" });
            MovieScreen = new HelpMasterDetailMenu_v2("Movie", new View.Movies.ViewMovies(), new Image() { Source = "iconMovie.png" });
            BooksScreen = new HelpMasterDetailMenu_v2("Books", new View.Books.ViewBooks(), new Image() { Source = "iconBooks.png" });
            MusicScreen = new HelpMasterDetailMenu_v2("Music", new View.Music.ViewMusic(), new Image() { Source = "iconMusic.png" });
            BudgetScreen = new HelpMasterDetailMenu_v2("Budget", new View.Budget.ViewBudget(), new Image() { Source = "iconBudget.png" });
            
            Title = "MenuMenu";
            IsEnabled = true;
            Content = new StackLayout
            {
                Padding = 50,
                Spacing = 20,
                BackgroundColor = Color.FromHex("ABABAB"),
                Children = {
                    new Label { Text = "Main Menu" },
                    HomeScreen.getGrid(),
                    MovieScreen.getGrid(),
                    BooksScreen.getGrid(),
                    MusicScreen.getGrid(),
                    BudgetScreen.getGrid(),
                }
            };


        }
    }
}
