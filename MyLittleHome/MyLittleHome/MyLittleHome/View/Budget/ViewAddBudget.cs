﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.View.Budget
{
   public class ViewAddBudget:ContentPage
    {
        ViewHelp.ViewHelp_Budget.AddBudget.HelpView helpView;
        public ViewAddBudget()
        {
            helpView = new ViewHelp.ViewHelp_Budget.AddBudget.HelpView();
            this.Content = helpView;
            this.Title = "Add new budget";
        }
    }
}
