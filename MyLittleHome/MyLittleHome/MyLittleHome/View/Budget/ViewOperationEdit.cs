﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.View.Budget
{
    public class ViewOperationEdit:ContentPage
    {
        ViewHelp.ViewHelp_Budget.EditOperation.HelpView viewHelp;
        public ViewOperationEdit(DataBase.Budget.TableBudgetOperation currentOperation)
        {
            viewHelp = new ViewHelp.ViewHelp_Budget.EditOperation.HelpView(currentOperation);

            this.Title = "Edit operation";
            this.Content = viewHelp;
        }
    }
}
