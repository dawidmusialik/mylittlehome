﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.View.Budget
{
    public class ViewCurrentBudget : ContentPage
    {
        ViewHelp.ViewHelp_Budget.CurrentBudget.HelpTopPanelElements topPanelElements;
        ViewHelp.ViewHelp_Budget.CurrentBudget.HelpBottomPanelElements bottomPanelElements;
        public static int _topPanelTapped = 0;
        TapGestureRecognizer openViewEditBudget;

        StackLayout viewStack;
        ListView viewList;
        DataBase.TableBudget tableBudget;
        List<DataBase.Budget.TableBudgetOperation> tableOperations;

        private System.Threading.CancellationTokenSource ctsButtonDelete;
        private int secondRefreshButtonDelete = 0;
        private System.Threading.CancellationTokenSource ctsButtonSend;
        private int secondRefreshButtonSend = 0;

        public ViewCurrentBudget(DataBase.TableBudget directBudget)
        {
            topPanelElements = new ViewHelp.ViewHelp_Budget.CurrentBudget.HelpTopPanelElements();
            bottomPanelElements = new ViewHelp.ViewHelp_Budget.CurrentBudget.HelpBottomPanelElements();

            viewStack = new StackLayout();
            tableBudget = new DataBase.TableBudget();
            tableBudget = directBudget;
            tableOperations = new List<DataBase.Budget.TableBudgetOperation>();
            tableOperations = App.MyLittleHomeDB_Budget.getOperation(tableBudget.budgetID);
            viewStack.BackgroundColor = Color.FromHex("ABABAB");

            viewStack.Children.Add(viewTopPanel());

            createListView();
            viewStack.Children.Add(viewList);

            viewStack.Children.Add(viewBottomPanel());

            this.Content = viewStack;

        }
       
        protected override void OnAppearing()
        {
            base.OnAppearing();

            tableBudget = App.MyLittleHomeDB_Budget.getBudget(tableBudget.budgetID);
            tableOperations = App.MyLittleHomeDB_Budget.getOperation(tableBudget.budgetID);

            this.Title = tableBudget.budgetName; ;
            topPanelElements.labelBudgetDescription.Text = tableBudget.budgetDescription;
            topPanelElements.labelBudgetStatus.Text = tableBudget.budgetStatus.ToString();
            topPanelElements.labelBudgetOperations.Text = tableOperations.Count.ToString();
            /*
            bottomPanelElements.sendOperations.Clicked += (object sender, EventArgs e) =>
            {
                if(bottomPanelElements.entryValue.Text != null && bottomPanelElements.entryDescription.Text != null)
                {
                    App.MyLittleHomeDB_Budget.insertNewOperation(tableBudget.budgetID, float.Parse(bottomPanelElements.entryValue.Text), bottomPanelElements.entryDescription.Text);
                    tableBudget = App.MyLittleHomeDB_Budget.getBudget(tableBudget.budgetID);
                    tableOperations = App.MyLittleHomeDB_Budget.getOperation(tableBudget.budgetID);

                    topPanelElements.labelBudgetStatus.Text = tableBudget.budgetStatus.ToString();

                     var v = viewList.ItemsSource.Cast<object>().FirstOrDefault();
                    if(v != null)
                    {
                        viewList.ScrollTo(v, ScrollToPosition.Start, false);
                    }
                    
                    bottomPanelElements.entryValue.Text = null;
                    bottomPanelElements.entryDescription.Text = null;
                    App.LocalNotification("New operation added");
                }
            };
            */
            bottomPanelElements.sendOperations.Clicked(ButtonSendTapGesture());

        }

        private TapGestureRecognizer ButtonSendTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (bottomPanelElements.entryValue.Text != null && bottomPanelElements.entryDescription.Text != null)
                {
                    App.MyLittleHomeDB_Budget.insertNewOperation(tableBudget.budgetID, float.Parse(bottomPanelElements.entryValue.Text), bottomPanelElements.entryDescription.Text);
                    tableBudget = App.MyLittleHomeDB_Budget.getBudget(tableBudget.budgetID);
                    tableOperations = App.MyLittleHomeDB_Budget.getOperation(tableBudget.budgetID);

                    topPanelElements.labelBudgetStatus.Text = tableBudget.budgetStatus.ToString();

                    var v = viewList.ItemsSource.Cast<object>().FirstOrDefault();
                    if (v != null)
                    {
                        viewList.ScrollTo(v, ScrollToPosition.Start, false);
                    }

                    bottomPanelElements.entryValue.Text = null;
                    bottomPanelElements.entryDescription.Text = null;
                    App.LocalNotification("New operation added and 2 second to refresh");
                    StartUpdateButtonSend();
                }
            };
            
            return _tap;
        }

        private Grid viewTopPanel()
        {
            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },


                },
                ColumnDefinitions =
                {
                   new ColumnDefinition { Width = GridLength.Auto },
                   new ColumnDefinition { Width = GridLength.Star},
                   new ColumnDefinition { Width = GridLength.Auto},
                   new ColumnDefinition { Width = GridLength.Star},
                   new ColumnDefinition { Width = GridLength.Auto},




                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
               // VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };
            topPanelElements.buttonDeleteBudget.Clicked(ButtonDeleteTapGesture());
            topPanelElements.buttonDeleteBudget.IsVisible = false;
           /*
            topPanelElements.buttonDeleteBudget.Clicked += (sender, e) =>
            {
                App.MyLittleHomeDB_Budget.deleteBudget(tableBudget.budgetID);
                App.LocalNotification("Budget deleted");
                App.MasterDetailPage.Detail.Navigation.PopAsync();
            };
            */
            grid.Children.Add(topPanelElements.labelOperations, 0, 1, 0, 1);
            grid.Children.Add(topPanelElements.labelBudgetOperations, 1, 2, 0, 1);

            grid.Children.Add(topPanelElements.labelStatus, 2, 3, 0, 1);
           grid.Children.Add(topPanelElements.labelBudgetStatus, 3,4, 0, 1);

            grid.Children.Add(topPanelElements.buttonDeleteBudget, 4, 5, 0, 2);

            grid.Children.Add(topPanelElements.labelDescription, 0, 1, 1,2);
            grid.Children.Add(topPanelElements.labelBudgetDescription, 1, 5, 1, 2);


            openViewEditBudget = new TapGestureRecognizer();
            openViewEditBudget.Tapped += (sender, e) =>
              {
                  StartUpdateButtonDelete();
                  topPanelElements.buttonDeleteBudget.IsVisible = true;
                  _topPanelTapped++;
                  if(_topPanelTapped == 1)
                  {
                      App.LocalNotification("Double tap to edit budget");
                  }
                  if(_topPanelTapped == 2)
                  {
                      App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Budget.ViewBudgetEdit(tableBudget));
                  }

              };

            grid.GestureRecognizers.Add(openViewEditBudget);

            return grid;
        }

        private TapGestureRecognizer ButtonDeleteTapGesture()
        {
                TapGestureRecognizer _tap = new TapGestureRecognizer();
                _tap.Tapped += (sender, e) => {
                    App.MyLittleHomeDB_Budget.deleteBudget(tableBudget.budgetID);
                    App.LocalNotification("Budget deleted");
                    App.MasterDetailPage.Detail.Navigation.PopAsync();
                };
                return _tap;
        }
#region Odświeżania przycisku kasowania budżetu
        public void StartUpdateButtonDelete()
        {
            if (ctsButtonDelete != null) ctsButtonDelete.Cancel();
            ctsButtonDelete = new System.Threading.CancellationTokenSource();
            var ignore = UpdateAsyncButtonDelete(ctsButtonDelete);
        }

        public void StopUpdateButtonDelete()
        {
            if (ctsButtonDelete != null) ctsButtonDelete.Cancel();
            ctsButtonDelete = null;
            secondRefreshButtonDelete = 0;
            topPanelElements.buttonDeleteBudget.IsVisible = false;
            _topPanelTapped = 0;
        }

        public async System.Threading.Tasks.Task UpdateAsyncButtonDelete(System.Threading.CancellationTokenSource ct)
        {
            while (!ct.IsCancellationRequested)
            {
                if (secondRefreshButtonDelete >= 5)
                {
                    StopUpdateButtonDelete();
                }
                else
                {
                    secondRefreshButtonDelete++;
                }

                await System.Threading.Tasks.Task.Delay(1000);
            }
        }
        #endregion
#region Odświeżanie listy dwie sekundy po dodaniu nowego rekordu do budżetu
        public void StartUpdateButtonSend()
        {
            if (ctsButtonSend != null) ctsButtonSend.Cancel();
            ctsButtonSend = new System.Threading.CancellationTokenSource();
            var ignore = UpdateAsyncButtonSend(ctsButtonSend);
        }

        public void StopUpdateButtonSend()
        {
            if (ctsButtonSend != null) ctsButtonSend.Cancel();
            ctsButtonSend = null;
            secondRefreshButtonSend = 0;

            //
            //App.LocalNotification("2 second to refresh");
            refreshList();
        }

        public async System.Threading.Tasks.Task UpdateAsyncButtonSend(System.Threading.CancellationTokenSource ct)
        {
            while (!ct.IsCancellationRequested)
            {
                if (secondRefreshButtonSend >= 2)
                {
                    StopUpdateButtonSend();
                }
                else
                {
                    secondRefreshButtonSend++;
                }

                await System.Threading.Tasks.Task.Delay(1000);
            }
        }
        #endregion
        private void createListView()
        {
            viewList = new ListView();
            viewList.ItemsSource = tableOperations;
            viewList.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Budget.CurrentBudget.HelpListView));
            viewList.IsPullToRefreshEnabled = true;
            viewList.HasUnevenRows = true;
            viewList.RowHeight = -1;
            viewList.Refreshing += (sender, e1) => { refreshList(); };
            viewList.ItemTapped += viewListItemTapped;

            ViewHelp.ViewHelp_Budget.CurrentBudget.HelpListViewElements.operationNumber= 0;
        }

        private void viewListItemTapped(object sender, ItemTappedEventArgs e)
        {
           
            App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Budget.ViewOperationEdit(((DataBase.Budget.TableBudgetOperation)e.Item)));

        }

        private void refreshList()
        {
            ViewHelp.ViewHelp_Budget.CurrentBudget.HelpListViewElements.operationNumber = 0;
            viewList.ItemsSource = tableOperations;
            viewList.EndRefresh();
        }


        private Grid viewBottomPanel()
        {
            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },

                },
                ColumnDefinitions =
                {
                   new ColumnDefinition { Width = GridLength.Auto },
                   new ColumnDefinition { Width = GridLength.Star},
                  new ColumnDefinition { Width = GridLength.Auto}

                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };

            grid.Children.Add(bottomPanelElements.labelEntryName, 0, 1, 0, 1);
            grid.Children.Add(bottomPanelElements.entryValue, 1, 2, 0, 1);

            grid.Children.Add(bottomPanelElements.labelEditorName, 0, 1, 1, 2);
            grid.Children.Add(bottomPanelElements.entryDescription, 1, 2, 1, 2);

            grid.Children.Add(bottomPanelElements.sendOperations, 2, 3, 0, 2);
           
            return grid;
        }
    }

}
