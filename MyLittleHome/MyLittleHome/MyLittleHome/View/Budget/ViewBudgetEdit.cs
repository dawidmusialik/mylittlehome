﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.View.Budget
{
    public class ViewBudgetEdit:ContentPage
    {
        ViewHelp.ViewHelp_Budget.EditBudget.HelpView viewEditBudget;
        public ViewBudgetEdit(DataBase.TableBudget _senderTableBudget)
        {
            viewEditBudget = new ViewHelp.ViewHelp_Budget.EditBudget.HelpView(_senderTableBudget);

            ViewCurrentBudget._topPanelTapped = 0;
            

            

            this.Content = viewEditBudget;
            this.Title = "Edit budget";

        }
    }
}
