﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.View.Budget
{
   public class ViewBudget:ContentPage
    {
        ListView viewList;
        StackLayout viewStack;
        ViewHelp.ViewHelp_Budget.Budget.HelpTopPanelElements topPanelElements;
        
        public ViewBudget()
        {
            Title = "Budget";
            viewList = new ListView();
            viewStack = new StackLayout();
            viewStack.BackgroundColor = Color.FromHex("ABABAB");

            createListView();
            viewStack.Children.Add(GridTittleAndButton());
            viewStack.Children.Add(viewList);
            this.Content = viewStack;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            refreshList();
        }
        private void createListView()
        {
            viewList.ItemsSource = App.MyLittleHomeDB_Budget.getBudgetList();
            viewList.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Budget.Budget.HelpView));
            viewList.IsPullToRefreshEnabled = true;

            viewList.HasUnevenRows = true;
            viewList.RowHeight = -1;
            viewList.Refreshing += (sender, e1) => { refreshList(); }; 
            viewList.ItemTapped += viewListItemTapped;

            ViewHelp.ViewHelp_Budget.Budget.HelpViewElements.budgetNumber = 0;
        }

        private void viewListItemTapped(object sender, ItemTappedEventArgs e)
        {
            var test = e;
            App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Budget.ViewCurrentBudget(((DataBase.TableBudget) e.Item)));

        }

        private void refreshList()
        {
            ViewHelp.ViewHelp_Budget.Budget.HelpViewElements.budgetNumber = 0;
            viewList.ItemsSource = App.MyLittleHomeDB_Budget.getBudgetList();
            viewList.EndRefresh();
        }

        private Grid GridTittleAndButton()
        {
            topPanelElements = new ViewHelp.ViewHelp_Budget.Budget.HelpTopPanelElements();
            Grid Gridview = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 35 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,

            };
            Gridview.Children.Add(topPanelElements.ButtonAddBudget, 0, 2, 0, 1);
            Gridview.Children.Add(topPanelElements.TitlePage, 2, 5, 0, 1);
            Gridview.Children.Add(new BoxView(), 5, 7, 0, 1);

            return Gridview;
        }
    }
}
