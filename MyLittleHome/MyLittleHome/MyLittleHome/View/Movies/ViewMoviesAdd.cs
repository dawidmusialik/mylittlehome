﻿using MyLittleHome.Component;
using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.View.Movies
{
    public class ViewMoviesAdd : TabbedPage
    {
        ViewHelp.ViewHelp_Movies.AddMovies.HelpView ViewAddNewMovies = new ViewHelp.ViewHelp_Movies.AddMovies.HelpView();
        ViewHelp.ViewHelp_Movies.FormatMovies.HelpView ViewFormatMovies = new ViewHelp.ViewHelp_Movies.FormatMovies.HelpView();
        ViewHelp.ViewHelp_Movies.LanguageMovies.HelpView ViewLanguageMovies = new ViewHelp.ViewHelp_Movies.LanguageMovies.HelpView();
        ViewHelp.ViewHelp_Movies.QualityMovies.HelpView ViewQualityMovies = new ViewHelp.ViewHelp_Movies.QualityMovies.HelpView();
        ViewHelp.ViewHelp_Movies.SubbitlesMovies.HelpView ViewSubbitlesMovies = new ViewHelp.ViewHelp_Movies.SubbitlesMovies.HelpView();
        public ViewMoviesAdd()
        {

            this.Title = "Add";
            this.Children.Add(ViewAddNewMovies);
            this.Children.Add(ViewFormatMovies);
            this.Children.Add(ViewLanguageMovies);
            this.Children.Add(ViewQualityMovies);
            this.Children.Add(ViewSubbitlesMovies);
          //  this.BackgroundColor = Color.FromHex("ABABAB");
          //  this.BarBackgroundColor = Color.FromHex("ABABAB");
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();
            this.BarBackgroundColor = CustomVisual.Instance.getBarColor();
        }
    }
}