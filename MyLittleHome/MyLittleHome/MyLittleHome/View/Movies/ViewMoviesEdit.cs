﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.View.Movies
{
    public class ViewMoviesEdit : TabbedPage
    {
        ViewHelp.ViewHelp_Movies.LoanMovies.HelpView LoanView;
        ViewHelp.ViewHelp_Movies.EditMovies.HelpView EditView;
        public ViewMoviesEdit(int moviesid)
        {
              // LoanView = new ViewHelp.ViewHelp_Movies.LoanMovies.HelpView();

            var ObjectOneMovies = App.MyLittleHomeDB_Movies.GetMoviesById(moviesid);
            if (ObjectOneMovies.Count >= 0)
            {
                EditView = new ViewHelp.ViewHelp_Movies.EditMovies.HelpView(ObjectOneMovies[0]);

                if (ObjectOneMovies[0].Loan == true)
                {
                    this.Title = "Edit";
                    LoanView = new ViewHelp.ViewHelp_Movies.LoanMovies.HelpView(ObjectOneMovies[0]);

                    this.Children.Add(EditView);
                    this.Children.Add(LoanView);
                }
                else
                {
                    this.Title = "Edit";
                    this.Children.Add(EditView);
                }

            }
            else
            {
                this.Children.Add(new ContentPage { Content = new StackLayout { Children = { new Label { Text = "Wystąpił błąd podczas ładowania książki" } } } });

            }

            this.BackgroundColor = Color.FromHex("ABABAB");
            this.BarBackgroundColor = Color.FromHex("ABABAB");
        }
    }
}
