﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using MyLittleHome.Component;


namespace MyLittleHome.View.Movies
{
    public class ViewMovies : ContentPage
    {
        private ListView ListaView;
        ViewHelp.ViewHelp_Movies.Movies.HelpTopPanelElements ElementsTopPanel;
        private System.Threading.CancellationTokenSource cts;
        private int secondRefresh = 0;


        public ViewMovies()
        {
            ListaView = new ListView();
            ElementsTopPanel = new ViewHelp.ViewHelp_Movies.Movies.HelpTopPanelElements();

            Title = "Movies";
            StackLayout MoviesView = new StackLayout();

          //  MoviesView.BackgroundColor = Color.FromHex("ABABAB");
            MoviesView.VerticalOptions = LayoutOptions.FillAndExpand;

            MoviesView.Children.Add(GridTittleAndButton());
            MoviesView.Children.Add(List());

            this.Content = MoviesView;
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();

        }

        private Grid GridTittleAndButton()
        {

            Grid Gridview = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 35 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,

            };
            ElementsTopPanel.ButtonEditMovie.IsVisible = false;
            Gridview.Children.Add(ElementsTopPanel.ButtonAddMovie, 0, 2, 0, 1);
            Gridview.Children.Add(ElementsTopPanel.TitlePage, 2, 5, 0, 1);
            Gridview.Children.Add(ElementsTopPanel.ButtonEditMovie, 5, 7, 0, 1);

            return Gridview;
        }

        //Główny widok listy
        public ListView List()
        {

            ListaView.ItemsSource = App.MyLittleHomeDB_Movies.GetMovies();
            ListaView.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Movies.Movies.HelpListView));
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            // ListaView.RowHeight = 265;
            ListaView.HasUnevenRows = true;
            ListaView.RowHeight = -1;

            ListaView.ItemTapped += (object sender, ItemTappedEventArgs e) => {
                StartUpdate();

                ElementsTopPanel.ButtonEditMovie.IsVisible = true;
              //  ElementsTopPanel.ButtonEditMovies.TextColor = Color.Blue;
                DataBase.TableMovies movies = (DataBase.TableMovies)e.Item;
                

                TapGestureRecognizer _tap = new TapGestureRecognizer();

                _tap.Tapped += (sender2, e2) => {
                    App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesEdit(movies.ID));

                    // App.MasterDetailPage.Detail.Navigation.PopModalAsync();
                    //App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Books.ViewBooksEdit(books.ID));
                    App.MasterDetailPage.IsPresented = false;

                    ElementsTopPanel.ButtonEditMovie.IsVisible = false;
                };

                ElementsTopPanel.ButtonEditMovie.Clicked(_tap);
            };
            ViewHelp.ViewHelp_Movies.Movies.HelpListElements.Number = 0;
            return ListaView;
        }


        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_Movies.GetMovies();
                ListaView.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Movies.Movies.HelpListView));
                ViewHelp.ViewHelp_Movies.Movies.HelpListElements.Number = 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku

        public void StartUpdate()
        {
            if (cts != null) cts.Cancel();
            cts = new System.Threading.CancellationTokenSource();
            var ignore = UpdateAsync(cts);
        }

        public void StopUpdate()
        {
            if (cts != null) cts.Cancel();
            cts = null;
            secondRefresh = 0;
            ElementsTopPanel.ButtonEditMovie.IsVisible = false;
        }

        public async System.Threading.Tasks.Task UpdateAsync(System.Threading.CancellationTokenSource ct)
        {
            while (!ct.IsCancellationRequested)
            {
                if (secondRefresh >= 5)
                {
                    StopUpdate();
                }
                else
                {
                    secondRefresh++;
                }

                await System.Threading.Tasks.Task.Delay(1000);
            }
        }
    }
}
