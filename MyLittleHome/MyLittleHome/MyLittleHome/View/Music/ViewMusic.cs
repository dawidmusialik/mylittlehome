﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.View.Music
{
    public class ViewMusic : ContentPage
    { 
        private ListView ListaView;
        ViewHelp.ViewHelp_Music.Music.HelpTopPanelElements ElementsTopPanel;
        private System.Threading.CancellationTokenSource cts;
        private int secondRefresh = 0;


        public ViewMusic()
    {
        ListaView = new ListView();
            ElementsTopPanel = new ViewHelp.ViewHelp_Music.Music.HelpTopPanelElements();

        Title = "Music";
        StackLayout MusicView = new StackLayout();

            MusicView.BackgroundColor = Color.FromHex("ABABAB");
            MusicView.VerticalOptions = LayoutOptions.FillAndExpand;

            MusicView.Children.Add(GridTittleAndButton());
            MusicView.Children.Add(List());

        this.Content = MusicView;
    }


    private Grid GridTittleAndButton()
    {

        Grid Gridview = new Grid()
        {
            RowDefinitions =
                {
                   new RowDefinition { Height = 35 },
                },
            ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                },
            Padding = 0,
            RowSpacing = 1,
            ColumnSpacing = 0,

        };
        ElementsTopPanel.ButtonEditMusic.IsVisible = false;
        Gridview.Children.Add(ElementsTopPanel.ButtonAddMusic, 0, 2, 0, 1);
        Gridview.Children.Add(ElementsTopPanel.TitlePage, 2, 5, 0, 1);
        Gridview.Children.Add(ElementsTopPanel.ButtonEditMusic, 5, 7, 0, 1);

        return Gridview;
    }

    private ListView List()
    {
        ListaView.ItemsSource = App.MyLittleHomeDB_Music.GetMusic();
        ListaView.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Music.Music.HelpListView));
        ListaView.IsPullToRefreshEnabled = true;
        ListaView.Refreshing += (sender, e1) => { refreshList(); };
        ListaView.HasUnevenRows = true;
        ListaView.RowHeight = -1;

        ListaView.ItemTapped += (object sender, ItemTappedEventArgs e) => {
            ElementsTopPanel.ButtonEditMusic.IsVisible = true;
            StartUpdate();
            // ElementsTopPanel.ButtonEditMusic.TextColor = Color.Blue;
            DataBase.TableMusic music = (DataBase.TableMusic)e.Item;
          /*  ElementsTopPanel.ButtonEditMusic.Clicked += (object senderB, EventArgs eB) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Music.ViewMusicEdit(music.ID));
               
                App.MasterDetailPage.IsPresented = false;

                ElementsTopPanel.ButtonEditMusic.IsVisible = false;
            };
            */
            TapGestureRecognizer _tap = new TapGestureRecognizer();

            _tap.Tapped += (sender2, e2) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Music.ViewMusicEdit(music.ID));

                // App.MasterDetailPage.Detail.Navigation.PopModalAsync();
                //App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Books.ViewBooksEdit(books.ID));
                App.MasterDetailPage.IsPresented = false;

                ElementsTopPanel.ButtonEditMusic.IsVisible = false;
            };

            ElementsTopPanel.ButtonEditMusic.Clicked(_tap);
        };

        ViewHelp.ViewHelp_Music.Music.HelpListElements.Number = 0;
        return ListaView;
    }

        //Odświeżanie widoku
        private void refreshList()
        {
            ListaView.ItemsSource = App.MyLittleHomeDB_Music.GetMusic(); 
            ListaView.EndRefresh();
        }

        public void StartUpdate()
        {
            if (cts != null) cts.Cancel();
            cts = new System.Threading.CancellationTokenSource();
            var ignore = UpdateAsync(cts);
        }

        public void StopUpdate()
        {
            if (cts != null) cts.Cancel();
            cts = null;
            secondRefresh = 0;
            ElementsTopPanel.ButtonEditMusic.IsVisible = false;
        }

        public async System.Threading.Tasks.Task UpdateAsync(System.Threading.CancellationTokenSource ct)
        {
            while (!ct.IsCancellationRequested)
            {
                if (secondRefresh >= 5)
                {
                    StopUpdate();
                }
                else
                {
                    secondRefresh++;
                }

                await System.Threading.Tasks.Task.Delay(1000);
            }
        }
    }
}
