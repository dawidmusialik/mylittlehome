﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;


namespace MyLittleHome.View.Music
{
    public class ViewMusicAdd: TabbedPage
    {
        ViewHelp.ViewHelp_Music.AddMusic.HelpView ViewAddNewMusic = new ViewHelp.ViewHelp_Music.AddMusic.HelpView();
        ViewHelp.ViewHelp_Music.FormatMusic.HelpView ViewFormatMusic = new ViewHelp.ViewHelp_Music.FormatMusic.HelpView();
        public ViewMusicAdd()
        {

            this.Title = "Add";
            this.Children.Add(ViewAddNewMusic);
            this.Children.Add(ViewFormatMusic);
          //  this.BackgroundColor = Color.FromHex("ABABAB");
          //  this.BarBackgroundColor = Color.FromHex("ABABAB");
           
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();
            this.BarBackgroundColor = CustomVisual.Instance.getBarColor();
        }
    }
}
