﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.View.Music
{
    public class ViewMusicEdit: TabbedPage
    {
        ViewHelp.ViewHelp_Music.LoanMusic.HelpView LoanView;
        ViewHelp.ViewHelp_Music.EditMusic.HelpView EditView;

        public ViewMusicEdit(int musicid)
        {
            var ObjectOneBook = App.MyLittleHomeDB_Music.GetMusicByID(musicid);
            if (ObjectOneBook.Count >= 0)
            {
                EditView = new ViewHelp.ViewHelp_Music.EditMusic.HelpView(ObjectOneBook[0]);

                if (ObjectOneBook[0].Loan == true)
                {
                    this.Title = "Edit";
                    LoanView = new ViewHelp.ViewHelp_Music.LoanMusic.HelpView(ObjectOneBook[0]);

                    this.Children.Add(EditView);
                    this.Children.Add(LoanView);
                }
                else
                {
                    this.Title = "Edit";
                    this.Children.Add(EditView);
                }

            }
            else
            {
                this.Children.Add(new ContentPage { Content = new StackLayout { Children = { new Label { Text = "Wystąpił błąd podczas ładowania albumu" } } } });

            }

            //this.BackgroundColor = Color.FromHex("ABABAB");
            //this.BarBackgroundColor = Color.FromHex("ABABAB");
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();
            this.BarBackgroundColor = CustomVisual.Instance.getBarColor();
        }
    }
}
