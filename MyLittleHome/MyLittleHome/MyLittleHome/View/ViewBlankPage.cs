﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.View
{
	public class ViewBlankPage : ContentPage
	{
        //Obiekt StackLayout
        StackLayout LayoutBlankPage;
		public ViewBlankPage ()
		{
            //Nowy obiekt StackLayout
            LayoutBlankPage = new StackLayout();
            Title = "BlankPage";

            LayoutBlankPage.Children.Add(new Label { Text = "Welcome to Blank Page, Constructor View" });
            this.Content = LayoutBlankPage;
            Padding = 50;
        }
    
	}
}
