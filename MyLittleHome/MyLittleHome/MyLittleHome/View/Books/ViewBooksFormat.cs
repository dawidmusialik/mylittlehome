﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyLittleHome.View.Books
{
    public class ViewBooksFormat : ContentPage
    {
        private StackLayout BooksViewFormat;
        private ListView ListaView;
        private Button DeleteFormat { get; set; }

        ViewHelp.ViewHelp_Books.FormatBooks.HelpTopPanelElements BooksElementsTopPanel;

        public ViewBooksFormat()
        {
            Title = "Books Format";
            BooksViewFormat = new StackLayout();
            ListaView = new ListView();
            BooksElementsTopPanel = new ViewHelp.ViewHelp_Books.FormatBooks.HelpTopPanelElements();

            this.DeleteFormat = CreateButtonDelete("Delete");
            this.DeleteFormat.IsVisible = false;

            BooksViewFormat.Children.Add(TopPanel());
            BooksViewFormat.Children.Add(FormatList());
            BooksViewFormat.Children.Add(DeleteFormat);


            this.Content = BooksViewFormat;
        }

        private Grid TopPanel()
        {
            Grid GRID = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            GRID.Children.Add(BooksElementsTopPanel.Name, 0, 1, 0, 1);
            GRID.Children.Add(BooksElementsTopPanel.FormatName, 1, 4, 0, 1);
            GRID.Children.Add(BooksElementsTopPanel.SendFormat, 4, 5, 0, 1);
            return GRID;
        }


        public ListView FormatList()
        {

            ListaView = new ListView();

            ListaView.ItemsSource = App.MyLittleHomeDB_BooksFormat.GetBooksFormatItemSource();
            ListaView.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Books.FormatBooks.HelpListView));
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            // ListaView.RowHeight = 265;
            ListaView.ItemTapped += ListaViewItemTapped;
            return ListaView;
        }

        private void ListaViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            DeleteFormat.IsVisible = true;
            DataBase.TableBooksFormat Usuwanie = new DataBase.TableBooksFormat();

            DeleteFormat.Clicked += (object sender2, EventArgs e2) =>
            {
                Usuwanie = (DataBase.TableBooksFormat)e.Item;

                if (App.MyLittleHomeDB_Books.CheckIDFormatBooks_In_TableBooks(Usuwanie.ID) == false)
                {
                    App.MyLittleHomeDB_BooksFormat.DeleteOneBooksFormat(Usuwanie.ID);
                }
                Usuwanie = null;
                DeleteFormat.IsVisible = false;
            };
        }


        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_BooksFormat.GetBooksFormatItemSource();
                ListaView.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Books.FormatBooks.HelpListView));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku

        private Button CreateButtonDelete(string nazwa)
        {
            Button name = new Button();
            name.Text = nazwa;
            return name;
        }


    }
}
