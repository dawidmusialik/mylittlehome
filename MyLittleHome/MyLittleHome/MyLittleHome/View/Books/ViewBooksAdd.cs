﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;
using MyLittleHome.Component;
using Xamarin.Forms;

namespace MyLittleHome.View.Books
{
    public class ViewBookAdd : TabbedPage
    {
        ViewHelp.ViewHelp_Books.AddBooks.HelpView ViewAddNewBooks = new ViewHelp.ViewHelp_Books.AddBooks.HelpView();
        ViewHelp.ViewHelp_Books.FormatBooks.HelpView ViewFormatBooks = new ViewHelp.ViewHelp_Books.FormatBooks.HelpView();
        public ViewBookAdd()
        {

            this.Title = "Add";
            this.Children.Add(ViewAddNewBooks);
            this.Children.Add(ViewFormatBooks);
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();
            this.BarBackgroundColor = CustomVisual.Instance.getBarColor();

        }
    }
}
