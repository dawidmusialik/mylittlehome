﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;
using MyLittleHome.Component;
using Xamarin.Forms;

namespace MyLittleHome.View.Books
{
    public class ViewBooksEdit : TabbedPage
    {
        ViewHelp.ViewHelp_Books.LoanBooks.HelpView LoanView;
        ViewHelp.ViewHelp_Books.EditBooks.HelpView EditView;
        public ViewBooksEdit(int booksid) {

            //   LoanView = new ViewHelp.ViewHelp_Books.LoanBooks.HelpView();

            var ObjectOneBook = App.MyLittleHomeDB_Books.GetBooksByID(booksid);
            if (ObjectOneBook.Count >= 0)
            {
                EditView = new ViewHelp.ViewHelp_Books.EditBooks.HelpView(ObjectOneBook[0]);

                if (ObjectOneBook[0].Loan == true)
                {
                    this.Title = "Edit";
                    LoanView = new ViewHelp.ViewHelp_Books.LoanBooks.HelpView(ObjectOneBook[0]);

                    this.Children.Add(EditView);
                    this.Children.Add(LoanView);
                }
                else
                {
                    this.Title = "Edit";
                    this.Children.Add(EditView);
                }

            }
            else
            {
                this.Children.Add(new ContentPage {Content = new StackLayout { Children = { new Label { Text = "Wystąpił błąd podczas ładowania książki" } } } });

            }

            //this.BackgroundColor = Color.FromHex("ABABAB");
            //this.BarBackgroundColor = Color.FromHex("ABABAB");
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();
            this.BarBackgroundColor = CustomVisual.Instance.getBarColor();
        }
        
    }
}
