﻿using MyLittleHome.Component;
using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyLittleHome.View.Books
{
    //public class ViewBooks : ContentPage
    public class ViewBooks : ContentPage {
        private ListView ListaView;
        ViewHelp.ViewHelp_Books.Books.HelpTopPanelElements ElementsTopPanel;
        private System.Threading.CancellationTokenSource cts;
        private int secondRefresh = 0;


        public ViewBooks()
        {
            ListaView = new ListView();
            ElementsTopPanel = new ViewHelp.ViewHelp_Books.Books.HelpTopPanelElements();
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();
            Title = "Books";


            StackLayout BooksView = new StackLayout();

           // BooksView.BackgroundColor = Color.FromHex("ABABAB");
            BooksView.VerticalOptions = LayoutOptions.FillAndExpand;

            BooksView.Children.Add(GridTittleAndButton());
            BooksView.Children.Add(List());

            this.Content = BooksView;
        }

      

        private Grid GridTittleAndButton()
        {

            Grid Gridview = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 35 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,

            };
            Gridview.BackgroundColor = CustomVisual.Instance.getTopPanelColor();
            ElementsTopPanel.ButtonEditBook.IsVisible = false;
            Gridview.Children.Add(ElementsTopPanel.ButtonAddBook, 0, 2, 0, 1);
            Gridview.Children.Add(ElementsTopPanel.TitlePage, 2, 5, 0, 1);
            Gridview.Children.Add(ElementsTopPanel.ButtonEditBook, 5, 7, 0, 1);
            return Gridview;
        }

        private ListView List()
        {
            ListaView.ItemsSource = App.MyLittleHomeDB_Books.GetBooks();
            ListaView.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Books.Books.HelpListView));
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            //  ListaView.RowHeight = 270;
            ListaView.HasUnevenRows = true;
            ListaView.RowHeight = -1;
            

            ListaView.ItemTapped += (object sender, ItemTappedEventArgs e) => {
                StartUpdate();
                ElementsTopPanel.ButtonEditBook.IsVisible = true;
                //ElementsTopPanel.ButtonEditBook.TextColor = Color.Blue;
                DataBase.TableBooks books = (DataBase.TableBooks)e.Item;
                TapGestureRecognizer _tap = new TapGestureRecognizer();

                _tap.Tapped += (sender2, e2) => {
                    App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooksEdit(books.ID));

                    // App.MasterDetailPage.Detail.Navigation.PopModalAsync();
                    //App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Books.ViewBooksEdit(books.ID));
                    App.MasterDetailPage.IsPresented = false;

                    ElementsTopPanel.ButtonEditBook.IsVisible = false;
                };
                
                ElementsTopPanel.ButtonEditBook.Clicked(_tap);
            };

            ViewHelp.ViewHelp_Books.Books.HelpListElements.Number = 0;
            return ListaView;
        }

        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            //Odświeżanie źródel listy wywala wyjątek, dlatego przy odświeżaniu daję ładowanie widoku raz jeszcze.
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_Books.GetBooks();
                ListaView.ItemTemplate = new DataTemplate(typeof(ViewHelp.ViewHelp_Books.Books.HelpListView));
                App.MasterDetailPage.IsPresented = false;
                ViewHelp.ViewHelp_Books.Books.HelpListElements.Number = 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku

        public void StartUpdate() {
            if (cts != null) cts.Cancel();
            cts = new System.Threading.CancellationTokenSource();
            var ignore = UpdateAsync(cts);
        }

        public void StopUpdate() {
            if (cts != null) cts.Cancel();
            cts = null;
            secondRefresh = 0;
            ElementsTopPanel.ButtonEditBook.IsVisible = false;
        }

        public async System.Threading.Tasks.Task UpdateAsync(System.Threading.CancellationTokenSource ct) {
            while (!ct.IsCancellationRequested) {
                if(secondRefresh >=5) {
                    StopUpdate();
                }
                else {
                    secondRefresh++;
                }

                await System.Threading.Tasks.Task.Delay(1000);
            }
        }


    }
}
