﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.View
{
	public class ViewHomeScreen : ContentPage
	{
        private StackLayout Stack;
        private Grid viewGrid;
        private ScrollView viewScroll;
        ViewHelp.HelpHomeScreenElements viewElements;
		public ViewHomeScreen ()
		{
            viewElements = new ViewHelp.HelpHomeScreenElements();
            Stack = new StackLayout();
            viewScroll = new ScrollView();

            Title = "WelcomePage";
          
            Stack.Children.Add(new Label { Text = "My Little Home", FontSize = 26, TextColor = Color.Red, HorizontalOptions = LayoutOptions.Center });
            Stack.Children.Add(new Label { Text = "Dawid Musialik", FontSize = 16 , TextColor = Color.Red, HorizontalOptions = LayoutOptions.Center});

            Stack.Children.Add(new BoxView());
            Stack.Children.Add(createLabel("Database Items"));
            Stack.Children.Add(createLabel("Books"));

            setGrid();
            Stack.Children.Add(viewGrid);

            viewScroll.Content = Stack;
            
            this.Content = viewScroll;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            viewElements.refresItem();
        }

        private void setGrid()
        {
            viewGrid = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },

                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
            };

            //Books
            viewGrid.Children.Add(viewElements.booksAmountName,0,1,0,1);
            viewGrid.Children.Add(viewElements.booksAmount,1,2,0,1);
            viewGrid.Children.Add(viewElements.booksAmountFormatName,0,1,1,2);
            viewGrid.Children.Add(viewElements.booksAmountFormat,1,2,1,2);

            //separator
            viewGrid.Children.Add(createLabel("Budget"),0,2,2,3);

            //Budget
            viewGrid.Children.Add(viewElements.budgetAmountName,0,1,3,4);
            viewGrid.Children.Add(viewElements.budgetAmount,1,2,3,4);
            viewGrid.Children.Add(viewElements.budgetAmountValueName,0,1,4,5);
            viewGrid.Children.Add(viewElements.budgetAmountValue,1,2,4,5);

            //separator
            viewGrid.Children.Add(createLabel("Movies"), 0, 2, 5, 6);

            //Movies
            viewGrid.Children.Add(viewElements.moviesAmountName,0,1,6,7);
            viewGrid.Children.Add(viewElements.moviesAmount,1,2,6,7);
            viewGrid.Children.Add(viewElements.moviesAmountFormatName,0,1,7,8);
            viewGrid.Children.Add(viewElements.moviesAmountFormat,1,2,7,8);
            viewGrid.Children.Add(viewElements.moviesAmountLanguageName,0,1,8,9);
            viewGrid.Children.Add(viewElements.moviesAmountLanguage,1,2,8,9);
            viewGrid.Children.Add(viewElements.moviesAmountQualityName,0,1,9,10);
            viewGrid.Children.Add(viewElements.moviesAmountQuality,1,2,9,10);
            viewGrid.Children.Add(viewElements.moviesAmountSubbitlesName,0,1,10,11);
            viewGrid.Children.Add(viewElements.moviesAmountSubbitles,1,2,10,11);

            //separator
            viewGrid.Children.Add(createLabel("Music"), 0, 2, 11, 12);

            //Music
            viewGrid.Children.Add(viewElements.musicAmountName,0,1,12,13);
            viewGrid.Children.Add(viewElements.musicAmount,1,2,12,13);
            viewGrid.Children.Add(viewElements.musicAmountFormatName,0,1,13,14);
            viewGrid.Children.Add(viewElements.musicAmountFormat, 1, 2, 13, 14);
        }

        private Label createLabel(string text)
        {
            Label _label = new Label();
            _label.Text = text;
            _label.FontSize = 30;
            _label.TextColor = Color.FromHex("57FF9D");

            return _label;         
        }
    }
}
