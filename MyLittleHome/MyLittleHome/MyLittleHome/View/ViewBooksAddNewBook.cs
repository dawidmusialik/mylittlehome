﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.View
{
    public class ViewBookAddNewBook : ContentPage
    {
        ViewHelp.ViewHelp_Books.AddBooks.HelpView ViewAddNewBook;

        public ViewBookAddNewBook()
        {
            Title = " Add new books";
            ViewAddNewBook = new ViewHelp.ViewHelp_Books.AddBooks.HelpView();

            ScrollView Scroll = new ScrollView { Content = ViewAddNewBook };
            Scroll.BackgroundColor = Color.FromHex("BDBDBD");
            
            this.Content = Scroll;
        }
    }
}
