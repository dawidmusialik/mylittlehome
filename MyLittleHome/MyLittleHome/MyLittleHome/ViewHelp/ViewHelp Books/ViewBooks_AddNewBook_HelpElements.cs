﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
    public class ViewBooks_AddNewBook_HelpElements
    {
        //Główne elementy do wpisywania danych, które zostaną wysłane do tabeli Books
        private Entry Title; //Wprowadzenie tytułu
        private Entry Author;
        private Entry PublicationDate;
       // private Entry Format;
        private Entry StorageDevice;
        //Elementy nazw pól Entry
        private Label Title_Name;
        private Label Author_Name;
        private Label PublicationDate_Name;
        private Label Format_Name;
        private Label StorageDevice_Name;
        private Label Description_Name;

        //TextBox dla opisu
        private Editor Description;

        //Picker dla formatu
        private Picker Format;
        private int FormatLicznik = 1;

        //Pobranie elementów do wpisania danych, które zostaną wysłane do tabeli Books
        public Entry Get_Entry_Elements(string name)
        {
            switch (name)
            {
                case "Title":
                    return Title;
                case "Author":
                    return Author;
                case "PublicationDate":
                    return PublicationDate;
                case "StorageDevice":
                    return StorageDevice;
                

                default:
                    return new Entry();
            }

        }


        public Label Get_Label_Elements(string name)
        {
            switch (name)
            {
                case "Title_Name":
                    return Title_Name;
                case "Author_Name":
                    return Author_Name;
                case "PublicationDate_Name":
                    return PublicationDate_Name;
                case "Format_Name":
                    return Format_Name;
                case "StorageDevice_Name":
                    return StorageDevice_Name;
                case "Description_Name":
                    return Description_Name;

                default:
                    return new Label();
            }

        }

        public Editor Get_Editor_Elements(string name)
        {
            switch (name)
            {
                case "Description":
                    return Description;
                
                default:

                    return new Editor();
            }
        }

        public Picker Get_Picker_Elements(string name)
        {
            switch(name)
            {
                case "Format":
                    return Format;

                default:
                    return new Picker();
            }
        }

        //Publiczna funkcja tworząca wszystkie elementy
        public void Elements()
        {
            //Elementy pobierające dane z tabeli Books
            Elements_Entry_Create(ref Title, "Title");
            Elements_Entry_Create(ref Author, "Author");
            Elements_Entry_Create(ref PublicationDate, "PublicationDate");
            Elements_Entry_Create(ref StorageDevice, "StorageDevice");

            //Elementy nazw danych do tabeli Books
            Elements_Name_Create(ref Title_Name, "Title:");
            Elements_Name_Create(ref Author_Name, "Author:");
            Elements_Name_Create(ref PublicationDate_Name, "Year:");
            Elements_Name_Create(ref Format_Name, "Format:");
            Elements_Name_Create(ref StorageDevice_Name, "Storage Device:");
            Elements_Name_Create(ref Description_Name, "Description:");

            //Editor
            Elements_Editor_Create(ref Description);

            //Picker format
            Elements_Picer_Create(ref Format);
        }

        //Prywatna funkcja tworząca elementy entry do wpisywania danych
        private Entry Elements_Entry_Create(ref Entry name, string property)
        {
            name = new Entry();
            name.Placeholder = property;
            name.PlaceholderColor = Color.Black;
            name.FontSize = 15;
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }

        //Prywatna funkcja tworząca nowe elementy nazw
        private Label Elements_Name_Create(ref Label name, string text)
        {
            name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

        //Słownik opicji wyboru formatu
        Dictionary<string, int> DictionaryFormat = App.MyLittleHomeDB_BooksFormat.GetBooksFormatDictionary();
        


        private Picker Elements_Picer_Create(ref Picker NewFormat)
        {
            NewFormat = new Picker();
            NewFormat.Title = "Format";
            NewFormat.HorizontalOptions = LayoutOptions.Center;
            foreach(string items in DictionaryFormat.Keys)
            {
                NewFormat.Items.Add(items);
            }
            return NewFormat;
        }

        public int GetFormat()
        {
            if(Format.SelectedIndex !=- 1)
            {
                string ItemName = Format.Items[Format.SelectedIndex];
                FormatLicznik = DictionaryFormat[ItemName];
            }

            // return App.MyLittleHomeDB_BooksFormat.GetDirectBooksFormat(FormatLicznik);
            return FormatLicznik;
        }

        //Funkcja torząca pole editor do wprowadzania notatek
        private Editor Elements_Editor_Create(ref Editor NewEditor)
        {
            NewEditor = new Editor();

            NewEditor.TextColor = Color.Black;
            NewEditor.FontSize = 15;

            return NewEditor;
        }

        
    }
}
