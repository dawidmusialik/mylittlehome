﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
    public class ViewBooksFormatHelpElementsTopPanel
    {
        public Label Name { get; private set; }
        public Entry FormatName { get; private set; }
        public Button SendFormat { get; private set; }

        public ViewBooksFormatHelpElementsTopPanel()
        {
            CreateAll();
        }


        private void CreateAll()
        {
            this.Name = CreateLabel("Nazwa");
            this.FormatName = CreateEntry("Format");
            this.SendFormat = CreateButton("Send", SendFormatEventHandler);
        }


        private Label CreateLabel(string tekst)
        {
            Label name = new Label();
            name.Text = tekst;
            name.FontSize = 17;
            name.HorizontalOptions = LayoutOptions.Center;
            name.VerticalOptions = LayoutOptions.Center;
            return name;
        }

        private Entry CreateEntry(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.FontSize = 15;
            return name;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button nazwa = new Button();
            nazwa.Text = tekst;
            nazwa.Clicked += akcja;
            return nazwa;
        }

        private void SendFormatEventHandler(object sender, EventArgs e)
        {
            if (App.MyLittleHomeDB_BooksFormat.CheckInsertBooksFormat(this.FormatName.Text.ToString()) == true)
            {
                App.MyLittleHomeDB_BooksFormat.InsertBooksFormat(this.FormatName.Text.ToString());
                App.LocalNotification("Dodano format");
            }
        }
        
            

    }
}
