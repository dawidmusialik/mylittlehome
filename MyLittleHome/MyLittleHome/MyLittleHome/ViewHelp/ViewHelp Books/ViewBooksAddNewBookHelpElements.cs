﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
    public class ViewBooksAddNewBookHelpElements
    {
        public ViewBooksAddNewBookHelpElements()
        {
            Elements();
        }
        public Entry Title { get; private set; }
        public Entry Author { get; private set; }
        public Entry PublicationDate { get; private set; }
        public Entry StorageDevice { get; private set; }


        public Label TitleName { get; private set; }
        public Label AuthorName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }

        public Picker Format { get; private set; }
        private int FormatLicznik = 1;

        public Editor Description { get; private set; }

        public Button Cancel { get; private set; }
        public Button Save { get; private set; }




        //Publiczna funkcja tworząca wszystkie elementy
        private void Elements()
        {

            this.Title = ElementsEntryCreate("Title");
            this.Author = ElementsEntryCreate("Author");
            this.PublicationDate = ElementsEntryCreate("PublicationDate");
            this.Format = ElementsPicerCreate();
            this.StorageDevice = ElementsEntryCreate("StorageDevice");
            this.Description = ElementsEditorCreate();

            this.TitleName = ElementsNameCreate("Title:");
            this.AuthorName = ElementsNameCreate("Author:");
            this.PublicationDateName = ElementsNameCreate("Year:");
            this.FormatName = ElementsNameCreate("Format");
            this.StorageDeviceName = ElementsNameCreate("Storage Device:");
            this.DescriptionName = ElementsNameCreate("Description:");

            this.Cancel = ElementsButtonCreate("Cancel", ButtonCancelEventHandler);
            this.Save = ElementsButtonCreate("Save", ButtonSaveEventHandler);
        }

        //Prywatna funkcja tworząca elementy entry do wpisywania danych
        private Entry ElementsEntryCreate(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.PlaceholderColor = Color.Black;
            name.FontSize = 15;
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }

        //Prywatna funkcja tworząca nowe elementy nazw
        private Label ElementsNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

        //Słownik opicji wyboru formatu
        Dictionary<string, int> DictionaryFormat = App.MyLittleHomeDB_BooksFormat.GetBooksFormatDictionary();



        private Picker ElementsPicerCreate()
        {
            Picker NewFormat = new Picker();
            NewFormat.Title = "Format";
            NewFormat.HorizontalOptions = LayoutOptions.Center;
            foreach (string items in DictionaryFormat.Keys)
            {
                NewFormat.Items.Add(items);
            }
            return NewFormat;
        }

        public int GetFormat()
        {
            if (Format.SelectedIndex != -1)
            {
                string ItemName = Format.Items[Format.SelectedIndex];
                FormatLicznik = DictionaryFormat[ItemName];
            }

            // return App.MyLittleHomeDB_BooksFormat.GetDirectBooksFormat(FormatLicznik);
            return FormatLicznik;
        }

        //Funkcja torząca pole editor do wprowadzania notatek
        private Editor ElementsEditorCreate()
        {
            Editor NewEditor = new Editor();

            NewEditor.TextColor = Color.Black;
            NewEditor.FontSize = 15;

            return NewEditor;
        }


        private Button ElementsButtonCreate(string text, EventHandler akcja)
        {
            Button name = new Button();
            name.Text = text;
            name.Clicked += akcja;
            return name;
        }

        private void ButtonCancelEventHandler(object sender, EventArgs e)
        {
            App.MasterDetailPage.Detail = new NavigationPage(new View.ViewBooks());
        }

        private void ButtonSaveEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Books.InsertBooks(Title.Text.ToString(), Author.Text.ToString(), (Convert.ToInt32(PublicationDate.Text)), GetFormat(), Description.Text.ToString(), Description.Text.ToString());
            App.MasterDetailPage.Detail = new NavigationPage(new View.ViewBooks());
            App.LocalNotification("Dodano nową książkę");
        }


    }
}
