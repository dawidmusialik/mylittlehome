﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp {
    public class ViewBooksHelpElementsTopPanel {
        public Label TitlePage { get; private set; }
        public Button ButtonAddBook { get; private set; }
        public Button ButtonEditBook { get; set; }


        public ViewBooksHelpElementsTopPanel() {
            CreateElements();
        }

        private void CreateElements() {
            this.TitlePage = CreateLabel("My Books");
            this.ButtonAddBook = CreateButton("Add Books", ButtonAddBookEventHandler);
            this.ButtonEditBook = CreateButton("Edit Books", null);
        }

        private Label CreateLabel(string tekst) {
            Label Title = new Label();
            Title.Text = tekst;
            Title.FontSize = 30;
            Title.TextColor = Color.FromHex("57FF9D");
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.VerticalOptions = LayoutOptions.Center;
            Title.HorizontalOptions = LayoutOptions.Center;
            return Title;
        }

        private Button CreateButton(string tekst, EventHandler akcja) {
            Button button = new Button();
            button.Text = tekst;
            button.Clicked += akcja;
            button.VerticalOptions = LayoutOptions.End;
            button.HorizontalOptions = LayoutOptions.End;

            return button;
        }

        private void ButtonAddBookEventHandler(object sender, EventArgs e) {
            App.MasterDetailPage.Detail = new NavigationPage(new View.ViewBookAddNewBook());
        }




    }
}
