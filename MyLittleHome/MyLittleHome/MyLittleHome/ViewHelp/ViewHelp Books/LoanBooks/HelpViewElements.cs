﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;


namespace MyLittleHome.ViewHelp.ViewHelp_Books.LoanBooks {
    public class HelpViewElements {
        DataBase.TableBooksLoan DirectBookLoan;
        DataBase.TableBooksWithTableBooksFormat DirectBook;
        public HelpViewElements(DataBase.TableBooksWithTableBooksFormat OneBook) {
            DirectBookLoan = (App.MyLittleHomeDB_BooksLoan.GetDirectBooksLoan(OneBook.ID)[0]);
            DirectBook = OneBook;
            ElementsCreate();
        }

        public DatePicker DateLoan { get; private set; }
        public DatePicker DateReceipt { get; private set; }
        public Entry Who { get; private set; }
        public Label DateLoanName { get; private set; }
        public Label DateReceiptName { get; private set; }
        public Label WhoName { get; private set; }


        //public Button Cancel { get; private set; }
        public CustomButton Cancel { get; private set; }

        //public Button Save { get; private set; }
        public CustomButton Save { get; private set; }

        //public Button Delete { get; private set; }
        public CustomButton Delete { get; private set; }



        private void ElementsCreate() {
            this.DateLoanName = ElementsLabelNameCreate("Date Loan:");
            this.DateReceiptName = ElementsLabelNameCreate("Date Receipt:");
            this.WhoName = ElementsLabelNameCreate("Who?:");

            this.DateLoan = ElementsDatePicerCreate();
            this.DateLoan.Date = DirectBookLoan.DateLoan.Date;
            this.DateReceipt = ElementsDatePicerCreate();
            this.DateReceipt.Date = DirectBookLoan.DateReceipt.Date;

            this.Who = ElementsEntryCreate();
            this.Who.Text = DirectBookLoan.Who;

            //this.Cancel = ElementsButtonCreate("Cancel", CancelEventHandler);
            this.Cancel = new CustomButton(path:"iconBack.png");
            this.Cancel.Clicked(CancelTapGesture());

            //this.Save = ElementsButtonCreate("Save", SaveEventHandler);
            this.Save = new CustomButton(path:"iconSave.png");
            this.Save.Clicked(SaveTapGesture());

            //this.Delete = ElementsButtonCreate("Delete", DeleteEventHandler);
            this.Delete = new CustomButton(path:"iconDelete.png");
            this.Delete.Clicked(DeleteTapGesture());
        }


        private Entry ElementsEntryCreate() {
            Entry name = new Entry();
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }
        private Label ElementsLabelNameCreate(string text) {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
        private DatePicker ElementsDatePicerCreate() {
            DatePicker NewDate = new DatePicker();
            NewDate.Format = "yyy-MM-dd";
            NewDate.HorizontalOptions = LayoutOptions.Center;
            return NewDate;
        }

        private Button ElementsButtonCreate(string tekst, EventHandler akcja) {
            Button NewButton = new Button();
            NewButton.Text = tekst;
            NewButton.Clicked += akcja;
            return NewButton;
        }
        /*
        private void CancelEventHandler(object sender, EventArgs e) {
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
        }
        */
        private TapGestureRecognizer CancelTapGesture() {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
            };
            return _tap;
        }
        /*
        private void SaveEventHandler(object sender, EventArgs e) {
            App.MyLittleHomeDB_BooksLoan.UpdateBooksLoan(DirectBookLoan.IDBooks, DateLoan.Date, DateReceipt.Date, Who.Text);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooksEdit(DirectBook.ID));
            App.LocalNotification("Loan edited");
        } 
        */ 
           private TapGestureRecognizer SaveTapGesture() {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_BooksLoan.UpdateBooksLoan(DirectBookLoan.IDBooks, DateLoan.Date, DateReceipt.Date, Who.Text);
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooksEdit(DirectBook.ID));
                App.LocalNotification("Loan edited");
            };
            return _tap;
        }
        /*
        private void DeleteEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_BooksLoan.DeleteOneBooksLoan(DirectBook.ID);
            App.MyLittleHomeDB_Books.UpgradeBooks(DirectBookLoan.IDBooks, DirectBook.Title, DirectBook.Author, DirectBook.PublicationDate, DirectBook.IDTableBooksFormat, DirectBook.StorageDevice, DirectBook.Description, false);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooksEdit(DirectBook.ID));
            App.LocalNotification("Book returned");
        }
        */
        private TapGestureRecognizer DeleteTapGesture() {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_BooksLoan.DeleteOneBooksLoan(DirectBook.ID);
                App.MyLittleHomeDB_Books.UpgradeBooks(DirectBookLoan.IDBooks, DirectBook.Title, DirectBook.Author, DirectBook.PublicationDate, DirectBook.IDTableBooksFormat, DirectBook.StorageDevice, DirectBook.Description, false);
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooksEdit(DirectBook.ID));
                App.LocalNotification("Book returned");
            };
            return _tap;
        }
    }
}
