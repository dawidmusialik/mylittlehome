﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
    public class ViewBooksAddNewBookHelpViewGrid
    {
        ViewBooksAddNewBookHelpElements Elements;
        public ViewBooksAddNewBookHelpViewGrid()
        {
            Elements = new ViewBooksAddNewBookHelpElements();
        }


        public Grid GridView()
        {

            Grid ViewGrid = new Grid
            {
                /*
                RowDefinitions =
                {
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 100 },
                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Auto },
                new ColumnDefinition { Width = GridLength.Star },

                },*/
                RowDefinitions =
                {
                  // new RowDefinition { Height = 40 },
                  // new RowDefinition { Height = 40 },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = 80 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                 //   new ColumnDefinition { Width = GridLength.Star },
                 //   new ColumnDefinition { Width = GridLength.Star },
                 //   new ColumnDefinition { Width = GridLength.Star },
                  //  new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                HeightRequest = 275,
                VerticalOptions = LayoutOptions.Start,
                BackgroundColor = Color.Green,
            };

            /*//Tytuł
             ViewGrid.Children.Add(Elements.TitleName, 0, 1, 0, 1);
             ViewGrid.Children.Add(Elements.Title, 1, 7, 0, 1);
             //Autor
             ViewGrid.Children.Add(Elements.AuthorName, 0, 1, 1, 2);
             ViewGrid.Children.Add(Elements.Author, 1, 7, 1, 2);
             //PublicationDate
             ViewGrid.Children.Add(Elements.PublicationDateName, 0, 1, 2, 3);
             ViewGrid.Children.Add(Elements.PublicationDate, 1, 7, 2, 3);
             //Format
             ViewGrid.Children.Add(Elements.FormatName, 0, 1, 3, 4);
             ViewGrid.Children.Add(Elements.Format, 1, 7, 3, 4);
             //StorageDevice
             ViewGrid.Children.Add(Elements.StorageDeviceName, 0, 1, 4, 5);
             ViewGrid.Children.Add(Elements.StorageDevice, 1, 7, 4, 5);
             //Description
             ViewGrid.Children.Add(Elements.DescriptionName, 0, 1, 5, 6);
             ViewGrid.Children.Add(Elements.Description, 1, 7, 5, 6);
             */
           
            //Kolumna 0, Wiersz 1
            ViewGrid.Children.Add((TitleinGrid()), 0, 6, 0, 1);
            //Kolumna 1, Wiersz 1
            ViewGrid.Children.Add((AuthorinGrid()), 0, 6, 1, 2);
            //Kolumna 0, Wiersz 2
            ViewGrid.Children.Add(PublicationDateinGrid(), 0,6, 2, 3);
            //Kolumna 0, Wiersz 3
            ViewGrid.Children.Add(FormatinGrid(), 0, 6, 3, 4);
            //Kolumna 0, Wiersz 4
            ViewGrid.Children.Add(StorageDeviceinGrid(), 0, 6, 4, 5);
            //Kolumna 1, Wiersz od 2 do 4
            ViewGrid.Children.Add(DescriptioninGrid(), 0, 6, 5, 6);

            return ViewGrid;
        }


 

        private StackLayout TitleinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //   Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.TitleName);
            Stack.Children.Add(Elements.Title);

            return Stack;
        }
        private StackLayout AuthorinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //  Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.AuthorName);
            Stack.Children.Add(Elements.Author);

            return Stack;
        }
        private StackLayout PublicationDateinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;            //  Stack.BackgroundColor = Color.Sienna;
            Stack.Children.Add(Elements.PublicationDateName);
            Stack.Children.Add(Elements.PublicationDate);

            return Stack;
        }
        private StackLayout FormatinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.FormatName);
            Stack.Children.Add(Elements.Format);

            return Stack;
        }
        private StackLayout StorageDeviceinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.StorageDeviceName);
            Stack.Children.Add(Elements.StorageDevice);

            return Stack;
        }
        private StackLayout DescriptioninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.BackgroundColor = Color.FromHex("CCCCCC");
            Stack.Children.Add(Elements.DescriptionName);
            Stack.Children.Add(Elements.Description);

            return Stack;
        }


        //Widok grida z przyciskami akcji
        public Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },

                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
                HorizontalOptions = LayoutOptions.EndAndExpand,
                BackgroundColor = Color.Red,
            };
            GridButton.Children.Add(Elements.Cancel, 0, 1, 0, 1);
            GridButton.Children.Add(Elements.Save, 1, 2, 0, 1);
            // GridButton.Padding = new Thickness(0, 0, 0, 30);

            return GridButton;
        }


       


    }
}
