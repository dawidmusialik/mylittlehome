﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp
{
    public class ViewBooks_Edit_HelpElements
    {

        public ViewBooks_Edit_HelpElements() {
            Elements();
        }

        //Elementy na dane z tabeli Books
   
        public Entry ID { get; private set; }
        public Entry Title { get; private set; }
        public Entry Author { get; private set; }
        public Entry PublicationDate { get; private set; }
        public Entry StorageDevice { get; private set; }

        //Elementy na nazwy danych z tabeli Books

        public Label IDName { get; private set; }
        public Label TitleName { get; private set; }
        public Label AuthorName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }

        //Editor dla Opisu
        public Editor Description { get; private set; }

        //Picker dla formatu
        public Picker Format { get; private set; }
        private int FormatLicznik = 1;


 
        //Deklaracja funkcji stworzonych Elementy
        public void Elements()
        {
            //Elementy pobierające dane z tabeli Books
            this.ID = ElementsEntryCreate("ID");
            this.Title = ElementsEntryCreate("Title");
            this.Author = ElementsEntryCreate("Author");
            this.PublicationDate = ElementsEntryCreate("PublicationDate");
            this.StorageDevice = ElementsEntryCreate("StorageDevice");
            //Elementy nazw danych do tabeli Books
            this.IDName = ElementsLabelNameCreate("ID:");
            this.TitleName = ElementsLabelNameCreate("Title:");
            this.AuthorName = ElementsLabelNameCreate("Author:");
            this.PublicationDateName = ElementsLabelNameCreate("Year:");
            this.FormatName = ElementsLabelNameCreate("Format:");
            this.StorageDeviceName = ElementsLabelNameCreate("Storage Device:");
            this.DescriptionName = ElementsLabelNameCreate("Description:");
            //Editor
            this.Description = ElementsEditorCreate("Description");
            //Picker format
            this.Format = ElementsPicerCreate("BooksFormat");
        }

        //Funkcja tworząca elementy, które zawierają dane z tabeli Books
        private Entry Elements_Create(ref Entry name, string property)
        {
            name = new Entry();
            name.SetBinding(Entry.TextProperty, property);
            name.HorizontalTextAlignment = TextAlignment.Center;
           // name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

        private Entry ElementsEntryCreate( string property) {
            Entry name = new Entry();
            name.SetBinding(Entry.TextProperty, property);
            name.HorizontalTextAlignment = TextAlignment.Center;
            // name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
        //Funkcja tworząca nazwy elementów, które zawierają dane z tabeli Books

        private Label ElementsLabelNameCreate(string text) {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

        private Editor ElementsEditorCreate(string property)
        {
            Editor NewEditor = new Editor();
            NewEditor.SetBinding(Editor.TextProperty, property);
            NewEditor.TextColor = Color.Black;
            NewEditor.FontSize = 15;
            return NewEditor;
        }

        //Słownik opicji wyboru formatu
        Dictionary<string, int> DictionaryFormat = App.MyLittleHomeDB_BooksFormat.GetBooksFormatDictionary();


        private Picker ElementsPicerCreate( string property) {
            Picker NewFormat = new Picker();
            NewFormat.Title = "Format";
            NewFormat.SetBinding(Picker.TitleProperty, property);
            NewFormat.HorizontalOptions = LayoutOptions.Center;
            foreach (string items in DictionaryFormat.Keys) {
                NewFormat.Items.Add(items);
            }
            return NewFormat;
        }

        public int GetFormat()
        {
            if (Format.SelectedIndex != -1)
            {
                string ItemName = Format.Items[Format.SelectedIndex];
                FormatLicznik = DictionaryFormat[ItemName];
            }

            // return App.MyLittleHomeDB_BooksFormat.GetDirectBooksFormat(FormatLicznik);
            return FormatLicznik;
        }

    }
}
