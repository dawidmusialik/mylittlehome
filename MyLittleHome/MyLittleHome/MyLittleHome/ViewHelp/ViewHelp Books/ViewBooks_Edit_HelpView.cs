﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp
{
   public class ViewBooks_Edit_HelpView
    {
        public ViewBooks_Edit_HelpView(int booksid) {
            id2 = booksid;
        }
        private ListView ListaView;
       ViewBooks_Edit_HelpElements Elements = new ViewBooks_Edit_HelpElements();
        public static int id2;
        public StackLayout Listaview()
        {
            
            StackLayout Stack = new StackLayout();

            ListaView = new ListView();

            ListaView.ItemsSource = App.MyLittleHomeDB_Books.GetBooksByID(id2);
            ListaView.ItemTemplate = new DataTemplate(() => AllRows());
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            ListaView.RowHeight = 360;

            Stack.VerticalOptions = LayoutOptions.FillAndExpand;

            //Elementy wyglądu
            Stack.Children.Add(ListaView);
            Stack.Children.Add(GridViewButton());
            Stack.BackgroundColor = Color.FromHex("ABABAB");
            return Stack;
        }
        

        private ViewCell AllRows()
        {
            ViewCell AllVIew = new ViewCell
            {
                View = new StackLayout
                {
                    Children =
                    {
                        GridView(),
                    }
                }
            };


            return AllVIew;
        }

        private Grid GridView()
        {
          //  Elements.Elements();
            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 80 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };
            //Kolumna od 0 do 2, wiersz od 0 do 1
            grid.Children.Add((IDinGrid()), 0, 1, 2, 4);
            //Kolumna 0, Wiersz 1
            grid.Children.Add((TitleinGrid()), 0, 6, 0, 1);
            //Kolumna 1, Wiersz 1
            grid.Children.Add((AuthorinGrid()), 0, 6, 1, 2);
            //Kolumna 0, Wiersz 2
            grid.Children.Add(PublicationDateinGrid(), 1, 2, 2, 4);
            //Kolumna 0, Wiersz 3
            grid.Children.Add(FormatinGrid(), 2, 3, 2, 4);
            //Kolumna 0, Wiersz 4
            grid.Children.Add(StorageDeviceinGrid(), 3, 6, 2, 4);
            //Kolumna 1, Wiersz od 2 do 4
            grid.Children.Add(DescriptioninGrid(), 0, 6, 4, 9);
            return grid;
        }

        private StackLayout IDinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.IDName);
            Stack.Children.Add(Elements.ID);

            return Stack;
        }

        private StackLayout TitleinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //   Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.TitleName);
            Stack.Children.Add(Elements.Title);

            return Stack;
        }
        private StackLayout AuthorinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //  Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.AuthorName);
            Stack.Children.Add(Elements.Author);

            return Stack;
        }
        private StackLayout PublicationDateinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Sienna;
            Stack.Children.Add(Elements.PublicationDateName);
            Stack.Children.Add(Elements.PublicationDate);

            return Stack;
        }
        private StackLayout FormatinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.FormatName);
            Stack.Children.Add(Elements.Format);

            return Stack;
        }
        private StackLayout StorageDeviceinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.StorageDeviceName);
            Stack.Children.Add(Elements.StorageDevice);

            return Stack;
        }
        private StackLayout DescriptioninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.BackgroundColor = Color.FromHex("CCCCCC");
            Stack.Children.Add(Elements.DescriptionName);
            Stack.Children.Add(Elements.Description);

            return Stack;
        }


        //Widok grida z przyciskami akcji
        private Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },

                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
                HorizontalOptions = LayoutOptions.EndAndExpand,
            };
            GridButton.Children.Add(Cancel(), 0, 1, 0, 1);
            GridButton.Children.Add(Save(), 1, 2, 0, 1);
            GridButton.Children.Add(Delete(id2),2,3,0,1);

            GridButton.Padding = new Thickness(0, 0, 0, 5);

            return GridButton;
        }

        private Button Cancel()
        {
            Button cancel = new Button();
            cancel.Text = "Cancel";
            cancel.Clicked += (object sender, EventArgs e) =>
            {
                App.MasterDetailPage.Detail = new NavigationPage(new View.Books.ViewBooks());
            };

            return cancel;

        }

        public Button Save()
        {
            Button save = new Button();
            save.Text = "Save";
            save.Clicked += (object sender, EventArgs e) =>
            {
                App.MyLittleHomeDB_Books.UpgradeBooks(id2,Elements.Title.Text.ToString(), Elements.Author.Text.ToString(), (Convert.ToInt32(Elements.PublicationDate.Text)), Elements.GetFormat(), Elements.StorageDevice.Text.ToString(), Elements.Description.Text.ToString());
                App.MasterDetailPage.Detail = new NavigationPage(new View.Books.ViewBooks());
                App.LocalNotification("Edytowano książkę");
            };

            return save;
        }

        public Button Delete(int id)
        {
            Button delete = new Button();
            delete.Text = "Delete";
            delete.Clicked += (object sender, EventArgs e) =>
            {
                App.MyLittleHomeDB_Books.DeleteOneBooks(id);
                App.MasterDetailPage.Detail = new NavigationPage(new View.Books.ViewBooks());
                App.LocalNotification("Usunięto książkę");
            };

            return delete;
        }


        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_Books.GetBooksByID(id2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku

    }
}
