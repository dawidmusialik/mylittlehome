﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace MyLittleHome.ViewHelp
{
    public class ViewBooksHelpViewListView : ViewCell
    {
        ViewBooksHelpElementsListView Elements;

        public ViewBooksHelpViewListView()
        {
            Elements = new ViewBooksHelpElementsListView();
            this.View = GridView();
        }


        private Grid GridView()
        {

            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 20 },
                   new RowDefinition { Height = 30 },
                   new RowDefinition { Height = 20 },
                   new RowDefinition { Height = 30 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 48 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };
            //Kolumna od 0 do 2, wiersz od 0 do 1
            grid.Children.Add((IDinGrid()), 0, 1, 2, 4);
            //Kolumna 0, Wiersz 1
            grid.Children.Add((TitleinGrid()), 0, 6, 0, 1);
            //Kolumna 1, Wiersz 1
            grid.Children.Add((AuthorinGrid()), 0, 6, 1, 2);
            //Kolumna 0, Wiersz 2
            grid.Children.Add(PublicationDateinGrid(), 1, 2, 2, 4);
            //Kolumna 0, Wiersz 3
            grid.Children.Add(FormatinGrid(), 2, 3, 2, 4);
            //Kolumna 0, Wiersz 4
            grid.Children.Add(StorageDeviceinGrid(), 3, 6, 2, 4);
            //Kolumna 1, Wiersz od 2 do 4
            grid.Children.Add(DescriptioninGrid(), 0, 6, 4, 9);
            return grid;
        }

        private StackLayout IDinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.IDName);
            Stack.Children.Add(Elements.ID);

            return Stack;
        }

        private StackLayout TitleinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //   Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.TitleName);
            Stack.Children.Add(Elements.Title);

            return Stack;
        }
        private StackLayout AuthorinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //  Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.AuthorName);
            Stack.Children.Add(Elements.Author);

            return Stack;
        }
        private StackLayout PublicationDateinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Sienna;
            Stack.Children.Add(Elements.PublicationDateName);
            Stack.Children.Add(Elements.PublicationDate);

            return Stack;
        }
        private StackLayout FormatinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.FormatName);
            Stack.Children.Add(Elements.Format);

            return Stack;
        }



        private StackLayout StorageDeviceinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.StorageDeviceName);
            Stack.Children.Add(Elements.StorageDevice);

            return Stack;
        }
        private StackLayout DescriptioninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.BackgroundColor = Color.FromHex("CCCCCC");
            Stack.Children.Add(Elements.DescriptionName);
            Stack.Children.Add(Elements.Description);

            return Stack;
        }



    }
}
