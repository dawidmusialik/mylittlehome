﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Books.EditBooks
{
    public class HelpViewElements
    {
        DataBase.TableBooksWithTableBooksFormat DirectBook;

        public HelpViewElements(DataBase.TableBooksWithTableBooksFormat OneBook)
        {
            DirectBook = OneBook;
            Elements();
        }

        //Elementy na dane z tabeli Books

        public Entry ID { get; private set; }
        public Entry Title { get; private set; }
        public Entry Author { get; private set; }
        public Entry StorageDevice { get; private set; }

        //Elementy na nazwy danych z tabeli Books

        public Label IDName { get; private set; }
        public Label TitleName { get; private set; }
        public Label AuthorName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }
        public Label LoanName { get; private set; }
        public Label LoanDescription { get; private set; }

        //Editor dla Opisu
        public Editor Description { get; private set; }

        //Picker dla formatu
        public Picker Format { get; private set; }
        private int FormatLicznik = 1;
        public DatePicker PublicationDate { get; private set; }

        //Switch dla statusu posiadania ksiązki
        public Switch Loan { get; private set; }

        //Button
        public CustomButton Cancel { get; private set; }
        //public Button Cancel { get; private set; }

        public CustomButton Delete { get; private set; }
        //public Button Delete { get; private set; }

        public CustomButton Save { get; private set; }
        //public Button Save { get; private set; }



        //Deklaracja funkcji stworzonych Elementy
        public void Elements()
        {
            //Elementy pobierające dane z tabeli Books
            this.ID = ElementsEntryCreate();
            this.ID.Text = DirectBook.ID.ToString();

            this.Title = ElementsEntryCreate();
            this.Title.Text = DirectBook.Title;

            this.Author = ElementsEntryCreate();
            this.Author.Text = DirectBook.Author;

            this.PublicationDate = ElementsDatePicerCreate();
            this.PublicationDate.Date = DirectBook.PublicationDate.Date;

            this.StorageDevice = ElementsEntryCreate();
            this.StorageDevice.Text = DirectBook.StorageDevice;

            //Editor
            this.Description = ElementsEditorCreate();
            this.Description.Text = DirectBook.Description;
           // this.Description.HeightRequest = this.Description.Text.Length;
            this.Description.HorizontalOptions = LayoutOptions.Fill;

            //Elementy nazw danych do tabeli Books
            this.IDName = ElementsLabelNameCreate("ID:");
            this.TitleName = ElementsLabelNameCreate("Title:");
            this.AuthorName = ElementsLabelNameCreate("Author:");
            this.PublicationDateName = ElementsLabelNameCreate("Date:");
            this.FormatName = ElementsLabelNameCreate("Format:");
            this.StorageDeviceName = ElementsLabelNameCreate("Storage Device:");
            this.DescriptionName = ElementsLabelNameCreate("Description:");
            
            //Picker format
            this.Format = ElementsPicerCreate();
            this.Format.Title = DirectBook.BooksFormat;

            //bButtons
            //this.Cancel = ElementsButtonCreate("Back", CancelEventHandler);
            this.Cancel = new CustomButton(path: "iconBack.png");
            this.Cancel.Clicked(CancelTapGesture());

            //this.Delete = ElementsButtonCreate("Delete", DeleteEventHandler);
            this.Delete = new CustomButton(path: "iconDelete.png");
            this.Delete.Clicked(DeleteTapGesture());

            //this.Save = ElementsButtonCreate("Save", SaveEventHandler);
            this.Save = new CustomButton(path:"iconSave.png");
            this.Save.Clicked(SaveTapGesture());

            //Switch
            this.LoanName = ElementsLabelNameCreate("Loan:");
            this.LoanDescription = ElementsLoanDescriptionCreate();
            this.Loan = ElementsSwitchCreate(LoanToggledEventHandler);

        }


        private Entry ElementsEntryCreate()
        {
            Entry name = new Entry();
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }
        //Funkcja tworząca nazwy elementów, które zawierają dane z tabeli Books

        private Label ElementsLabelNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
        private Label ElementsLoanDescriptionCreate() {
            Label newLabel = new Label();
            if(DirectBook.Loan == false)
                newLabel.Text = "No Loan";
            else
                newLabel.Text = "Loaned";
            return newLabel;
        }

        private Editor ElementsEditorCreate()
        {
            Editor NewEditor = new Editor();
            NewEditor.TextColor = Color.Black;
            NewEditor.FontSize = 15;
            return NewEditor;
        }

        private DatePicker ElementsDatePicerCreate()
        {
            DatePicker NewDate = new DatePicker();
            NewDate.Format = "yyy-MM-dd";
            NewDate.HorizontalOptions = LayoutOptions.Center;
            return NewDate;
        }

        //Słownik opicji wyboru formatu
        Dictionary<string, int> DictionaryFormat = App.MyLittleHomeDB_BooksFormat.GetBooksFormatDictionary();


        private Picker ElementsPicerCreate()
        {
            Picker NewFormat = new Picker();
            NewFormat.Title = "Format";
            NewFormat.HorizontalOptions = LayoutOptions.Center;
            foreach (string items in DictionaryFormat.Keys)
            {
                NewFormat.Items.Add(items);
            }
            return NewFormat;
        }

        public int GetFormat()
        {
            if (Format.SelectedIndex != -1)
            {
                string ItemName = Format.Items[Format.SelectedIndex];
                FormatLicznik = DictionaryFormat[ItemName];
            }

            // return App.MyLittleHomeDB_BooksFormat.GetDirectBooksFormat(FormatLicznik);
            return FormatLicznik;
        }


        private Button ElementsButtonCreate(string tekst, EventHandler akcja)
        {
            Button NewButton = new Button();
            NewButton.Text = tekst;
            NewButton.Clicked += akcja;
            return NewButton;
        }
        /*
       private void CancelEventHandler(object sender, EventArgs e)
        {
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
        }
        */
        private TapGestureRecognizer CancelTapGesture() {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
            };
            return _tap;
        }
        /*
        private void DeleteEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Books.DeleteOneBooks(DirectBook.ID);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
            App.LocalNotification("Book deleted");
        }
        */
        private TapGestureRecognizer DeleteTapGesture() {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_Books.DeleteOneBooks(DirectBook.ID);
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
                App.LocalNotification("Book deleted");
            };
            return _tap;
        }
        /*
        private void SaveEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Books.UpgradeBooks(DirectBook.ID, Title.Text.ToString(), Author.Text.ToString(), PublicationDate.Date, GetFormat(), StorageDevice.Text.ToString(), Description.Text.ToString(),Loan.IsToggled);
            if (Loan.IsToggled == true)
            {
                App.MyLittleHomeDB_BooksLoan.CreateBooksLoan(DirectBook.ID);
            }
            if (Loan.IsToggled == false)
            {
                App.MyLittleHomeDB_BooksLoan.DeleteOneBooksLoan(DirectBook.ID);
            }
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooksEdit(DirectBook.ID));
            
            App.LocalNotification("Book edited");
        }
        */
        private TapGestureRecognizer SaveTapGesture() {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_Books.UpgradeBooks(DirectBook.ID, Title.Text.ToString(), Author.Text.ToString(), PublicationDate.Date, GetFormat(), StorageDevice.Text.ToString(), Description.Text.ToString(), Loan.IsToggled);
                if (Loan.IsToggled == true) {
                    App.MyLittleHomeDB_BooksLoan.CreateBooksLoan(DirectBook.ID);
                }
                if (Loan.IsToggled == false) {
                    App.MyLittleHomeDB_BooksLoan.DeleteOneBooksLoan(DirectBook.ID);
                }
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooksEdit(DirectBook.ID));

                App.LocalNotification("Book edited");
            };
            return _tap;
        }

        private Switch ElementsSwitchCreate(EventHandler<ToggledEventArgs> akcja) {
            Switch NewSwitch = new Switch();
            NewSwitch.Toggled += akcja;
            NewSwitch.IsToggled = DirectBook.Loan;

            return NewSwitch;
        }
        private void LoanToggledEventHandler(object sender, ToggledEventArgs e) {
            if (e.Value == false) {
                this.LoanDescription.Text = "No Loan";
            }
            else {
                this.LoanDescription.Text = "Loaned";

            }
        }
    }
}
