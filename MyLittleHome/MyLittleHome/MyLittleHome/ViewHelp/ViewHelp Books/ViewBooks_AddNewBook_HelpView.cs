﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
    public class ViewBooks_AddNewBook_HelpView
    {
        ViewBooks_AddNewBook_HelpElements Elements = new ViewBooks_AddNewBook_HelpElements();
        
        public StackLayout ViewBooks_AddNewBook()
        {
            StackLayout Stack = new StackLayout();
            
            Stack.Children.Add(GridView());
            Stack.Children.Add(GridViewButton());

           // Stack.HeightRequest = 320;
            //Stack.BackgroundColor = Color.FromHex("BDBDBD");
            Stack.BackgroundColor = Color.Yellow;
            Stack.VerticalOptions = LayoutOptions.StartAndExpand;

            return Stack;
        }/*
        public ContentPage ViewBooks_AddNewBook()
        {
            ContentPage Content = new ContentPage();
            StackLayout Stack = new StackLayout();
            ScrollView Scroll = new ScrollView() { Content = Stack };
            

            Stack.Children.Add(GridView());
            Stack.Children.Add(GridViewButton());
            Stack.BackgroundColor = Color.FromHex("BDBDBD");

            Content.Content = Scroll;

            return Content;
            
        }*/


        private Grid GridView()
        {
            Elements.Elements();

            Grid ViewGrid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 35 },
                   new RowDefinition { Height = 100 },
                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Auto },
                new ColumnDefinition { Width = GridLength.Star },

                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                HeightRequest = 275,
                VerticalOptions = LayoutOptions.Start,
                BackgroundColor = Color.Green,
            };
            
            //Tytuł
            ViewGrid.Children.Add(Elements.Get_Label_Elements("Title_Name"),0,1,0,1);
            ViewGrid.Children.Add(Elements.Get_Entry_Elements("Title"),1,7,0,1);
            //Autor
            ViewGrid.Children.Add(Elements.Get_Label_Elements("Author_Name"), 0, 1, 1, 2);
            ViewGrid.Children.Add(Elements.Get_Entry_Elements("Author"), 1, 7, 1, 2);
            //PublicationDate
            ViewGrid.Children.Add(Elements.Get_Label_Elements("PublicationDate_Name"), 0, 1, 2, 3);
            ViewGrid.Children.Add(Elements.Get_Entry_Elements("PublicationDate"), 1, 7, 2, 3);
            //Format
            ViewGrid.Children.Add(Elements.Get_Label_Elements("Format_Name"), 0, 1, 3, 4);
            ViewGrid.Children.Add(Elements.Get_Picker_Elements("Format"), 1, 7, 3, 4);
            //StorageDevice
            ViewGrid.Children.Add(Elements.Get_Label_Elements("StorageDevice_Name"), 0, 1, 4, 5);
            ViewGrid.Children.Add(Elements.Get_Entry_Elements("StorageDevice"), 1, 7, 4, 5);
            //Description
            ViewGrid.Children.Add(Elements.Get_Label_Elements("Description_Name"), 0, 1, 5, 6);
            ViewGrid.Children.Add(Elements.Get_Editor_Elements("Description"), 1, 7, 5, 6);
            
            return ViewGrid;
        }


        //Widok grida z przyciskami akcji
        private Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },

                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
                HorizontalOptions = LayoutOptions.EndAndExpand,
                BackgroundColor = Color.Red,
            };
            GridButton.Children.Add(Cancel(), 0, 1, 0, 1);
            GridButton.Children.Add(Save(), 1, 2, 0, 1);
           // GridButton.Padding = new Thickness(0, 0, 0, 30);

            return GridButton;  
        }
        
        private Button Cancel()
        {
            Button cancel = new Button();
            cancel.Text = "Cancel";
            cancel.Clicked += (object sender, EventArgs e) =>
              { 
                  App.MasterDetailPage.Detail = new NavigationPage(new View.ViewBooks());
              };

            return cancel;

        }

        public Button Save()
        {
            Button save = new Button();
            save.Text = "Save";
            save.Clicked += (object sender, EventArgs e) =>
              {
                  App.MyLittleHomeDB_Books.InsertBooks(Elements.Get_Entry_Elements("Title").Text.ToString(), Elements.Get_Entry_Elements("Author").Text.ToString(),(Convert.ToInt32(Elements.Get_Entry_Elements("PublicationDate").Text)), Elements.GetFormat(), Elements.Get_Entry_Elements("StorageDevice").Text.ToString(), Elements.Get_Editor_Elements("Description").Text.ToString());
                  App.MasterDetailPage.Detail = new NavigationPage(new View.ViewBooks());
                  App.LocalNotification("Dodano nową książkę");
              };

            return save;
        }
         

    }
}
