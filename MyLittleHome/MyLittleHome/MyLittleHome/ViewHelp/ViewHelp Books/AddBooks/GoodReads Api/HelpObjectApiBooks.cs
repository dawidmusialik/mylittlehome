﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Books.AddBooks.GoodReads_Api
{
    public class HelpObjectApiBooks
    {
        public string Title { get; set; }
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string Author { get; set; }

        public HelpObjectApiBooks(string title, string id, DateTime date, string author) {
            Title = title;
            Id = id;
            Date = date;
            Author = author;
        }
    }
}
