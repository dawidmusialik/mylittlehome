﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using Xamarin.Forms;


using Newtonsoft.Json;
using System.Xml;
using System.Xml.Linq;

namespace MyLittleHome.ViewHelp.ViewHelp_Books.AddBooks.GoodReads_Api {
    public class HelpGoodReadsView : ContentPage {
        StackLayout Stack;
        ListView newListView;
        SearchBar whatBooksSearch;
        HelpGoodReadsItemSearch itemSearchBooks;

        public HelpGoodReadsView() {
            Stack = new StackLayout();
            whatBooksSearch = new SearchBar();
            itemSearchBooks = new HelpGoodReadsItemSearch();
        

            setElementsOnView();
            this.Content = Stack;
        }

        private void setElementsOnView()
        {
            whatBooksSearch = createSearchBar("Write title", pressSearchBar);

            Stack.Children.Add(whatBooksSearch);
            Stack.Children.Add(addGridName());
            Stack.Children.Add(addListView());
        }



        #region SearchBar - dodanie do widoku, tworzenie, obsługa akcji
    
        private SearchBar createSearchBar(string placeholder, EventHandler akcja) {
            SearchBar newSearchBar = new SearchBar();
            newSearchBar.Placeholder = placeholder;
            newSearchBar.SearchButtonPressed += akcja;

            return newSearchBar;
        }
        private void pressSearchBar(object sender, EventArgs e) {
            newListView.IsRefreshing = true;
            itemSearchBooks = new HelpGoodReadsItemSearch(((SearchBar)sender).Text.ToString());
            newListView.IsRefreshing = false;
            var te = itemSearchBooks;
            newListRefreshing();
        }
        #endregion


        #region Widok grida z nazwami elementów, które są pobierane z API
        private Grid addGridName()
        {
            Grid newGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                   // new ColumnDefinition { Width = GridLength.Star },
                }
            };
            newGrid.Children.Add(new Label { Text = "Date" }, 0, 1, 0, 1);
            newGrid.Children.Add(new Label { Text = "Title" }, 1, 2, 0, 1);
            return newGrid;
        }
        #endregion


        private ListView addListView() {
            
            newListView = new ListView();
            if(itemSearchBooks.listSearchBooks!=null)
            newListView.ItemsSource = itemSearchBooks.listSearchBooks;
            newListView.ItemTemplate = new DataTemplate(typeof(dateTemplateGoodReadsListView));
            newListView.IsPullToRefreshEnabled = true;
            newListView.Refreshing += (sender, e1) => { newListRefreshing(); };
            newListView.ItemTapped += newListViewItemTapped;
            newListView.HasUnevenRows = true;
            newListView.RowHeight = -1;
            return newListView;
        }

        private void newListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            ViewHelp_Books.AddBooks.HelpView.Elements.Title.Text = ((HelpObjectApiBooks)e.Item).Title;
            ViewHelp_Books.AddBooks.HelpView.Elements.PublicationDate.Date = ((HelpObjectApiBooks)e.Item).Date;
            ViewHelp_Books.AddBooks.HelpView.Elements.Author.Text = ((HelpObjectApiBooks)e.Item).Author;
            ViewHelp_Books.AddBooks.HelpView.Elements.Description.Text = getDescriptionsFromGoodReads(((HelpObjectApiBooks)e.Item).Id);
            App.MasterDetailPage.Detail.Navigation.PopModalAsync();
            App.LocalNotification("Download data");
        }

        private void newListRefreshing() {
            if (itemSearchBooks.listSearchBooks != null)
                newListView.ItemsSource = itemSearchBooks.listSearchBooks;
            newListView.EndRefresh();
        }

        private string getDescriptionsFromGoodReads(string idBooks)
        {
          XDocument connectGoodReadsSearchOneBook;
          IEnumerable<XElement> descriptionGoodReads;


            connectGoodReadsSearchOneBook = XDocument.Load("https://www.goodreads.com/book/show/" + idBooks + ".xml?key=" + Api.ApiKeySingleton.Instance.getGoodReadsApiKey());
            descriptionGoodReads = connectGoodReadsSearchOneBook.Root.Descendants("description"); //Opis jednej książki po jej id
            if (descriptionGoodReads.ToList()[0] != null)
            {
                return descriptionGoodReads.ToList()[0].Value;

            }

            return "No result";

        }

    }


    public class dateTemplateGoodReadsListView : ViewCell
    {
        public Label yearMovies { get; private set; }
        public Label titleMovies { get; private set; }

        #region Konstruktor domyślny
        public dateTemplateGoodReadsListView() : base()
        {
            View = setGridData();
        }
        #endregion

        #region Grid z bindowanymi elementami API
        private Grid setGridData()
        {
            createElements();
            Grid newGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                }
            };

            newGrid.Children.Add(yearMovies, 0, 1, 0, 1);
            newGrid.Children.Add(titleMovies, 1, 2, 0, 1);
            return newGrid;
        }
        #endregion

        #region Wywołąnie tworzenia elementów
        private void createElements()
        {
            yearMovies = createLabelDate("Date");
            titleMovies = createLabelTitle("Title");
        }
        #endregion

        #region Metody tworzące elementy
        private Label createLabelTitle(string property)
        {
            Label newLabel = new Label();
            newLabel.SetBinding(Label.TextProperty, new Binding(path: property));
            return newLabel;
        }
        private Label createLabelDate(string property)
        {
            Label newLabel = new Label();
            newLabel.SetBinding(Label.TextProperty, new Binding(path: property, stringFormat: "{0:yyyy-MM-dd}"));
            newLabel.BindingContextChanged += NewLabel_BindingContextChanged;
            return newLabel;
        }

        private void NewLabel_BindingContextChanged(object sender, EventArgs e)
        {
            if (((Label)sender).Text == "")
                ((Label)sender).Text = "No data";
        }
        #endregion
    }


}
