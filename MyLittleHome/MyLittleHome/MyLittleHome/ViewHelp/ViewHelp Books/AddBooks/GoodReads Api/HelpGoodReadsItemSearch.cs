﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using Xamarin.Forms;


using Newtonsoft.Json;
using System.Xml;
using System.Xml.Linq;
namespace MyLittleHome.ViewHelp.ViewHelp_Books.AddBooks.GoodReads_Api
{
   public class HelpGoodReadsItemSearch
    {
        //Pomocnicze zmienne do zapisywania w liscie z datami
        private int year;
        private int month;
        private int day;

        //Łączenie z api (podanie wartości http)
        private XDocument connectGoodReadsSearch;

        //Kontenery na dane z api
        private IEnumerable<XElement> titleGoodReads;
        private IEnumerable<XElement> idGoodReads;
        private IEnumerable<XElement> authorGoodReads;

        private IEnumerable<XElement> publicationYearGoodReads;
        private IEnumerable<XElement> publicationMonthGoodReads;
        private IEnumerable<XElement> publicationDayGoodReads;

        //Listy do których zostaną przypisane elementy z kontenerów
        private List<string> listOfTitle; //Lista wszystkich tytułów
        private List<string> listOfID; //Lista wszystkich id
        private List<string> listOfAuthors; //Lista wszystkich autorów
        private List<DateTime> listOfBooksPublicationDate;//Lista wszystkich dat

        public List<HelpObjectApiBooks> listSearchBooks; //Lista obiektów książek (połączone poprzednie listy)

        private int numberOfDownloadBook;
        private string nameBooks;
        public HelpGoodReadsItemSearch()
        {

        }

        public HelpGoodReadsItemSearch(string booksTitle) {
            clearHelpVariableFromDate();
            nameBooks = booksTitle;
            connectGoodReadsSearch = XDocument.Load("https://www.goodreads.com/search.xml?key=" + Api.ApiKeySingleton.Instance.getGoodReadsApiKey() + "&q=" + nameBooks);
            var te = connectGoodReadsSearch.Root.Descendants("results-end").ToList()[0].Value;
            //Jeżeli podczas wyszukwiania był chociaż jeden rekord
            if(Convert.ToInt32(connectGoodReadsSearch.Root.Descendants("results-end").ToList()[0].Value)!=0) 
            {
                downloadDataFromApi();//Pobranie danych z api do kontenerów XML
                createListForApiData();//Tworzenie list na przerobione dane z XML'a
                numberOfDownloadBook = titleGoodReads.ToList().Count;//Zapisanie ilości pobranych rekordów (wczytanie ile było tytułów)
                writeDataOnAllList();//Zapsianie rekordów z XML'a dl list
                createAndWriteDataonObjectListBooks();//Utworzenie i zapisanie obiektów książek, za pomocą wartości z poprzednich list
                App.LocalNotification("Search data succes");
            }
            //Jeżeli podczas wyszukiwania niczego nie znaleziono
            else 
            {
                listSearchBooks = new List<HelpObjectApiBooks>();
                listSearchBooks.Add(new HelpObjectApiBooks("No Results", "No results", new DateTime(0001, 1, 1), "No result"));
                App.LocalNotification("No results. Try again!");
            }
            
        }

        //Pobranie danych z Api do kontenerów XML'a
        private void downloadDataFromApi() 
        {
            titleGoodReads = connectGoodReadsSearch.Root.Descendants("title");//Wszystkie tytuly książek po wyszukwianiu
            idGoodReads = connectGoodReadsSearch.Root.Descendants("best_book").Select(x => x.Element("id"));//Wszystkie id książek po wyszukiwaniu
            authorGoodReads = connectGoodReadsSearch.Root.Descendants("author").Select(x=>x.Element("name"));
            publicationYearGoodReads = connectGoodReadsSearch.Root.Descendants("original_publication_year");//Wszystkie daty roku książek po wyszukiwaniu
            publicationMonthGoodReads = connectGoodReadsSearch.Root.Descendants("original_publication_month");//Wszystkie daty miseiąca książek po wyszukiwaniu
            publicationDayGoodReads = connectGoodReadsSearch.Root.Descendants("original_publication_day");//Wszystkie daty dnia książek po wyszukiwaniu
        }

        //Utworzenie list w których będą przechowywane dane z kontenerów jako wartości, oraz utworzenie listy obiektów książek która te wartości przechowuje
        private void createListForApiData() 
        {
            listOfTitle = new List<string>();
            listOfID = new List<string>();
            listOfAuthors = new List<string>();
            listOfBooksPublicationDate = new List<DateTime>();
            listSearchBooks = new List<HelpObjectApiBooks>();
        }

        //Funkcja wywolująca funkcję zapisujące dane do list z wartościami konternerów
        private void writeDataOnAllList() 
        {
            writeDataOnTitleList();
            writeDataOnIDList();
            writeDataOnDateList();
            writeDataOnAuthorList();
        }

        //Funkcja zapisująca tytuły z kontenerów XML'a od listy (string) tytułów
        private void writeDataOnTitleList() 
        {
            var listOfBooksTitle = titleGoodReads.ToList();
            foreach (var i in listOfBooksTitle) {
                listOfTitle.Add(i.Value);
            }
        }

        //Funkcja zapisująca id z kontenerów XML'a od listy (string) id
        private void writeDataOnIDList() 
        {
            var listOfBooksID = idGoodReads.ToList();
            foreach (var i in listOfBooksID) {
                listOfID.Add(i.Value);
            }
        }
        //Funkcja zapisująca author z kontenerów XML'a od listy (string) id
        private void writeDataOnAuthorList()
        {
            var listOfBooksAuthor = authorGoodReads.ToList();
            foreach (var i in listOfBooksAuthor)
            {
                listOfAuthors.Add(i.Value);
            }
        }

        //Funkcja zapisująca daty z kontenerów XML'a od listy (DateTime) dat
        private void writeDataOnDateList() 
        {
            var listOfYearPublicationDate = publicationYearGoodReads.ToList();
            var listOfMonthPublicationDate = publicationMonthGoodReads.ToList();
            var listOfDayPublicationDate = publicationDayGoodReads.ToList();

            for (int i = 0; i < numberOfDownloadBook; i++) {
                if (listOfYearPublicationDate[i].Value == "") { year = 0001; }
                else {
                    if (Convert.ToInt32(listOfYearPublicationDate[i].Value) > 1)
                        year = Convert.ToInt32(listOfYearPublicationDate[i].Value);
                    else
                        year = 0001;
                }


                if (listOfMonthPublicationDate[i].Value == "") { month = 1; }
                else {
                    if (Convert.ToInt32(listOfMonthPublicationDate[i].Value) > 1)
                        month = Convert.ToInt32(listOfMonthPublicationDate[i].Value);
                    else
                        year = 0001;
                }

                if (listOfDayPublicationDate[i].Value == "") { day = 1; }
                else {
                    if (Convert.ToInt32(listOfMonthPublicationDate[i].Value) > 1)
                        day = Convert.ToInt32(listOfDayPublicationDate[i].Value);
                    else
                        year = 0001;
                }


                listOfBooksPublicationDate.Add(new DateTime(year, month, day));
            }
            clearHelpVariableFromDate();
        }

        //Czyszczenie pomocniczych zmiennych do daty
        private void clearHelpVariableFromDate() {
            year = 0001;
            month = 1;
            day = 1;
        }
        //Funkcja tworząca nowe obiekty książek i zapisująca do nich dane z pozostałych list
        private void createAndWriteDataonObjectListBooks() 
        {
            for (int i = 0; i < numberOfDownloadBook; i++) {
                listSearchBooks.Add(new HelpObjectApiBooks(listOfTitle[i], listOfID[i], listOfBooksPublicationDate[i],listOfAuthors[i]));
            }
        }
    }
}
