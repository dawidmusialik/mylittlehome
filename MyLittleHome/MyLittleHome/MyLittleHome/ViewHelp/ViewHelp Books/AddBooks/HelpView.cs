﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Books.AddBooks
{
    public class HelpView  : ContentPage
    {
       public static HelpViewElements Elements;
        public HelpView()
        {
            Title = " Add new books";
            Elements = new HelpViewElements();
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();
            ScrollView Scroll = new ScrollView { Content = AddBooksView() };
           // Scroll.BackgroundColor = Color.FromHex("BDBDBD");
            this.Content = Scroll;
        }

        private Grid AddBooksView()
        {
            Grid AddBooks = new Grid();
            AddBooks.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            AddBooks.RowDefinitions.Add(new RowDefinition { Height = GridLength.Star });

            AddBooks.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });

            AddBooks.Children.Add(GridView(), 0, 1, 0, 1);
            AddBooks.Children.Add(GridViewButton(), 0, 1, 1, 2);
            return AddBooks;
        }


        private Grid GridView()
        {

            Grid ViewGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = 160 },

                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
             //   VerticalOptions = LayoutOptions.Start,
            };



            ViewGrid.Children.Add(Elements.TitleName, 0, 1, 0, 1);
            ViewGrid.Children.Add(Elements.Title, 1, 2, 0, 1);
            ViewGrid.Children.Add(Elements.Search, 2, 3, 0, 1);

            ViewGrid.Children.Add(Elements.AuthorName, 0, 1, 1, 2);
            ViewGrid.Children.Add(Elements.Author, 1, 3, 1, 2);

            ViewGrid.Children.Add(Elements.PublicationDateName, 0, 1, 2, 3);
            ViewGrid.Children.Add(Elements.PublicationDate, 1, 3, 2, 3);

            ViewGrid.Children.Add(Elements.FormatName, 0, 1, 3, 4);
            ViewGrid.Children.Add(Elements.Format, 1, 3, 3, 4);

            ViewGrid.Children.Add(Elements.StorageDeviceName, 0, 1, 4, 5);
            ViewGrid.Children.Add(Elements.StorageDevice, 1, 3, 4, 5);
            
            ViewGrid.Children.Add(Elements.DescriptionName, 0, 3, 5, 6);
            ViewGrid.Children.Add(Elements.Description, 0, 3, 6, 7); 

            return ViewGrid;
        }


        //Widok grida z przyciskami akcji
        private Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },
                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
              //  VerticalOptions = LayoutOptions.EndAndExpand,
               // HorizontalOptions = LayoutOptions.EndAndExpand,
            };
            GridButton.Children.Add(Elements.Cancel, 0, 1, 0, 1);
            GridButton.Children.Add(Elements.Save, 1, 2, 0, 1);

            return GridButton;
        }
    }
}
