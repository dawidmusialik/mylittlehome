﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Books.AddBooks
{
    public class HelpViewElements
    {
        public HelpViewElements()
        {
            Elements();
        }
        public Entry Title { get; private set; }
        public Entry Author { get; private set; }
       // public Entry PublicationDate { get; private set; }
        public Entry StorageDevice { get; private set; }


        public Label TitleName { get; private set; }
        public Label AuthorName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }

        public Picker Format { get; private set; }
        private int FormatLicznik = 0;

        public Editor Description { get; private set; }

        public CustomButton Cancel { get; private set; }
        //public Button Cancel { get; private set; }
        public CustomButton Save { get; set; }
        //public Button Save { get; private set; }
        public CustomButton Search { get; private set; }
        //public Button Search { get; private set; }

        public DatePicker PublicationDate { get; private set; }




        //Publiczna funkcja tworząca wszystkie elementy
        private void Elements()
        {

            this.Title = ElementsEntryCreate("Title");
            this.Author = ElementsEntryCreate("Author");
            this.Format = ElementsPicerCreate(DictionaryFormat);
            this.PublicationDate = ElementsDatePicerCreate();
            this.StorageDevice = ElementsEntryCreate("StorageDevice");
            this.Description = ElementsEditorCreate();

            this.TitleName = ElementsNameCreate("Title:");
            this.AuthorName = ElementsNameCreate("Author:");
            this.PublicationDateName = ElementsNameCreate("Date:");
            this.FormatName = ElementsNameCreate("Format");
            this.StorageDeviceName = ElementsNameCreate("Storage Device:");
            this.DescriptionName = ElementsNameCreate("Description:");

            //this.Cancel = ElementsButtonCreate("Cancel", ButtonCancelEventHandler);
            this.Cancel = new CustomButton(path: "iconBack.png");
            this.Cancel.Clicked(ButtonCancelTapGesture());

            //this.Save = ElementsButtonCreate("Save", ButtonSaveEventHandler);
            this.Save = new CustomButton(path: "iconSave.png");
            this.Save.Clicked(ButtonSaveTapGesture());

            //this.Search = ElementsButtonCreate("GD", ButtonSearchEventHandler);
            //this.Search.FontSize = 10;
            this.Search = new CustomButton(path:"iconGoodReads.png");
            this.Search.Clicked(ButtonSearchTapGesture());
            this.Search.WidthRequest = 50;
            this.Search.HeightRequest = 15;
        }

        //Prywatna funkcja tworząca elementy entry do wpisywania danych
        private Entry ElementsEntryCreate(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.PlaceholderColor = Color.Black;
            name.FontSize = 15;
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }

        //Prywatna funkcja tworząca nowe elementy nazw
        private Label ElementsNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

        //Słownik opicji wyboru formatu
        Dictionary<string, int> DictionaryFormat = App.MyLittleHomeDB_BooksFormat.GetBooksFormatDictionary();



        private Picker ElementsPicerCreate(Dictionary<string, int> dictionary)
        {
            Picker NewFormat = new Picker();
            NewFormat.Title = "Format";
            NewFormat.HorizontalOptions = LayoutOptions.Center;
            foreach (string items in dictionary.Keys)
            {
                NewFormat.Items.Add(items);
            }
            return NewFormat;
        }
        private DatePicker ElementsDatePicerCreate()
        {
            DatePicker NewDate = new DatePicker();
            NewDate.Format = "yyy-MM-dd";
            NewDate.HorizontalOptions = LayoutOptions.Center;
            return NewDate;
        }

        public int GetFormat()
        {
            if (Format.SelectedIndex != -1)
            {
                string ItemName = Format.Items[Format.SelectedIndex];
                FormatLicznik = DictionaryFormat[ItemName];
            }

            return FormatLicznik;
        }

        //Funkcja torząca pole editor do wprowadzania notatek
        private Editor ElementsEditorCreate()
        {
            Editor NewEditor = new Editor();

            NewEditor.TextColor = Color.Black;
            NewEditor.FontSize = 15;
            NewEditor.HorizontalOptions = LayoutOptions.FillAndExpand;
            NewEditor.VerticalOptions = LayoutOptions.FillAndExpand;
            NewEditor.BackgroundColor  = Color.FromHex("CCCCCC");

            return NewEditor;
        }





        private Button ElementsButtonCreate(string text, EventHandler akcja)
        {
            Button name = new Button();
            name.Text = text;
            name.Clicked += akcja;
            return name;
        }
        /*
        private void ButtonCancelEventHandler(object sender, EventArgs e)
        {
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
        }
        */
        private TapGestureRecognizer ButtonCancelTapGesture() {
            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
            };
            return tap;
        }
        /*
        private void ButtonSaveEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Books.InsertBooks(Title.Text.ToString(), Author.Text.ToString(),PublicationDate.Date, GetFormat(), StorageDevice.Text.ToString(), Description.Text.ToString(),false);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
            App.LocalNotification("New book added");
        }
        */
        private TapGestureRecognizer ButtonSaveTapGesture() {
            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) => {
                if (Title != null && Author != null && PublicationDate.Date != null && GetFormat() != 0 && StorageDevice != null && Description != null) 
                {
                    App.MyLittleHomeDB_Books.InsertBooks(Title.Text.ToString(), Author.Text.ToString(), PublicationDate.Date, GetFormat(), StorageDevice.Text.ToString(), Description.Text.ToString(), false);
                    App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Books.ViewBooks());
                    App.LocalNotification("New book added");
                }
                else 
                {
                    App.LocalNotification("Check the boxes");
                }

            };
            return tap;
        }
        /*
        private void ButtonSearchEventHandler(object sender, EventArgs e)
        {
            if(CrossConnectivity.Current.IsConnected == true)
            {
                App.MasterDetailPage.Detail.Navigation.PushModalAsync(new ViewHelp.ViewHelp_Books.AddBooks.GoodReads_Api.HelpGoodReadsView());
            }
            else
            {
                App.LocalNotification("Check the internet connections");
            }
        }
        */
        private TapGestureRecognizer ButtonSearchTapGesture() {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (CrossConnectivity.Current.IsConnected == true && Api.ApiKeySingleton.Instance.getGoodReadsApiKey() != "") {
                    App.MasterDetailPage.Detail.Navigation.PushModalAsync(new ViewHelp.ViewHelp_Books.AddBooks.GoodReads_Api.HelpGoodReadsView());
                }
                else {
                    App.LocalNotification("Check the internet connections or Api Key");
                }
            };
            return _tap;
        }
    }
}
