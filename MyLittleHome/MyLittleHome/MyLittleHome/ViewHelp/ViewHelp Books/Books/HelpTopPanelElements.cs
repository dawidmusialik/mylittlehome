﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Books.Books
{
    public class HelpTopPanelElements
    {
        public Label TitlePage { get; private set; }
        //public Button ButtonAddBook { get; private set; }
        public CustomButton ButtonEditBook { get; set; }
        public CustomButton ButtonAddBook { get; set; }


        public HelpTopPanelElements()
        {
            CreateElements();
        }

        private void CreateElements()
        {
            this.TitlePage = CreateLabel("My Books");
            //this.ButtonAddBook = CreateButton("Add", ButtonAddBookEventHandler);
            this.ButtonAddBook = new CustomButton(path: "iconAdd.png");
            this.ButtonAddBook.Clicked(ButtonAddBookEventHandler());
            //this.ButtonEditBook = CreateButton("Edit Book", null);
            this.ButtonEditBook = new CustomButton(path:"iconEdit.png");
        }

        private Label CreateLabel(string tekst)
        {
            Label Title = new Label();
            Title.Text = tekst;
            Title.FontSize = 30;
            Title.TextColor = Color.FromHex("57FF9D");
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.VerticalOptions = LayoutOptions.Center;
            Title.HorizontalOptions = LayoutOptions.Center;
            return Title;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button button = new Button();
            button.Text = tekst;
            button.Clicked += akcja;
            button.VerticalOptions = LayoutOptions.End;
            button.HorizontalOptions = LayoutOptions.End;

            return button;
        }

        private TapGestureRecognizer ButtonAddBookEventHandler()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Books.ViewBookAdd());
            };
            // App.MasterDetailPage.Detail = new NavigationPage(new View.Books.ViewBookAdd());
            //App.MasterDetailPage.Detail.Navigation.PushModalAsync(new View.Books.ViewBookAdd());
            return _tap;
        }




    }
}
