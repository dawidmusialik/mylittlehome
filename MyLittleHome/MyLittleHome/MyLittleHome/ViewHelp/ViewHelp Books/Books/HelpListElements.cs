﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Books.Books
{
    public class HelpListElements
    {
        public static int Number=0;

        public HelpListElements()
        {
            Elements();
        }
        
        public Label ID { get; private set; }
        public Label Title { get; private set; }
        public Label Author { get; private set; }
        public Label PublicationDate { get; private set; }
        public Label Format { get; private set; }
        public Label StorageDevice { get; private set; }
        public Label Description { get; private set; }
        public Label Loan { get; private set; }


        //Elementy na nazwy danych z tabeli Books
        public Label IDName { get; private set; }
        public Label TitleName { get; private set; }
        public Label AuthorName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }
        public Label LoanName { get; private set; }

        public Label NumberName { get; private set; }



        //Deklaracja funkcji stworzonych Elementy
        private void Elements()
        {
            //Elementy pobierające dane z tabeli Books
            this.ID = ElementsLabelCreate("ID");
            this.Title = ElementsLabelCreate("Title");
            this.Author = ElementsLabelCreate("Author");
            this.PublicationDate = ElementsLabelCreate("PublicationDate");
            this.Format = ElementsLabelCreate("BooksFormat");
            this.StorageDevice = ElementsLabelCreate("StorageDevice");
            this.Description = ElementsLabelCreate("Description");
            this.Loan = ElementsLabelCreate("LoanToSting");
 

            this.IDName = ElementsLabelNameCreate("Nr:");
            this.TitleName = ElementsLabelNameCreate("Title:");
            this.AuthorName = ElementsLabelNameCreate("Author:");
            this.PublicationDateName = ElementsLabelNameCreate("Date:");
            this.FormatName = ElementsLabelNameCreate("Format:");
            this.StorageDeviceName = ElementsLabelNameCreate("Storage Device:");
            this.DescriptionName = ElementsLabelNameCreate("Description:");
            this.LoanName = ElementsLabelNameCreate("Loan:");

            this.NumberName = ElementsLabelNameCreate("");
        }


        private Label ElementsLabelCreate(string property)
        {
            Label name = new Label();
            
            if (property == "PublicationDate")
            {
                name.SetBinding(Label.TextProperty, new Binding { Path = property, StringFormat =  "{0:yyyy-MM-dd}"  });
            }
           else
            {
                name.SetBinding(Label.TextProperty, new Binding { Path = property });
            }
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

        private Label ElementsLabelNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
    }
}
