﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
    public class ViewBooksFormatHelpViewListFormat : ViewCell
    {
        ViewHelp.ViewBooksFormatHelpElementsViewList Elements;

        public ViewBooksFormatHelpViewListFormat()
        {
            Elements = new ViewBooksFormatHelpElementsViewList();
            this.View = AllRow();
        }



        private StackLayout AllRow()
        {
            StackLayout Stack = new StackLayout();
            Stack.Children.Add(RowID());
            Stack.Children.Add(RowFormat());
            Stack.Orientation = StackOrientation.Horizontal;
            return Stack;
        }

        private Grid RowID()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.IDName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.ID, 1, 2, 0, 1);

            return Stack;
        }
        private Grid RowFormat()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.FormatName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.Format, 1, 2, 0, 1);

            return Stack;
        }


    }
}
