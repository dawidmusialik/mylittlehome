﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Music.LoanMusic
{
    public class HelpView : ContentPage
    {
        HelpViewElements Elements;
        public HelpView(DataBase.TableMusicWithConnectedTable OneMusic)
        {
            Elements = new HelpViewElements(OneMusic);
            this.Title = "Edit Loan";
            StackLayout Stack = new StackLayout();
            Stack.Children.Add(GridView());
            Stack.Children.Add(GridViewButton());
            Stack.BackgroundColor = Color.FromHex("BDBDBD");

            this.Content = Stack;

        }

        public Grid GridView()
        {

            Grid ViewGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },


                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                //   VerticalOptions = LayoutOptions.Start,
            };



            ViewGrid.Children.Add(Elements.DateLoanName, 0, 1, 0, 1);
            ViewGrid.Children.Add(Elements.DateLoan, 1, 2, 0, 1);

            ViewGrid.Children.Add(Elements.DateReceiptName, 0, 1, 1, 2);
            ViewGrid.Children.Add(Elements.DateReceipt, 1, 2, 1, 2);

            ViewGrid.Children.Add(Elements.WhoName, 0, 1, 2, 3);
            ViewGrid.Children.Add(Elements.Who, 1, 2, 2, 3);

            return ViewGrid;
        }

        public Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },
                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
                HorizontalOptions = LayoutOptions.EndAndExpand,
            };
            GridButton.Children.Add(Elements.Cancel, 0, 1, 0, 1);
            GridButton.Children.Add(Elements.Save, 1, 2, 0, 1);
            GridButton.Children.Add(Elements.Delete, 2, 3, 0, 1);

            GridButton.Padding = new Thickness(0, 0, 0, 5);

            return GridButton;
        }

    }
}
