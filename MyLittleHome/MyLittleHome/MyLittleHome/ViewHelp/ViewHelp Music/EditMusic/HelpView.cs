﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Music.EditMusic
{
    public class HelpView:ContentPage
    {
        HelpViewElements Elements;
        public HelpView(DataBase.TableMusicWithConnectedTable OneMusic)
        {
            Title = "Edit Music";
            StackLayout BooksViewEdit = new StackLayout();
            Elements = new HelpViewElements(OneMusic);


            BooksViewEdit.Children.Add(GridView());
            BooksViewEdit.Children.Add(GridViewButton());


            ScrollView Scroll = new ScrollView { Content = BooksViewEdit };
            Scroll.BackgroundColor = Color.FromHex("BDBDBD");

            this.Content = Scroll;
        }




        public Grid GridView()
        {

            Grid ViewGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                  // new RowDefinition { Height = 160 },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },


                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                //   VerticalOptions = LayoutOptions.Start,
            };



            ViewGrid.Children.Add(Elements.AlbumName, 0, 1, 0, 1);
            ViewGrid.Children.Add(Elements.Album, 1, 2, 0, 1);

            ViewGrid.Children.Add(Elements.AuthorName, 0, 1, 1, 2);
            ViewGrid.Children.Add(Elements.Author, 1, 2, 1, 2);

            ViewGrid.Children.Add(Elements.NumberOfSongsName, 0, 1, 2, 3);
            ViewGrid.Children.Add(Elements.NumberOfSongs, 1, 2, 2, 3);

            ViewGrid.Children.Add(Elements.PublicationDateName, 0, 1, 3, 4);
            ViewGrid.Children.Add(Elements.PublicationDate, 1, 2, 3, 4);

            ViewGrid.Children.Add(Elements.FormatName, 0, 1, 4, 5);
            ViewGrid.Children.Add(Elements.Format, 1, 2, 4, 5);

            ViewGrid.Children.Add(Elements.StorageDeviceName, 0, 1, 5, 6);
            ViewGrid.Children.Add(Elements.StorageDevice, 1, 2, 5, 6);

            ViewGrid.Children.Add(Elements.LoanName, 0, 1, 6, 7);
            ViewGrid.Children.Add(Elements.LoanDescription, 1, 2, 6, 7);
            ViewGrid.Children.Add(Elements.Loan, 1, 2, 6, 7);

            ViewGrid.Children.Add(Elements.DescriptionName, 0, 2, 7, 8);
            ViewGrid.Children.Add(Elements.Description, 0, 2, 8, 9);

            return ViewGrid;
        }



        //Widok grida z przyciskami akcji
        public Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },

                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
                HorizontalOptions = LayoutOptions.EndAndExpand,
            };
            GridButton.Children.Add(Elements.Cancel, 0, 1, 0, 1);
            GridButton.Children.Add(Elements.Save, 1, 2, 0, 1);
            GridButton.Children.Add(Elements.Delete, 2, 3, 0, 1);

            GridButton.Padding = new Thickness(0, 0, 0, 5);

            return GridButton;
        }

    }
}
