﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Music.Music
{
   public class HelpListView: ViewCell
    {
        HelpListElements Elements;

        public HelpListView()
        {
            Elements = new HelpListElements();
            this.View = GridView();
        }


        private Grid GridView()
        {
            HelpListElements.Number++;
            Elements.NumberName.Text = HelpListElements.Number.ToString();

            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto },

                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };
            grid.Children.Add(IDinGrid(), 0, 1, 2, 3);
            grid.Children.Add(LoaninGrid(), 1, 2, 2, 3);

            grid.Children.Add((AlbuminGrid()), 0, 8, 0, 1);
            grid.Children.Add((AuthorinGrid()), 0, 8, 1, 2);


            grid.Children.Add(PublicationDateinGrid(), 2, 3, 2, 3);
            grid.Children.Add(FormatinGrid(), 3, 4, 2, 3);
            grid.Children.Add((NumberofSongseinGrid()), 4, 5, 2, 3);

            grid.Children.Add(StorageDeviceinGrid(), 5, 8, 2, 3);
            grid.Children.Add(DescriptioninGrid(), 0, 8, 4, 9);
            return grid;
        }

        private StackLayout IDinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.IDName);
            Stack.Children.Add(Elements.ID);

            return Stack;
        }

        private StackLayout AlbuminGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //   Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.AlbumName);
            Stack.Children.Add(Elements.Album);

            return Stack;
        }
        private StackLayout AuthorinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //  Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.AuthorName);
            Stack.Children.Add(Elements.Author);

            return Stack;
        }
        private StackLayout PublicationDateinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Sienna;
            Stack.Children.Add(Elements.PublicationDateName);
            Stack.Children.Add(Elements.PublicationDate);

            return Stack;
        }
        private StackLayout FormatinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.FormatName);
            Stack.Children.Add(Elements.Format);

            return Stack;
        }



        private StackLayout StorageDeviceinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.StorageDeviceName);
            Stack.Children.Add(Elements.StorageDevice);

            return Stack;
        }

        private StackLayout NumberofSongseinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.NumberOfSongsName);
            Stack.Children.Add(Elements.NumberOfSongs);

            return Stack;
        }

        private StackLayout DescriptioninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.BackgroundColor = Color.FromHex("CCCCCC");
            Stack.Children.Add(Elements.DescriptionName);
            Stack.Children.Add(Elements.Description);

            return Stack;
        }
        private StackLayout LoaninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.Children.Add(new Label { Text = "Loan:", TextColor = Color.Red, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center });
            Stack.Children.Add(Elements.Loan);

            return Stack;
        }
    }
}
