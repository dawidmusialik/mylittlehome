﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Music.Music
{
    public class HelpTopPanelElements
    { 
        public Label TitlePage { get; private set; }
        //public Button ButtonAddMusic { get; private set; }
        //public Button ButtonEditMusic { get; set; }

        public CustomButton ButtonEditMusic { get; set; }
        public CustomButton ButtonAddMusic { get; private set; }

        public HelpTopPanelElements()
        {
            CreateElements();
        }

        private void CreateElements()
        {
            this.TitlePage = CreateLabel("My Music");

            this.ButtonAddMusic = new CustomButton(path: "iconAdd.png");
            this.ButtonAddMusic.Clicked(ButtonAddMusicEventHandler());

            this.ButtonEditMusic = new CustomButton(path: "iconEdit.png");

           // this.ButtonAddMusic = CreateButton("Add", ButtonAddMusicEventHandler);
           // this.ButtonEditMusic = CreateButton("Edit Music", null);
        }

        private Label CreateLabel(string tekst)
        {
            Label Title = new Label();
            Title.Text = tekst;
            Title.FontSize = 30;
            Title.TextColor = Color.FromHex("57FF9D");
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.VerticalOptions = LayoutOptions.Center;
            Title.HorizontalOptions = LayoutOptions.Center;
            return Title;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button button = new Button();
            button.Text = tekst;
            button.Clicked += akcja;
            button.VerticalOptions = LayoutOptions.End;
            button.HorizontalOptions = LayoutOptions.End;

            return button;
        }

        private TapGestureRecognizer ButtonAddMusicEventHandler()
        {

            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Music.ViewMusicAdd());
            };
            return _tap;
            
        }
    }
}
