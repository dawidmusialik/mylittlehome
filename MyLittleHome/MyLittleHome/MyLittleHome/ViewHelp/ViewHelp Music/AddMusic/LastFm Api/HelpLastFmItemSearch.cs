﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Xml;
using System.Xml.Linq;
namespace MyLittleHome.ViewHelp.ViewHelp_Music.AddMusic.LastFm_Api
{
   public class HelpLastFmItemSearch
    {
        public List<HelpLastFMObjectsAlbumsSearch> listOfAlbums;
        ViewHelp_Music.AddMusic.LastFm_Api.AlbumSearch.HelpLastFMJsonAlbumsSearch deserializeJsonAlbumsSearch;
        public HelpLastFmItemSearch()
        {
            listOfAlbums = new List<HelpLastFMObjectsAlbumsSearch>();
        }
        public HelpLastFmItemSearch(string albumName)
        {
            deserializeJsonAlbumsSearch = new AlbumSearch.HelpLastFMJsonAlbumsSearch();
            listOfAlbums = new List<HelpLastFMObjectsAlbumsSearch>();
            getDataFromApi(albumName);
            writeDataFromApiOnListOfAlbums();
        }
        private void getDataFromApi(string albumName)
        {
            string _albumsSearchURL = "http://ws.audioscrobbler.com/2.0/?method=album.search&album=" + albumName + "&api_key=" + Api.ApiKeySingleton.Instance.getLastFmApiKey() + "&format=json";
            HttpClient albumSearchClient = new HttpClient();
            var jsonAlbumsSearch = albumSearchClient.GetStringAsync(_albumsSearchURL);
            deserializeJsonAlbumsSearch = JsonConvert.DeserializeObject<ViewHelp_Music.AddMusic.LastFm_Api.AlbumSearch.HelpLastFMJsonAlbumsSearch>(jsonAlbumsSearch.Result);
        }
        private void writeDataFromApiOnListOfAlbums()
        {
            if (deserializeJsonAlbumsSearch.Results.Albummatches.Album.Count != 0)
            {
                foreach (var item in deserializeJsonAlbumsSearch.Results.Albummatches.Album)
                {
                    listOfAlbums.Add(new HelpLastFMObjectsAlbumsSearch(item.Name, item.Artist));
                }
            }
        }
    }
}
