﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.ViewHelp.ViewHelp_Music.AddMusic.LastFm_Api.AlbumSearch
{
    public class HelpLastFMJsonAlbumsSearch
    {

        [JsonProperty("results")]
        public Results Results { get; set; }
    }
    public class Results
    {

        [JsonProperty("opensearch:Query")]
        public OpensearchQuery OpensearchQuery { get; set; }

        [JsonProperty("opensearch:totalResults")]
        public string OpensearchTotalResults { get; set; }

        [JsonProperty("opensearch:startIndex")]
        public string OpensearchStartIndex { get; set; }

        [JsonProperty("opensearch:itemsPerPage")]
        public string OpensearchItemsPerPage { get; set; }

        [JsonProperty("albummatches")]
        public Albummatches Albummatches { get; set; }

        [JsonProperty("@attr")]
        public Attr Attr { get; set; }
    }

    public class Attr
    {

        [JsonProperty("for")]
        public string For { get; set; }
    }

    public class Albummatches
    {

        [JsonProperty("album")]
        public IList<Album> Album { get; set; }
    }

    public class Album
    {

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("artist")]
        public string Artist { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("image")]
        public IList<Image> Image { get; set; }

        [JsonProperty("streamable")]
        public string Streamable { get; set; }

        [JsonProperty("mbid")]
        public string Mbid { get; set; }
    }

    public class Image
    {

        [JsonProperty("#text")]
        public string Text { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }
    }

    public class OpensearchQuery
    {

        [JsonProperty("#text")]
        public string Text { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("searchTerms")]
        public string SearchTerms { get; set; }

        [JsonProperty("startPage")]
        public string StartPage { get; set; }
    }

}
