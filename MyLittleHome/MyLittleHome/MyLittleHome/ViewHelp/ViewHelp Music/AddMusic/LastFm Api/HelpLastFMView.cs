﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using Xamarin.Forms;


using Newtonsoft.Json;
using System.Xml;
using System.Xml.Linq;

namespace MyLittleHome.ViewHelp.ViewHelp_Music.AddMusic.LastFm_Api{
    
    public class HelpLastFMView : ContentPage
    {
        StackLayout Stack;
        ListView newListView;
        SearchBar whatBooksSearch;
        HelpLastFmItemSearch itemSearchMusic;
        ViewHelp_Music.AddMusic.LastFm_Api.AlbumInfo.HelpLastFMJsonAlbumInfo deserializeJsonAlbumInfo;

        public HelpLastFMView()
        {
            Stack = new StackLayout();
            whatBooksSearch = new SearchBar();
            itemSearchMusic = new HelpLastFmItemSearch();


            setElementsOnView();
            this.Content = Stack;
        }

        private void setElementsOnView()
        {
            whatBooksSearch = createSearchBar("Write title", pressSearchBar);

            Stack.Children.Add(whatBooksSearch);
            Stack.Children.Add(addGridName());
            Stack.Children.Add(addListView());
        }



        #region SearchBar - dodanie do widoku, tworzenie, obsługa akcji

        private SearchBar createSearchBar(string placeholder, EventHandler akcja)
        {
            SearchBar newSearchBar = new SearchBar();
            newSearchBar.Placeholder = placeholder;
            newSearchBar.SearchButtonPressed += akcja;

            return newSearchBar;
        }
        private void pressSearchBar(object sender, EventArgs e)
        {
            newListView.IsRefreshing = true;
            itemSearchMusic = new HelpLastFmItemSearch(((SearchBar)sender).Text.ToString());
            newListView.IsRefreshing = false;
            var te = itemSearchMusic;
            newListRefreshing();
        }
        #endregion


        #region Widok grida z nazwami elementów, które są pobierane z API
        private Grid addGridName()
        {
            Grid newGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                   // new ColumnDefinition { Width = GridLength.Star },
                }
            };
            newGrid.Children.Add(new Label { Text = "Author" }, 0, 1, 0, 1);
            newGrid.Children.Add(new Label { Text = "Album" }, 1, 2, 0, 1);
            return newGrid;
        }
        #endregion


        private ListView addListView()
        {

            newListView = new ListView();
            if (itemSearchMusic.listOfAlbums != null)
                newListView.ItemsSource = itemSearchMusic.listOfAlbums;
            newListView.ItemTemplate = new DataTemplate(typeof(dateTemplateLastFMListView));
            newListView.IsPullToRefreshEnabled = true;
            newListView.Refreshing += (sender, e1) => { newListRefreshing(); };
            newListView.ItemTapped += newListViewItemTapped;
            newListView.HasUnevenRows = true;
            newListView.RowHeight = -1;
            return newListView;
        }

        private void newListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            getInfoAboutAlbumFromApi((HelpLastFMObjectsAlbumsSearch)e.Item);
            writeInfoFromApiToViewElements();
        }

        private void newListRefreshing()
        {
            if (itemSearchMusic.listOfAlbums != null)
                newListView.ItemsSource = itemSearchMusic.listOfAlbums;
            newListView.EndRefresh();
        }



        private void getInfoAboutAlbumFromApi(HelpLastFMObjectsAlbumsSearch _object)
        {

            var _artistname = _object.albumAuthor;
            var _albumname = _object.albumName;
            
             string _albumInfoURL = "http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=" + Api.ApiKeySingleton.Instance.getLastFmApiKey() + "&artist=" + _artistname + "&album=" + _albumname + "&format=json";
             HttpClient albumInfoClient = new HttpClient();
             var jsonAlbumInfo = albumInfoClient.GetStringAsync(_albumInfoURL);
             deserializeJsonAlbumInfo = JsonConvert.DeserializeObject<ViewHelp_Music.AddMusic.LastFm_Api.AlbumInfo.HelpLastFMJsonAlbumInfo>(jsonAlbumInfo.Result);
        }

        private void writeInfoFromApiToViewElements()
        {
            ViewHelp_Music.AddMusic.HelpView.Elements.Album.Text = deserializeJsonAlbumInfo.Album.Name;
            ViewHelp_Music.AddMusic.HelpView.Elements.Author.Text = deserializeJsonAlbumInfo.Album.Artist;
            ViewHelp_Music.AddMusic.HelpView.Elements.NumberOfSongs.Text = deserializeJsonAlbumInfo.Album.Tracks.Track.Count.ToString();
            ViewHelp_Music.AddMusic.HelpView.Elements.PublicationDate.Date = Convert.ToDateTime(deserializeJsonAlbumInfo.Album.Wiki.Published);
            ViewHelp_Music.AddMusic.HelpView.Elements.Description.Text = deserializeJsonAlbumInfo.Album.Wiki.Content;
            App.MasterDetailPage.Detail.Navigation.PopModalAsync();
            App.LocalNotification("Download data");

        }

    }


    public class dateTemplateLastFMListView : ViewCell
    {
        public Label albumAuthor { get; private set; }
        public Label albumName { get; private set; }

        #region Konstruktor domyślny
        public dateTemplateLastFMListView() : base()
        {
            View = setGridData();
        }
        #endregion

        #region Grid z bindowanymi elementami API
        private Grid setGridData()
        {
            createElements();
            Grid newGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                }
            };

            newGrid.Children.Add(albumAuthor, 0, 1, 0, 1);
            newGrid.Children.Add(albumName, 1, 2, 0, 1);
            return newGrid;
        }
        #endregion

        #region Wywołąnie tworzenia elementów
        private void createElements()
        {
            albumAuthor = createLabelBinding("albumAuthor");
            albumName = createLabelBinding("albumName");
        }
        #endregion

        #region Metody tworzące elementy
        private Label createLabelBinding(string property)
        {
            Label newLabel = new Label();
            newLabel.SetBinding(Label.TextProperty, new Binding(path: property));
            return newLabel;
        }
        #endregion
    }

    
}
