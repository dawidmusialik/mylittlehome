﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.ViewHelp.ViewHelp_Music.AddMusic.LastFm_Api
{
    public class HelpLastFMObjectsAlbumsSearch
    {
        public string albumAuthor { get; set; }
        public string albumName { get; set; }

        public HelpLastFMObjectsAlbumsSearch(string album, string author)
        {
            albumAuthor = author;
            albumName = album;
        }

    }

    public class HelpLastFMObjectsAlbumInfo
    {


    }

}
