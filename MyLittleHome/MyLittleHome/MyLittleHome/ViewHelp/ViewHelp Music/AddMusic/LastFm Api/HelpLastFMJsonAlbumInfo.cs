﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.ViewHelp.ViewHelp_Music.AddMusic.LastFm_Api.AlbumInfo
{
    public class Image
    {
        [JsonProperty("#text")]
        public string Text { get; set; }
        [JsonProperty("size")]
        public string Size { get; set; }
    }

    public class Attr
    {
        [JsonProperty("rank")]
        public string Rank { get; set; }
    }

    public class Streamable
    {
        [JsonProperty("#text")]
        public string Text { get; set; }
        [JsonProperty("fulltrack")]
        public string Fulltrack { get; set; }
    }

    public class Artist
    {

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("mbid")]
        public string Mbid { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class Track
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("duration")]
        public string Duration { get; set; }
        [JsonProperty("@attr")]
        public Attr Attr { get; set; }
        [JsonProperty("streamable")]
        public Streamable Streamable { get; set; }
        [JsonProperty("artist")]
        public Artist Artist { get; set; }
    }

    public class Tracks
    {
        [JsonProperty("track")]
        public IList<Track> Track { get; set; }
    }

    public class Tag
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class Tags
    {
        [JsonProperty("tag")]
        public IList<Tag> Tag { get; set; }
    }

    public class Wiki
    {
        [JsonProperty("published")]
        public string Published { get; set; }
        [JsonProperty("summary")]
        public string Summary { get; set; }
        [JsonProperty("content")]
        public string Content { get; set; }
    }

    public class Album
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("artist")]
        public string Artist { get; set; }
        [JsonProperty("mbid")]
        public string Mbid { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("image")]
        public IList<Image> Image { get; set; }
        [JsonProperty("listeners")]
        public string Listeners { get; set; }
        [JsonProperty("playcount")]
        public string Playcount { get; set; }
        [JsonProperty("tracks")]
        public Tracks Tracks { get; set; }
        [JsonProperty("tags")]
        public Tags Tags { get; set; }
        [JsonProperty("wiki")]
        public Wiki Wiki { get; set; }
    }

    public class HelpLastFMJsonAlbumInfo
    {
        [JsonProperty("album")]
        public Album Album { get; set; }
    }
}
