﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;
namespace MyLittleHome.ViewHelp.ViewHelp_Music.AddMusic
{
    public class HelpView:ContentPage
    {
        public static HelpViewElements Elements;
        public HelpView()
        {
            Title = " Add new album";
            Elements = new HelpViewElements();
            ScrollView Scroll = new ScrollView { Content = AddMusicView() };
           // Scroll.BackgroundColor = Color.FromHex("BDBDBD");
            this.BackgroundColor = CustomVisual.Instance.getBackgroundColor();

            this.Content = Scroll;
        }


        private Grid AddMusicView()
        {
            Grid AddMusic = new Grid();
            AddMusic.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            AddMusic.RowDefinitions.Add(new RowDefinition { Height = GridLength.Star });

            AddMusic.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });

            AddMusic.Children.Add(GridView(), 0, 1, 0, 1);
            AddMusic.Children.Add(GridViewButton(), 0, 1, 1, 2);
            return AddMusic;
        }

        private Grid GridView()
        {

            Grid ViewGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = 160 },

                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                //   VerticalOptions = LayoutOptions.Start,
            };



            ViewGrid.Children.Add(Elements.AlbumName, 0, 1, 0, 1);
            ViewGrid.Children.Add(Elements.Album, 1, 2, 0, 1);
            ViewGrid.Children.Add(Elements.Search, 2, 3, 0, 1);

            ViewGrid.Children.Add(Elements.AuthorName, 0, 1, 1, 2);
            ViewGrid.Children.Add(Elements.Author, 1, 3, 1, 2);

            ViewGrid.Children.Add(Elements.NumberOfSongsName, 0, 1, 2, 3);
            ViewGrid.Children.Add(Elements.NumberOfSongs, 1, 3, 2, 3);

            ViewGrid.Children.Add(Elements.PublicationDateName, 0, 1, 3, 4);
            ViewGrid.Children.Add(Elements.PublicationDate, 1, 3, 3, 4);

            ViewGrid.Children.Add(Elements.FormatName, 0, 1, 4, 5);
            ViewGrid.Children.Add(Elements.Format, 1, 3, 4, 5);

            ViewGrid.Children.Add(Elements.StorageDeviceName, 0, 1, 5, 6);
            ViewGrid.Children.Add(Elements.StorageDevice, 1, 3, 5, 6);

            ViewGrid.Children.Add(Elements.DescriptionName, 0, 3, 6, 7);
            ViewGrid.Children.Add(Elements.Description, 0, 3, 7, 8);

            return ViewGrid;
        }


        //Widok grida z przyciskami akcji
        private Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },
                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
                //  VerticalOptions = LayoutOptions.EndAndExpand,
                // HorizontalOptions = LayoutOptions.EndAndExpand,
            };
            GridButton.Children.Add(Elements.Cancel, 0, 1, 0, 1);
            GridButton.Children.Add(Elements.Save, 1, 2, 0, 1);

            return GridButton;
        }

    }
}
