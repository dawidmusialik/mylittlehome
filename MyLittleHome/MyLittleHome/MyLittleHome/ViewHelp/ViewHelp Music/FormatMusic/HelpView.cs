﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Music.FormatMusic
{
   public class HelpView :ContentPage
    {
        ViewHelp.ViewHelp_Music.FormatMusic.HelpListElements Elements;
        ViewHelp.ViewHelp_Music.FormatMusic.HelpTopPanelElements BooksElementsTopPanel;

        private ListView ListaView;
        private StackLayout BooksViewFormat;

        private Button DeleteFormat { get; set; }

        public HelpView()
        {
            Title = "Music Format";

            BooksElementsTopPanel = new HelpTopPanelElements();
            ListaView = new ListView();
            BooksViewFormat = new StackLayout();

            this.DeleteFormat = CreateButtonDelete("Delete");
            this.DeleteFormat.IsVisible = false;

            BooksViewFormat.Children.Add(TopPanel());
            BooksViewFormat.Children.Add(FormatList());
            BooksViewFormat.Children.Add(DeleteFormat);
            BooksViewFormat.BackgroundColor = Color.FromHex("BDBDBD");



            this.Content = BooksViewFormat;

        }

        private Grid TopPanel()
        {
            Grid GRID = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            GRID.Children.Add(BooksElementsTopPanel.Name, 0, 1, 0, 1);
            GRID.Children.Add(BooksElementsTopPanel.FormatName, 1, 4, 0, 1);
            GRID.Children.Add(BooksElementsTopPanel.SendFormat, 4, 5, 0, 1);
            return GRID;
        }

        public ListView FormatList()
        {

            ListaView = new ListView();

            ListaView.ItemsSource = App.MyLittleHomeDB_MusicFormat.GetMusicFormatItemSource();
            ListaView.ItemTemplate = new DataTemplate(() => ListCell());
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            // ListaView.RowHeight = 265;
            ListaView.ItemTapped += ListaViewItemTapped;
            return ListaView;
        }

        private void ListaViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            DeleteFormat.IsVisible = true;
            DataBase.TableMusicFormat Usuwanie = new DataBase.TableMusicFormat();

            DeleteFormat.Clicked += (object sender2, EventArgs e2) =>
            {
                Usuwanie = (DataBase.TableMusicFormat)e.Item;

                if (App.MyLittleHomeDB_Music.CheckIDFormatMusic_In_TableMusic(Usuwanie.ID) == false)
                {
                    App.MyLittleHomeDB_MusicFormat.DeleteOneMusicFormat(Usuwanie.ID);
                }
                Usuwanie = null;
                DeleteFormat.IsVisible = false;
            };
        }



        private ViewCell ListCell()
        {
            Elements = new HelpListElements();

            return new ViewCell
            {
                View = AllRow(),
            };
        }


        private StackLayout AllRow()
        {
            StackLayout Stack = new StackLayout();
            Stack.Children.Add(RowID());
            Stack.Children.Add(RowFormat());
            Stack.Orientation = StackOrientation.Horizontal;
            return Stack;
        }

        private Grid RowID()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.IDName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.ID, 1, 2, 0, 1);

            return Stack;
        }
        private Grid RowFormat()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.FormatName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.Format, 1, 2, 0, 1);

            return Stack;
        }

        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_MusicFormat.GetMusicFormatItemSource();
                ListaView.ItemTemplate = new DataTemplate(() => ListCell());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku

        private Button CreateButtonDelete(string nazwa)
        {
            Button name = new Button();
            name.Text = nazwa;
            return name;
        }
    }
}
