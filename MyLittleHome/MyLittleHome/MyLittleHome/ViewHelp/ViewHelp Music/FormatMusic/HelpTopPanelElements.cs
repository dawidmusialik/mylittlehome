﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;


namespace MyLittleHome.ViewHelp.ViewHelp_Music.FormatMusic
{
    public class HelpTopPanelElements
    {
        public Label Name { get; private set; }
        public Entry FormatName { get; private set; }
       // public Button SendFormat { get; private set; }
        public CustomButton SendFormat { get; private set; }


        public HelpTopPanelElements()
        {
            CreateAll();
        }


        private void CreateAll()
        {
            this.Name = CreateLabel("Nazwa");
            this.FormatName = CreateEntry("Format");
            //  this.SendFormat = CreateButton("Send", SendFormatEventHandler);
            this.SendFormat = new CustomButton();
            this.SendFormat.setSource(path: "iconSend.png");
            this.SendFormat.Clicked(SendFormatTapGesture());

        }


        private Label CreateLabel(string tekst)
        {
            Label name = new Label();
            name.Text = tekst;
            name.FontSize = 17;
            name.HorizontalOptions = LayoutOptions.Center;
            name.VerticalOptions = LayoutOptions.Center;
            return name;
        }

        private Entry CreateEntry(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.FontSize = 15;
            return name;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button nazwa = new Button();
            nazwa.Text = tekst;
            nazwa.Clicked += akcja;
            return nazwa;
        }
        /*
        private void SendFormatEventHandler(object sender, EventArgs e)
        {
            if (App.MyLittleHomeDB_MusicFormat.CheckInsertMusicFormat(this.FormatName.Text.ToString()) == true)
            {
                App.MyLittleHomeDB_MusicFormat.InsertMusicFormat(this.FormatName.Text.ToString());
                App.LocalNotification("Format added");

                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Music.ViewMusicAdd());

            }
        }
        */
        private TapGestureRecognizer SendFormatTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (this.FormatName.Text != null)
                {
                    if (App.MyLittleHomeDB_BooksFormat.CheckInsertBooksFormat(this.FormatName.Text.ToString()) == true)
                    {
                        App.MyLittleHomeDB_MusicFormat.InsertMusicFormat(this.FormatName.Text.ToString());
                        App.LocalNotification("Format added");

                        App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Music.ViewMusicAdd());
                    }
                }
                else
                {
                    App.LocalNotification("Write format name");

                }


            };

            return _tap;
        }

    }
}
