﻿using MyLittleHome.Component;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.Movies
{
    public class HelpTopPanelElements
    {
        public Label TitlePage { get; private set; }
       //  public Button ButtonAddMovies { get; private set; }
       // public Button ButtonEditMovies { get; set; }

        public CustomButton ButtonEditMovie { get; set; }
        public CustomButton ButtonAddMovie { get; private set; }

        public HelpTopPanelElements()
        {
            CreateElements();
        }

        private void CreateElements()
        {
            this.TitlePage = CreateLabel("My Movies");
            //this.ButtonAddBook = CreateButton("Add", ButtonAddBookEventHandler);
            this.ButtonAddMovie = new CustomButton(path: "iconAdd.png");
            this.ButtonAddMovie.Clicked(ButtonAddBookEventHandler());
            //this.ButtonEditBook = CreateButton("Edit Movies", null);
            this.ButtonEditMovie = new CustomButton(path: "iconEdit.png");
        }

        private Label CreateLabel(string tekst)
        {
            Label Title = new Label();
            Title.Text = tekst;
            Title.FontSize = 30;
            Title.TextColor = Color.FromHex("57FF9D");
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.VerticalOptions = LayoutOptions.Center;
            Title.HorizontalOptions = LayoutOptions.Center;
            return Title;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button button = new Button();
            button.Text = tekst;
            button.Clicked += akcja;
            button.VerticalOptions = LayoutOptions.End;
            button.HorizontalOptions = LayoutOptions.End;

            return button;
        }
        /*
        private void ButtonAddMoviesEventHandler(object sender, EventArgs e)
        {
           // App.MasterDetailPage.Detail = new NavigationPage(new View.Movies.ViewMoviesAdd());
            App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Movies.ViewMoviesAdd());

        }
        */
        private TapGestureRecognizer ButtonAddBookEventHandler()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Movies.ViewMoviesAdd());
            };
            return _tap;
        }



    }
}
