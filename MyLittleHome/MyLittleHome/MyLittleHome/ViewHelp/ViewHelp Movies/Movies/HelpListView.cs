﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.Movies
{
    public class HelpListView : ViewCell
    {
        HelpListElements Elements;
        private ListView ListaView;
        
        public HelpListView()
        {
            Elements = new Movies.HelpListElements();
            this.View = GridView();
        }

    

        private Grid GridView()
        {
            Elements = new HelpListElements();
            HelpListElements.Number++;
            Elements.ID.Text = Convert.ToString(HelpListElements.Number);

            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                  // new RowDefinition { Height = 40 },
                  // new RowDefinition { Height = 40 },
                  // new RowDefinition { Height = 40 },
                  // new RowDefinition { Height = 140 },
                  
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto },
                  //  new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };



            grid.Children.Add((TitleinGrid()), 0, 5, 0, 1);

            grid.Children.Add((IDinGrid()), 0, 1, 1, 2);
            grid.Children.Add(PublicationDateinGrid(), 1, 2, 1, 2);
            grid.Children.Add(FormatinGrid(), 2, 3, 1, 2);

            grid.Children.Add((QualityinGrid()), 0, 1, 2, 3);
            grid.Children.Add((LanguageinGrid()), 1, 2, 2, 3);
            grid.Children.Add((SubbitlesinGrid()), 2, 3, 2, 3);

            grid.Children.Add(StorageDeviceinGrid(), 3, 5, 1, 2);
            grid.Children.Add(LoaninGrid(), 3, 5, 2, 3);

            grid.Children.Add(DescriptioninGrid(), 0, 5, 3, 4);

            return grid;
        }

        private StackLayout IDinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.Children.Add(Elements.IDName);
            Stack.Children.Add(Elements.ID);

            return Stack;
        }

        private StackLayout TitleinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //   Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.TitleName);
            Stack.Children.Add(Elements.Title);

            return Stack;
        }

        private StackLayout PublicationDateinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Sienna;
            Stack.Children.Add(Elements.PublicationDateName);
            Stack.Children.Add(Elements.PublicationDate);

            return Stack;
        }
        private StackLayout FormatinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.FormatName);
            Stack.Children.Add(Elements.Format);

            return Stack;
        }

        private StackLayout QualityinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.QualityName);
            Stack.Children.Add(Elements.Quality);

            return Stack;
        }

        private StackLayout LanguageinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.LanguageName);
            Stack.Children.Add(Elements.Language);

            return Stack;
        }

        private StackLayout SubbitlesinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            // Stack.BackgroundColor = Color.Black;
            Stack.Children.Add(Elements.SubbitlesName);
            Stack.Children.Add(Elements.Subbitles);

            return Stack;
        }
        private StackLayout LoaninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.VerticalOptions = LayoutOptions.Center;
            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.LoanName);
            Stack.Children.Add(Elements.Loan);

            return Stack;
        }

        private StackLayout StorageDeviceinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.VerticalOptions = LayoutOptions.Center;
            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.StorageDeviceName);
            Stack.Children.Add(Elements.StorageDevice);

            return Stack;
        }
        private StackLayout DescriptioninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.BackgroundColor = Color.FromHex("CCCCCC");
            Stack.Children.Add(Elements.DescriptionName);
            Stack.Children.Add(Elements.Description);

            return Stack;
        }
        
        
    }
}
