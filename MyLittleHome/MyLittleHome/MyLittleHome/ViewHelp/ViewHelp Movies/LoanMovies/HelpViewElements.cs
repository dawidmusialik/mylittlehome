﻿using MyLittleHome.Component;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.LoanMovies
{
   public class HelpViewElements
    {
        DataBase.TableMoviesLoan DirectMoviesLoan;
        DataBase.TableMoviesWithConettedTable DirectMovies;
        public HelpViewElements(DataBase.TableMoviesWithConettedTable OneMovies)
        {
            DirectMoviesLoan = (App.MyLittleHomeDB_MoviesLoan.GetDirectMoviesLoan(OneMovies.ID)[0]);
            DirectMovies = OneMovies;
            ElementsCreate();
        }

        public DatePicker DateLoan { get; private set; }
        public DatePicker DateReceipt { get; private set; }
        public Entry Who { get; private set; }
        public Label DateLoanName { get; private set; }
        public Label DateReceiptName { get; private set; }
        public Label WhoName { get; private set; }


        //public Button Cancel { get; private set; }
        public CustomButton Cancel { get; private set; }

        //public Button Save { get; private set; }
        public CustomButton Save { get; private set; }

        //public Button Delete { get; private set; }
        public CustomButton Delete { get; private set; }

        private void ElementsCreate()
        {
            this.DateLoanName = ElementsLabelNameCreate("Date Loan:");
            this.DateReceiptName = ElementsLabelNameCreate("Date Receipt:");
            this.WhoName = ElementsLabelNameCreate("Who?:");

            this.DateLoan = ElementsDatePicerCreate();
            this.DateLoan.Date = DirectMoviesLoan.DateLoan.Date;
            this.DateReceipt = ElementsDatePicerCreate();
            this.DateReceipt.Date = DirectMoviesLoan.DateReceipt.Date;

            this.Who = ElementsEntryCreate();
            this.Who.Text = DirectMoviesLoan.Who;

            //this.Cancel = ElementsButtonCreate("Cancel", CancelEventHandler);
            this.Cancel = new CustomButton(path: "iconBack.png");
            this.Cancel.Clicked(CancelTapGesture());

            //this.Save = ElementsButtonCreate("Save", SaveEventHandler);
            this.Save = new CustomButton(path: "iconSave.png");
            this.Save.Clicked(SaveTapGesture());

            //this.Delete = ElementsButtonCreate("Delete", DeleteEventHandler);
            this.Delete = new CustomButton(path: "iconDelete.png");
            this.Delete.Clicked(DeleteTapGesture());

        }

        private Entry ElementsEntryCreate()
        {
            Entry name = new Entry();
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }
        private Label ElementsLabelNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
        private DatePicker ElementsDatePicerCreate()
        {
            DatePicker NewDate = new DatePicker();
            NewDate.Format = "yyy-MM-dd";
            NewDate.HorizontalOptions = LayoutOptions.Center;
            return NewDate;
        }

        private Button ElementsButtonCreate(string tekst, EventHandler akcja)
        {
            Button NewButton = new Button();
            NewButton.Text = tekst;
            NewButton.Clicked += akcja;
            return NewButton;
        }
        /*
        private void CancelEventHandler(object sender, EventArgs e)
        {
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
        }
        */

        private TapGestureRecognizer CancelTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
            };
            return _tap;
        }
        /*
        private void SaveEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_MoviesLoan.UpdateMoviesLoan(DirectMoviesLoan.IDMovies, DateLoan.Date, DateReceipt.Date, Who.Text);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesEdit(DirectMovies.ID));
            App.LocalNotification("Loan edited");
        }
        */
        private TapGestureRecognizer SaveTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_MoviesLoan.UpdateMoviesLoan(DirectMoviesLoan.IDMovies, DateLoan.Date, DateReceipt.Date, Who.Text);
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesEdit(DirectMovies.ID));
                App.LocalNotification("Loan edited");
            };
            return _tap;
        }
        /*
        private void DeleteEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_MoviesLoan.DeleteOneMoviesLoan(DirectMovies.ID);
            App.MyLittleHomeDB_Movies.UpgradeMovies(DirectMoviesLoan.IDMovies, DirectMovies.Title, DirectMovies.PublicationDate, DirectMovies.IDFormat, DirectMovies.IDQuality, DirectMovies.IDLanguage, DirectMovies.IDSubbitles, DirectMovies.StorageDevice, DirectMovies.Description, false);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesEdit(DirectMovies.ID));
            App.LocalNotification("Movie returned");
        }
        */
        private TapGestureRecognizer DeleteTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_MoviesLoan.DeleteOneMoviesLoan(DirectMovies.ID);
                App.MyLittleHomeDB_Movies.UpgradeMovies(DirectMoviesLoan.IDMovies, DirectMovies.Title, DirectMovies.PublicationDate, DirectMovies.IDFormat, DirectMovies.IDQuality, DirectMovies.IDLanguage, DirectMovies.IDSubbitles, DirectMovies.StorageDevice, DirectMovies.Description, false);
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesEdit(DirectMovies.ID));
                App.LocalNotification("Movie returned");
            };
            return _tap;
        }

    }
}
