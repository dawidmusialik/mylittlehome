﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.LanguageMovies
{
    public class HelpTopPanelElements
    {
        public Label Name { get; private set; }
        public Entry LanguageName { get; private set; }
        //public Button SendLanguage { get; private set; }
        public CustomButton SendLanguage { get; private set; }


        public HelpTopPanelElements()
        {
            CreateAll();
        }


        private void CreateAll()
        {
            this.Name = CreateLabel("Nazwa");
            this.LanguageName = CreateEntry("Language");
            //this.SendLanguage = CreateButton("Send", SendLanguageEventHandler);
            this.SendLanguage = new CustomButton(path: "iconSend.png");
            this.SendLanguage.Clicked(SendLanguageTapGesture());
        }


        private Label CreateLabel(string tekst)
        {
            Label name = new Label();
            name.Text = tekst;
            name.FontSize = 17;
            name.HorizontalOptions = LayoutOptions.Center;
            name.VerticalOptions = LayoutOptions.Center;
            return name;
        }

        private Entry CreateEntry(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.FontSize = 15;
            return name;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button nazwa = new Button();
            nazwa.Text = tekst;
            nazwa.Clicked += akcja;
            return nazwa;
        }
        /*
        private void SendLanguageEventHandler(object sender, EventArgs e)
        {
            if (App.MyLittleHomeDB_MoviesLanguage.CheckInsertMoviesLanguage(this.LanguageName.Text.ToString()) == true)
            {
                App.MyLittleHomeDB_MoviesLanguage.InsertMoviesLanguage(this.LanguageName.Text.ToString());
                App.LocalNotification("Language added");

                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());

            }
        }
        */
        private TapGestureRecognizer SendLanguageTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (this.LanguageName.Text != null)
                {
                    if (App.MyLittleHomeDB_MoviesLanguage.CheckInsertMoviesLanguage(this.LanguageName.Text.ToString()) == true)
                    {
                        App.MyLittleHomeDB_MoviesLanguage.InsertMoviesLanguage(this.LanguageName.Text.ToString());
                        App.LocalNotification("Language added");

                        App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());

                    }
                }
                else
                {
                    App.LocalNotification("Write language");

                }


            };

            return _tap;
        }

    }
}
