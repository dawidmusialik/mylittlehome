﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.QualityMovies
{
    public class HelpView : ContentPage
    {
        ViewHelp.ViewHelp_Movies.QualityMovies.HelpListElements Elements;
        ViewHelp.ViewHelp_Movies.QualityMovies.HelpTopPanelElements QualityElementsTopPanel;

        private ListView ListaView;
        private StackLayout MoviesViewFormat;

        private Button DeleteQuality { get; set; }

        public HelpView()
        {
            Title = "Movies Quality";

            QualityElementsTopPanel = new HelpTopPanelElements();
            ListaView = new ListView();
            MoviesViewFormat = new StackLayout();

            this.DeleteQuality = CreateButtonDelete("Delete");
            this.DeleteQuality.IsVisible = false;

            MoviesViewFormat.Children.Add(TopPanel());
            MoviesViewFormat.Children.Add(FormatList());
            MoviesViewFormat.Children.Add(DeleteQuality);
            MoviesViewFormat.BackgroundColor = Color.FromHex("BDBDBD");



            this.Content = MoviesViewFormat;

        }

        private Grid TopPanel()
        {
            Grid GRID = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            GRID.Children.Add(QualityElementsTopPanel.Name, 0, 1, 0, 1);
            GRID.Children.Add(QualityElementsTopPanel.QualityName, 1, 4, 0, 1);
            GRID.Children.Add(QualityElementsTopPanel.SendQuality, 4, 5, 0, 1);
            return GRID;
        }

        public ListView FormatList()
        {

            ListaView = new ListView();

            ListaView.ItemsSource = App.MyLittleHomeDB_MoviesQuality.GetMoviesQualityItemSource();
            ListaView.ItemTemplate = new DataTemplate(() => ListCell());
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            // ListaView.RowHeight = 265;
            ListaView.ItemTapped += ListaViewItemTapped;
            return ListaView;
        }

        private void ListaViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            DeleteQuality.IsVisible = true;
            DataBase.TableMoviesQuality Usuwanie = new DataBase.TableMoviesQuality();

            DeleteQuality.Clicked += (object sender2, EventArgs e2) =>
            {
                Usuwanie = (DataBase.TableMoviesQuality)e.Item;

                if (App.MyLittleHomeDB_Movies.CheckIDQualityMovies_In_TableMovies(Usuwanie.ID) == false)
                {
                    App.MyLittleHomeDB_MoviesQuality.DeleteOneMoviesQuality(Usuwanie.ID);
                }
                Usuwanie = null;
                DeleteQuality.IsVisible = false;
            };
        }



        private ViewCell ListCell()
        {
            Elements = new HelpListElements();

            return new ViewCell
            {
                View = AllRow(),
            };
        }


        private StackLayout AllRow()
        {
            StackLayout Stack = new StackLayout();
            Stack.Children.Add(RowID());
            Stack.Children.Add(RowFormat());
            Stack.Orientation = StackOrientation.Horizontal;
            return Stack;
        }

        private Grid RowID()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.IDName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.ID, 1, 2, 0, 1);

            return Stack;
        }
        private Grid RowFormat()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.QualityName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.Quality, 1, 2, 0, 1);

            return Stack;
        }

        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_MoviesQuality.GetMoviesQualityItemSource();
                ListaView.ItemTemplate = new DataTemplate(() => ListCell());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku

        private Button CreateButtonDelete(string nazwa)
        {
            Button name = new Button();
            name.Text = nazwa;
            return name;
        }
    }
}
