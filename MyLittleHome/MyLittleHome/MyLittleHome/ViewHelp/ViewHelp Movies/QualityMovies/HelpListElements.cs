﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.QualityMovies
{
    public class HelpListElements
    {
        public Label ID { get; private set; }
        public Label IDName { get; private set; }

        public Label Quality { get; private set; }
        public Label QualityName { get; private set; }


        public HelpListElements()
        {
            Elements();
        }



        //Deklaracja funkcji stworzonych Elementy
        private void Elements()
        {
            //Elementy pobierające dane z tabeli Books
            this.ID = ElementsDataCreate("ID");
            this.IDName = ElementsNameCreate("ID:");

            this.Quality = ElementsDataCreate("Quality");
            this.QualityName = ElementsNameCreate("Quality:");

        }



        //Funkcja tworząca elementy, które zawierają dane z tabeli Books
        private Label ElementsDataCreate(string property)
        {
            Label name = new Label();
            name.SetBinding(Label.TextProperty, property);
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            name.FontSize = 15;
            return name;
        }
        //Funkcja tworząca nazwy elementów, które zawierają dane z tabeli Books
        private Label ElementsNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            name.FontSize = 17;
            return name;
        }
    }
}
