﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.QualityMovies
{
    public class HelpTopPanelElements
    {
        public Label Name { get; private set; }
        public Entry QualityName { get; private set; }
        //public Button SendQuality { get; private set; }
        public CustomButton SendQuality { get; private set; }


        public HelpTopPanelElements()
        {
            CreateAll();
        }


        private void CreateAll()
        {
            this.Name = CreateLabel("Nazwa");
            this.QualityName = CreateEntry("Quality");
            //this.SendQuality = CreateButton("Send", SendQualityEventHandler);
            this.SendQuality = new CustomButton(path: "iconSend.png");
            this.SendQuality.Clicked(SendQualityTapGesture());
        }


        private Label CreateLabel(string tekst)
        {
            Label name = new Label();
            name.Text = tekst;
            name.FontSize = 17;
            name.HorizontalOptions = LayoutOptions.Center;
            name.VerticalOptions = LayoutOptions.Center;
            return name;
        }

        private Entry CreateEntry(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.FontSize = 15;
            return name;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button nazwa = new Button();
            nazwa.Text = tekst;
            nazwa.Clicked += akcja;
            return nazwa;
        }
        /*
        private void SendQualityEventHandler(object sender, EventArgs e)
        {
            if (App.MyLittleHomeDB_MoviesQuality.CheckInsertMoviesQuality(this.QualityName.Text.ToString()) == true)
            {
                App.MyLittleHomeDB_MoviesQuality.InsertMoviesQuality(this.QualityName.Text.ToString());
                App.LocalNotification("Quality added");

                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());

            }
        }
        */
        private TapGestureRecognizer SendQualityTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (this.QualityName.Text != null)
                {
                    if (App.MyLittleHomeDB_MoviesQuality.CheckInsertMoviesQuality(this.QualityName.Text.ToString()) == true)
                    {
                        App.MyLittleHomeDB_MoviesQuality.InsertMoviesQuality(this.QualityName.Text.ToString());
                        App.LocalNotification("Quality added");

                        App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());

                    }
                }
                else
                {
                    App.LocalNotification("Write quality");

                }


            };

            return _tap;
        }
    }
}
