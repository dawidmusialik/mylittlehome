﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.SubbitlesMovies
{
    public class HelpTopPanelElements
    {
        public Label Name { get; private set; }
        public Entry SubbitlesName { get; private set; }
        // public Button SendSubbitles { get; private set; }
        public CustomButton SendSubbitles { get; private set; }


        public HelpTopPanelElements()
        {
            CreateAll();
        }


        private void CreateAll()
        {
            this.Name = CreateLabel("Nazwa");
            this.SubbitlesName = CreateEntry("Subbitles");
            //this.SendSubbitles = CreateButton("Send", SendSubbitlesEventHandler);
            this.SendSubbitles = new CustomButton(path: "iconSend.png");
            this.SendSubbitles.Clicked(SendSubbitlesTapGesture());
        }


        private Label CreateLabel(string tekst)
        {
            Label name = new Label();
            name.Text = tekst;
            name.FontSize = 17;
            name.HorizontalOptions = LayoutOptions.Center;
            name.VerticalOptions = LayoutOptions.Center;
            return name;
        }

        private Entry CreateEntry(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.FontSize = 15;
            return name;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button nazwa = new Button();
            nazwa.Text = tekst;
            nazwa.Clicked += akcja;
            return nazwa;
        }
        /*
        private void SendSubbitlesEventHandler(object sender, EventArgs e)
        {
            if (App.MyLittleHomeDB_MoviesSubbitles.CheckInsertMoviesSubbitles(this.SubbitlesName.Text.ToString()) == true)
            {
                App.MyLittleHomeDB_MoviesSubbitles.InsertMoviesSubbitles(this.SubbitlesName.Text.ToString());
                App.LocalNotification("Subbitles added");

                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());

            }
        }
        */
        private TapGestureRecognizer SendSubbitlesTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (this.SubbitlesName.Text != null)
                {
                    if (App.MyLittleHomeDB_MoviesSubbitles.CheckInsertMoviesSubbitles(this.SubbitlesName.Text.ToString()) == true)
                    {
                        App.MyLittleHomeDB_MoviesSubbitles.InsertMoviesSubbitles(this.SubbitlesName.Text.ToString());
                        App.LocalNotification("Subbitles added");

                        App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());

                    }
                }
                else
                {
                    App.LocalNotification("Write subbitles");

                }


            };

            return _tap;
        }
    }
}
