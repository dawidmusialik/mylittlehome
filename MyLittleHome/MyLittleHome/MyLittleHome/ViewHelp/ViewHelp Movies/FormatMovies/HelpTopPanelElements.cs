﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.FormatMovies
{
    //Top panel elements Format Movies
    public class HelpTopPanelElements
    {
        public Label Name { get; private set; }
        public Entry FormatName { get; private set; }
        //public Button SendFormat { get; private set; }
        public CustomButton SendFormat { get; private set; }

        public HelpTopPanelElements()
        {
            CreateAll();
        }


        private void CreateAll()
        {
            this.Name = CreateLabel("Nazwa");
            this.FormatName = CreateEntry("Format");
            // this.SendFormat = CreateButton("Send", SendFormatEventHandler);
            this.SendFormat = new CustomButton(path: "iconSend.png");
            this.SendFormat.Clicked(SendFormatTapGesture());
        }


        private Label CreateLabel(string tekst)
        {
            Label name = new Label();
            name.Text = tekst;
            name.FontSize = 17;
            name.HorizontalOptions = LayoutOptions.Center;
            name.VerticalOptions = LayoutOptions.Center;
            return name;
        }

        private Entry CreateEntry(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.FontSize = 15;
            return name;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button nazwa = new Button();
            nazwa.Text = tekst;
            nazwa.Clicked += akcja;
            return nazwa;
        }
        /*
        private void SendFormatEventHandler(object sender, EventArgs e)
        {
            if (App.MyLittleHomeDB_MoviesFormat.CheckInsertMoviesFormat(this.FormatName.Text.ToString()) == true)
            {
                App.MyLittleHomeDB_MoviesFormat.InsertMoviesFormat(this.FormatName.Text.ToString());
                App.LocalNotification("Format added");

                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());

            }
        }
        */
        private TapGestureRecognizer SendFormatTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (this.FormatName.Text != null)
                {
                    if (App.MyLittleHomeDB_MoviesFormat.CheckInsertMoviesFormat(this.FormatName.Text.ToString()) == true)
                    {
                        App.MyLittleHomeDB_MoviesFormat.InsertMoviesFormat(this.FormatName.Text.ToString());
                        App.LocalNotification("Format added");

                        App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesAdd());
                    }
                }
                else
                {
                    App.LocalNotification("Write format name");
                }


            };

            return _tap;
        }

    }
}
