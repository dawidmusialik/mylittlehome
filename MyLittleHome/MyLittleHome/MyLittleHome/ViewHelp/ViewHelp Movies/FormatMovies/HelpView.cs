﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Movies.FormatMovies
{
    public class HelpView:  ContentPage
    {
        ViewHelp.ViewHelp_Movies.FormatMovies.HelpListElements Elements;
        ViewHelp.ViewHelp_Movies.FormatMovies.HelpTopPanelElements MoviesElementsTopPanel;

        private ListView ListaView;
        private StackLayout MoviesViewFormat;

        private Button DeleteFormat { get; set; }

        public HelpView()
        {
            Title = "Movies Format";

            MoviesElementsTopPanel = new ViewHelp.ViewHelp_Movies.FormatMovies.HelpTopPanelElements();
            ListaView = new ListView();
            MoviesViewFormat = new StackLayout();

            this.DeleteFormat = CreateButtonDelete("Delete");
            this.DeleteFormat.IsVisible = false;

            MoviesViewFormat.Children.Add(TopPanel());
            MoviesViewFormat.Children.Add(FormatList());
            MoviesViewFormat.Children.Add(DeleteFormat);
            MoviesViewFormat.BackgroundColor = Color.FromHex("BDBDBD");



            this.Content = MoviesViewFormat;

        }

        private Grid TopPanel()
        {
            Grid GRID = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            GRID.Children.Add(MoviesElementsTopPanel.Name, 0, 1, 0, 1);
            GRID.Children.Add(MoviesElementsTopPanel.FormatName, 1, 4, 0, 1);
            GRID.Children.Add(MoviesElementsTopPanel.SendFormat, 4, 5, 0, 1);
            return GRID;
        }

        public ListView FormatList()
        {

            ListaView = new ListView();

            ListaView.ItemsSource = App.MyLittleHomeDB_MoviesFormat.GetMoviesFormatItemSource();
            ListaView.ItemTemplate = new DataTemplate(() => ListCell());
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            // ListaView.RowHeight = 265;
            ListaView.ItemTapped += ListaViewItemTapped;
            return ListaView;
        }

        private void ListaViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            DeleteFormat.IsVisible = true;
            DataBase.TableMoviesFormat Usuwanie = new DataBase.TableMoviesFormat();

            DeleteFormat.Clicked += (object sender2, EventArgs e2) =>
            {
                Usuwanie = (DataBase.TableMoviesFormat)e.Item;

                if (App.MyLittleHomeDB_Movies.CheckIDFormatMovies_In_TableMovies(Usuwanie.ID) == false)
                {
                    App.MyLittleHomeDB_MoviesFormat.DeleteOneMoviesFormat(Usuwanie.ID);
                }
                Usuwanie = null;
                DeleteFormat.IsVisible = false;
            };
        }



        private ViewCell ListCell()
        {
            Elements = new ViewHelp.ViewHelp_Movies.FormatMovies.HelpListElements();

            return new ViewCell
            {
                View = AllRow(),
            };
        }


        private StackLayout AllRow()
        {
            StackLayout Stack = new StackLayout();
            Stack.Children.Add(RowID());
            Stack.Children.Add(RowFormat());
            Stack.Orientation = StackOrientation.Horizontal;
            return Stack;
        }

        private Grid RowID()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.IDName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.ID, 1, 2, 0, 1);

            return Stack;
        }
        private Grid RowFormat()
        {
            Grid Stack = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{Height = 40 }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{Width = GridLength.Star},
                    new ColumnDefinition{Width = GridLength.Star},
                }
            };
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.FormatName, 0, 1, 0, 1);
            Stack.Children.Add(Elements.Format, 1, 2, 0, 1);

            return Stack;
        }

        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_MoviesFormat.GetMoviesFormatItemSource();
                ListaView.ItemTemplate = new DataTemplate(() => ListCell());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku

        private Button CreateButtonDelete(string nazwa)
        {
            Button name = new Button();
            name.Text = nazwa;
            return name;
        }
    }
}
