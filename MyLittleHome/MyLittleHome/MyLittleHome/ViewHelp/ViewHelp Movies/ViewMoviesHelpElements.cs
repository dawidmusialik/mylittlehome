﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.ViewHelp {
    public class ViewMoviesHelpElements {

        public ViewMoviesHelpElements()
        {
            Elements();
        }

        //Elementy na dane z tabeli Movies
        public Label ID { get; private set; }
        public Label Title { get; private set; }
        public Label PublicationDate { get; private set; }
        public Label Format { get; private set; }
        public Label Quality { get; private set; }
        public Label Language { get; private set; }
        public Label Subbitles { get; private set; }
        public Label StorageDevice { get; private set; }
        public Label Description { get; private set; }
        public Label Loan { get; private set; }

        //Elementy na nazwy danych z tabeli Movies
        public Label IDName { get; private set; }
        public Label TitleName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label QualityName { get; private set; }
        public Label LanguageName { get; private set; }
        public Label SubbitlesName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }
        public Label LoanName { get; private set; }





        //Deklaracja funkcji stworzonych Elementy
        public void Elements() {
            this.ID = ElementsLabelCreate("ID");
            this.IDName = ElementsLabelNameCreate("ID:");

            this.Title = ElementsLabelCreate("Title");
            this.TitleName = ElementsLabelNameCreate("Title:");

            this.PublicationDate = ElementsLabelCreate("PublicationDate");
            this.PublicationDateName = ElementsLabelNameCreate("Date");

            this.Format = ElementsLabelCreate("FormatText");
            this.FormatName = ElementsLabelNameCreate("Format:");

            this.Quality = ElementsLabelCreate("QualityText");
            this.QualityName = ElementsLabelNameCreate("Quality:");

            this.Language = ElementsLabelCreate("LanguageText");
            this.LanguageName = ElementsLabelNameCreate("Language:");

            this.Subbitles = ElementsLabelCreate("SubbitlesText");
            this.SubbitlesName = ElementsLabelNameCreate("Subbitles:");

            this.StorageDevice = ElementsLabelCreate("StorageDevice");
            this.StorageDeviceName = ElementsLabelNameCreate("Storage Device:");

            this.Description = ElementsLabelCreate("Description");
            this.DescriptionName = ElementsLabelNameCreate("Description:");

            this.Loan = ElementsLabelCreate("LoanToSting");
            this.LoanName = ElementsLabelNameCreate("Loan:");
        }


        private Label ElementsLabelCreate(string property)
        {
            Label name = new Label();

            if (property == "PublicationDate")
            {
                name.SetBinding(Label.TextProperty, new Binding { Path = property, StringFormat = "{0:yyyy-MM-dd}" });
            }
            else
            {
                name.SetBinding(Label.TextProperty, new Binding { Path = property });
            }
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

        private Label ElementsLabelNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

    }
}
