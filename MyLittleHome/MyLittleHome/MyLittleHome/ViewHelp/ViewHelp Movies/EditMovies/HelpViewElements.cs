﻿using MyLittleHome.Component;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.EditMovies
{
   public class HelpViewElements
    {
        DataBase.TableMoviesWithConettedTable DirectMovies;
        public HelpViewElements(DataBase.TableMoviesWithConettedTable OneMovie)
        {
            DirectMovies = new DataBase.TableMoviesWithConettedTable();
            DirectMovies = OneMovie;
            Elements();
        }

        public Entry Title { get; private set; }
        public DatePicker PublicationDate { get; private set; }
        public Picker Format { get; private set; }
        public Picker Quality { get; private set; }
        public Picker Language { get; private set; }
        public Picker Subbitles { get; private set; }
        public Entry StorageDevice { get; private set; }
        public Editor Description { get; private set; }



        //Button
        public CustomButton Cancel { get; private set; }
        //public Button Cancel { get; private set; }

        public CustomButton Delete { get; private set; }
        //public Button Delete { get; private set; }

        public CustomButton Save { get; private set; }
        //public Button Save { get; private set; }



        public Switch Loan { get; private set; }




        public Label TitleName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label QualityName { get; private set; }
        public Label LanguageName { get; private set; }
        public Label SubbitlesName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }

        public Label LoanName { get; private set; }
        public Label LoanDescription { get; private set; }


       


        Dictionary<string, int> DictionaryFormat = App.MyLittleHomeDB_MoviesFormat.GetMoviesFormatDictionary();
        Dictionary<string, int> DictionaryQuality = App.MyLittleHomeDB_MoviesQuality.GetMoviesQualityDictionary();
        Dictionary<string, int> DictionaryLanguage = App.MyLittleHomeDB_MoviesLanguage.GetMoviesLanguageDictionary();
        Dictionary<string, int> DictionarySubbitles = App.MyLittleHomeDB_MoviesSubbitles.GetMoviesFormatSubbitles();

        private void Elements()
        {
            this.Title = ElementsEntryCreate("Title");
            this.TitleName = ElementsNameCreate("Title:");
            this.Title.Text = DirectMovies.Title;

            this.PublicationDate = ElementsDatePicerCreate();
            this.PublicationDateName = ElementsNameCreate("Date:");
            this.PublicationDate.Date = DirectMovies.PublicationDate;

            this.Format = ElementsPickerCreate(DictionaryFormat, "Format");
            this.FormatName = ElementsNameCreate("Format:");
            this.Format.Title = DirectMovies.FormatText;

            this.Quality = ElementsPickerCreate(DictionaryQuality, "Quality");
            this.QualityName = ElementsNameCreate("Quality:");
            this.Quality.Title = DirectMovies.QualityText;

            this.Language = ElementsPickerCreate(DictionaryLanguage, "Language");
            this.LanguageName = ElementsNameCreate("Language:");
            this.Language.Title = DirectMovies.LanguageText;

            this.Subbitles = ElementsPickerCreate(DictionarySubbitles, "Subbitles");
            this.SubbitlesName = ElementsNameCreate("Subbitles:");
            this.Subbitles.Title = DirectMovies.SubbitlesText;

            this.StorageDevice = ElementsEntryCreate("StorageDevice");
            this.StorageDeviceName = ElementsNameCreate("Storage Device:");
            this.StorageDevice.Text = DirectMovies.StorageDevice;

            this.Description = ElementsEditorCreate();
            this.DescriptionName = ElementsNameCreate("Description");
            this.Description.Text = DirectMovies.Description;

            this.LoanName = ElementsNameCreate("Loan:");
            this.LoanDescription = ElementsLoanDescriptionCreate();
            this.Loan = ElementsSwitchCreate(LoanToggledEventHandler);


            //bButtons
            //this.Cancel = ElementsButtonCreate("Back", CancelEventHandler);
            this.Cancel = new CustomButton(path: "iconBack.png");
            this.Cancel.Clicked(CancelTapGesture());

            //this.Delete = ElementsButtonCreate("Delete", DeleteEventHandler);
            this.Delete = new CustomButton(path: "iconDelete.png");
            this.Delete.Clicked(DeleteTapGesture());

            //this.Save = ElementsButtonCreate("Save", SaveEventHandler);
            this.Save = new CustomButton(path: "iconSave.png");
            this.Save.Clicked(SaveTapGesture());

        }



        private Entry ElementsEntryCreate(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.PlaceholderColor = Color.Black;
            name.FontSize = 15;
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }
        private Label ElementsNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
        private Picker ElementsPickerCreate(Dictionary<string, int> dictionary, string title)
        {
            Picker NewPicker = new Picker();
            NewPicker.Title = title;
            NewPicker.HorizontalOptions = LayoutOptions.Center;
            foreach (string items in dictionary.Keys)
            {
                NewPicker.Items.Add(items);
            }
            return NewPicker;
        }
        private DatePicker ElementsDatePicerCreate()
        {
            DatePicker NewDate = new DatePicker();
            NewDate.Format = "yyy-MM-dd";
            NewDate.HorizontalOptions = LayoutOptions.Center;
            return NewDate;
        }
        private Editor ElementsEditorCreate()
        {
            Editor NewEditor = new Editor();

            NewEditor.TextColor = Color.Black;
            NewEditor.FontSize = 15;
            NewEditor.HorizontalOptions = LayoutOptions.FillAndExpand;
            NewEditor.VerticalOptions = LayoutOptions.FillAndExpand;
            NewEditor.BackgroundColor = Color.FromHex("CCCCCC");

            return NewEditor;
        }
        private Button ElementsButtonCreate(string text, EventHandler akcja)
        {
            Button name = new Button();
            name.Text = text;
            name.Clicked += akcja;
            return name;
        }

        private Label ElementsLoanDescriptionCreate()
        {
            Label newLabel = new Label();
            if (DirectMovies.Loan == false)
                newLabel.Text = "No Loan";
            else
                newLabel.Text = "Loaned";
            return newLabel;
        }
        private Switch ElementsSwitchCreate(EventHandler<ToggledEventArgs> akcja)
        {
            Switch NewSwitch = new Switch();
            NewSwitch.Toggled += akcja;
            NewSwitch.IsToggled = DirectMovies.Loan;

            return NewSwitch;
        }
        private void LoanToggledEventHandler(object sender, ToggledEventArgs e)
        {
            if (e.Value == false)
            {
                this.LoanDescription.Text = "No Loan";
            }
            else
            {
                this.LoanDescription.Text = "Loaned";

            }
        }
        public int GetIDFromDictionary(int Licznik, Dictionary<string, int> dictionary, Picker picker)//Licznik  = 1;
        {

            if (picker.SelectedIndex != -1)
            {
                string ItemName = picker.Items[picker.SelectedIndex];
                Licznik = dictionary[ItemName];
            }

            return Licznik;
        }
        /*
        private void CancelEventHandler(object sender, EventArgs e)
        {

            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
        }
        */
        private TapGestureRecognizer CancelTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
            };
            return _tap;
        }
        /*
        private void DeleteEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Movies.DeleteOneMovies(DirectMovies.ID);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
            App.LocalNotification("Movie deleted");
        }
        */
        private TapGestureRecognizer DeleteTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_Movies.DeleteOneMovies(DirectMovies.ID);
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
                App.LocalNotification("Movie deleted");
            };
            return _tap;
        }
        /*
        private void SaveEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Movies.UpgradeMovies(DirectMovies.ID, Title.Text.ToString(), PublicationDate.Date,GetIDFromDictionary(1,DictionaryFormat,Format), GetIDFromDictionary(1,DictionaryQuality,Quality), GetIDFromDictionary(1,DictionaryLanguage,Language), GetIDFromDictionary(1,DictionarySubbitles,Subbitles), StorageDevice.Text.ToString(), Description.Text.ToString(), Loan.IsToggled);
            if (Loan.IsToggled == true)
            {
                App.MyLittleHomeDB_MoviesLoan.CreateMoviesLoan(DirectMovies.ID);
            }
            if (Loan.IsToggled == false)
            {
                App.MyLittleHomeDB_MoviesLoan.DeleteOneMoviesLoan(DirectMovies.ID);
            }

            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesEdit(DirectMovies.ID));
            
            App.LocalNotification("Movie edited");
        }
        */
        private TapGestureRecognizer SaveTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_Movies.UpgradeMovies(DirectMovies.ID, Title.Text.ToString(), PublicationDate.Date, GetIDFromDictionary(1, DictionaryFormat, Format), GetIDFromDictionary(1, DictionaryQuality, Quality), GetIDFromDictionary(1, DictionaryLanguage, Language), GetIDFromDictionary(1, DictionarySubbitles, Subbitles), StorageDevice.Text.ToString(), Description.Text.ToString(), Loan.IsToggled);
                if (Loan.IsToggled == true)
                {
                    App.MyLittleHomeDB_MoviesLoan.CreateMoviesLoan(DirectMovies.ID);
                }
                if (Loan.IsToggled == false)
                {
                    App.MyLittleHomeDB_MoviesLoan.DeleteOneMoviesLoan(DirectMovies.ID);
                }

                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMoviesEdit(DirectMovies.ID));

                App.LocalNotification("Movie edited");
            };
            return _tap;
        }

    }
}
