﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.AddMovies.TMDb_Api
{
   public class HelpTMDbView:ContentPage
    {
        TMDbClient webClient;
        SearchBar whatMovieSearch;
        SearchContainer<SearchMovie> allMoviesFromApi;
        ListView newList;
        StackLayout Stack;

        public HelpTMDbView()
        {
            webClient = new TMDbClient(Api.ApiKeySingleton.Instance.getTMDbApiKey());
            whatMovieSearch = new SearchBar();
            allMoviesFromApi = new SearchContainer<SearchMovie>();

            Stack = new StackLayout();
            addSearchBar();
            addListView();
            this.Content = Stack;
        }

        #region Dodanie elementów do widoku StackLayout
        private void addSearchBar()
        {
            whatMovieSearch = createSearchBar("Write title",pressSearchBar);
            Stack.Children.Add(whatMovieSearch);
        }
        private void addListView()
        {
            Stack.Children.Add(setGridName());
            Stack.Children.Add(movieFromApi());
        }
        #endregion

        #region Tworzenie i obsługa SearchBar'a
        private SearchBar createSearchBar(string placeholder, EventHandler akcja)
        {
            SearchBar newSearchBar = new SearchBar();
            newSearchBar.Placeholder = placeholder;
            newSearchBar.SearchButtonPressed += akcja;

            return newSearchBar;
        }
        private void pressSearchBar(object sender, EventArgs e)
        {
            allMoviesFromApi = webClient.SearchMovieAsync(((SearchBar)sender).Text.ToString()).Result;
            newList.IsRefreshing = true;
            if(allMoviesFromApi.Results.Count>0)
            {
                newListRefreshing();
                newList.IsRefreshing = false;
                App.LocalNotification("Download data");
            }
            else
            {
                newList.IsRefreshing = false;
                App.LocalNotification("No such data");
            }
        }
        #endregion


        #region Widok grida z nazwami elementów, które są pobierane z API
        private Grid setGridName()
        {
            Grid newGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                   // new ColumnDefinition { Width = GridLength.Star },
                }
            };
            newGrid.Children.Add(new Label { Text = "Date" }, 0, 1, 0, 1);
            newGrid.Children.Add(new Label { Text = "Title" }, 1, 2, 0, 1);
            return newGrid;
        }
        #endregion

        #region Widok listy, kliknięcie na element listy, odświeżenie listy
        private ListView movieFromApi()
        {
            newList = new ListView();
            newList.ItemsSource = allMoviesFromApi.Results;
            newList.ItemTemplate = new DataTemplate(typeof(dateTemplateListView));
            newList.IsPullToRefreshEnabled = true;
            newList.Refreshing += (sender, e1) => { newListRefreshing(); };
            newList.ItemTapped += newListItemTapped;
            newList.HasUnevenRows = true;
            newList.RowHeight = -1;
            return newList;
        }

        private void newListItemTapped(object sender, ItemTappedEventArgs e)
        {
            var te = ((SearchMovie)e.Item);
            HelpView.Elements.Title.Text = te.Title;
            HelpView.Elements.PublicationDate.Date = te.ReleaseDate.Value;
            HelpView.Elements.Description.Text = te.Overview;
            App.MasterDetailPage.Navigation.PopModalAsync();
            App.LocalNotification("Download data");
        }

        private void newListRefreshing()
        {
            //((ListView)sender).ItemsSource = dictionaryNameMovies;
            // ((ListView)sender).EndRefresh();
            newList.ItemsSource = allMoviesFromApi.Results;
            newList.EndRefresh();
        }
        #endregion
    }//Koniec classy HelpTMDbView



    public class dateTemplateListView:ViewCell
    {
        public Label yearMovies { get; private set; }
        public Label titleMovies { get; private set; }

        #region Konstruktor domyślny
        public dateTemplateListView():base()
        {
            View = setGridData();
        }
        #endregion

        #region Grid z bindowanymi elementami API
        private Grid setGridData()
        {
            createElements();
            Grid newGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                }
            };
            
            newGrid.Children.Add(yearMovies, 0, 1, 0, 1);
            newGrid.Children.Add(titleMovies, 1, 2, 0, 1);
            return newGrid;
        }
        #endregion

        #region Wywołąnie tworzenia elementów
        private void createElements()
        {
            yearMovies = createLabelDate("ReleaseDate");
            titleMovies = createLabelTitle("Title");
        }
        #endregion

        #region Metody tworzące elementy
        private Label createLabelTitle(string property)
        {
            Label newLabel = new Label();
            newLabel.SetBinding(Label.TextProperty, new Binding(path: property));
            return newLabel;
        }
        private Label createLabelDate(string property)
        {
            Label newLabel = new Label();
            newLabel.SetBinding(Label.TextProperty, new Binding(path: property, stringFormat: "{0:yyyy-MM-dd}"));
            newLabel.BindingContextChanged += NewLabel_BindingContextChanged;
            return newLabel;
        }

        private void NewLabel_BindingContextChanged(object sender, EventArgs e)
        {
            if(((Label)sender).Text=="")
            ((Label)sender).Text = "No data";
        }
        #endregion
    }


}
