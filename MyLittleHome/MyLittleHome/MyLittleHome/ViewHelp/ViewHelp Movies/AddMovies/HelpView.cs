﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;


using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;

namespace MyLittleHome.ViewHelp.ViewHelp_Movies.AddMovies
{
    public class HelpView:ContentPage
    {
      public static  HelpViewElements Elements;
        
        public HelpView()
        {
            Title = "Add new movies";
            Elements = new HelpViewElements();
         
            ScrollView Scroll = new ScrollView { Content = AddMoviesView() };
            Scroll.BackgroundColor = Color.FromHex("BDBDBD");
            this.Content = Scroll;
        }

        private Grid AddMoviesView()
        {
            Grid AddMovies = new Grid();
            AddMovies.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            AddMovies.RowDefinitions.Add(new RowDefinition { Height = GridLength.Star });

            AddMovies.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });

            AddMovies.Children.Add(GridView(), 0, 1, 0, 1);
            AddMovies.Children.Add(GridViewButton(), 0, 1, 1, 2);
          
            return AddMovies;
        }

        private Grid GridView()
        {

            Grid ViewGrid = new Grid
            {

                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = 160 },

                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                //   VerticalOptions = LayoutOptions.Start,
            };



            ViewGrid.Children.Add(Elements.TitleName, 0, 1, 0, 1);
            ViewGrid.Children.Add(Elements.Title, 1, 2, 0, 1);
            ViewGrid.Children.Add(Elements.Search, 2, 3, 0, 1);

            ViewGrid.Children.Add(Elements.PublicationDateName, 0, 1, 1, 2);
            ViewGrid.Children.Add(Elements.PublicationDate, 1, 3, 1, 2);

            ViewGrid.Children.Add(Elements.FormatName, 0, 1, 2, 3);
            ViewGrid.Children.Add(Elements.Format, 1, 3, 2, 3);

            ViewGrid.Children.Add(Elements.QualityName, 0, 1, 3, 4);
            ViewGrid.Children.Add(Elements.Quality, 1, 3, 3, 4);

            ViewGrid.Children.Add(Elements.LanguageName, 0, 1, 4, 5);
            ViewGrid.Children.Add(Elements.Language, 1, 3, 4, 5);

            ViewGrid.Children.Add(Elements.SubbitlesName, 0, 1, 5, 6);
            ViewGrid.Children.Add(Elements.Subbitles, 1, 3, 5, 6);

            ViewGrid.Children.Add(Elements.StorageDeviceName, 0, 1, 6, 7);
            ViewGrid.Children.Add(Elements.StorageDevice, 1, 3, 6, 7);

            ViewGrid.Children.Add(Elements.DescriptionName, 0, 3, 7, 8);
            ViewGrid.Children.Add(Elements.Description, 0, 3, 8, 9);
            return ViewGrid;
        }


        private Grid GridViewButton()
        {
            Grid GridButton = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 45 },
                },
                ColumnDefinitions =
                {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = GridLength.Star },

                },
                //  VerticalOptions = LayoutOptions.EndAndExpand,
                // HorizontalOptions = LayoutOptions.EndAndExpand,
            };
            GridButton.Children.Add(Elements.Cancel, 0, 1, 0, 1);
            GridButton.Children.Add(Elements.Save, 1, 2, 0, 1);

            return GridButton;
        }


    }
}
