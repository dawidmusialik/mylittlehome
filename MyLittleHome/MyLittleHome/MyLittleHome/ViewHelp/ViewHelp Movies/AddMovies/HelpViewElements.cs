﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Text;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;
using Xamarin.Forms;
using MyLittleHome.Component;


namespace MyLittleHome.ViewHelp.ViewHelp_Movies.AddMovies
{
    public class HelpViewElements
    {
        public HelpViewElements()
        {
            Elements();
        }

        public Entry Title { get; private set; }
        public DatePicker PublicationDate { get; private set; }
        public Picker Format { get; private set; }
        public Picker Quality { get; private set; }
        public Picker Language { get; private set; }
        public Picker Subbitles { get; private set; }
        public Entry StorageDevice { get; private set; }
        public Editor Description { get; private set; }


        public CustomButton Cancel { get; private set; }
        //public Button Cancel { get; private set; }
        public CustomButton Save { get; set; }
        //public Button Save { get; private set; }
        public CustomButton Search { get; private set; }
        //public Button Search { get; private set; }


        public Label TitleName { get; private set; }
        public Label PublicationDateName { get; private set; }
        public Label FormatName { get; private set; }
        public Label QualityName { get; private set; }
        public Label LanguageName { get; private set; }
        public Label SubbitlesName { get; private set; }
        public Label StorageDeviceName { get; private set; }
        public Label DescriptionName { get; private set; }

        Dictionary<string, int> DictionaryFormat = App.MyLittleHomeDB_MoviesFormat.GetMoviesFormatDictionary();
        Dictionary<string, int> DictionaryQuality = App.MyLittleHomeDB_MoviesQuality.GetMoviesQualityDictionary();
        Dictionary<string, int> DictionaryLanguage= App.MyLittleHomeDB_MoviesLanguage.GetMoviesLanguageDictionary();
        Dictionary<string, int> DictionarySubbitles = App.MyLittleHomeDB_MoviesSubbitles.GetMoviesFormatSubbitles();


        private void Elements()
        {
            this.Title = ElementsEntryCreate("Title");
            this.TitleName = ElementsNameCreate("Title:");
            this.PublicationDate = ElementsDatePicerCreate();
            this.PublicationDateName = ElementsNameCreate("Date:");
            this.Format = ElementsPickerCreate(DictionaryFormat, "Format");
            this.FormatName = ElementsNameCreate("Format:");
            this.Quality = ElementsPickerCreate(DictionaryQuality, "Quality");
            this.QualityName = ElementsNameCreate("Quality:");
            this.Language = ElementsPickerCreate(DictionaryLanguage,"Language");
            this.LanguageName = ElementsNameCreate("Name:");
            this.Subbitles = ElementsPickerCreate(DictionarySubbitles,"Subbitles");
            this.SubbitlesName = ElementsNameCreate("Subbitles:");
            this.StorageDevice = ElementsEntryCreate("StorageDevice");
            this.StorageDeviceName = ElementsNameCreate("Storage Device:");
            this.Description = ElementsEditorCreate();
            this.DescriptionName = ElementsNameCreate("Description");

            //this.Cancel = ElementsButtonCreate("Cancel", ButtonCancelEventHandler);
            this.Cancel = new CustomButton(path: "iconBack.png");
            this.Cancel.Clicked(ButtonCancelTapGesture());

            //this.Save = ElementsButtonCreate("Save", ButtonSaveEventHandler);
            this.Save = new CustomButton(path: "iconSave.png");
            this.Save.Clicked(ButtonSaveTapGesture());

            //this.Search = ElementsButtonCreate("GD", ButtonSearchEventHandler);
            //this.Search.FontSize = 10;
            this.Search = new CustomButton(path: "iconTMDB.png");
            this.Search.Clicked(ButtonSearchTapGesture());
            this.Search.WidthRequest = 50;
            this.Search.HeightRequest = 15;

        }


        private Entry ElementsEntryCreate(string placeholder)
        {
            Entry name = new Entry();
            name.Placeholder = placeholder;
            name.PlaceholderColor = Color.Black;
            name.FontSize = 15;
            name.HorizontalTextAlignment = TextAlignment.Center;
            return name;
        }
        private Label ElementsNameCreate(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
        private Picker ElementsPickerCreate(Dictionary<string, int> dictionary, string title)
        {
            Picker NewPicker = new Picker();
            NewPicker.Title = title;


            NewPicker.HorizontalOptions = LayoutOptions.CenterAndExpand;
           // NewPicker.VerticalOptions = LayoutOptions.CenterAndExpand;
            foreach (string items in dictionary.Keys)
            {
                NewPicker.Items.Add(items);
            }
            return NewPicker;
        }
        private DatePicker ElementsDatePicerCreate()
        {
            DatePicker NewDate = new DatePicker();
            NewDate.Format = "yyy-MM-dd";
            NewDate.HorizontalOptions = LayoutOptions.CenterAndExpand;
            return NewDate;
        }
        private Editor ElementsEditorCreate()
        {
            Editor NewEditor = new Editor();

            NewEditor.TextColor = Color.Black;
            NewEditor.FontSize = 15;
            NewEditor.HorizontalOptions = LayoutOptions.FillAndExpand;
            NewEditor.VerticalOptions = LayoutOptions.FillAndExpand;
            NewEditor.BackgroundColor = Color.FromHex("CCCCCC");

            return NewEditor;
        }
        private Button ElementsButtonCreate(string text, EventHandler akcja)
        {
            Button name = new Button();
            name.Text = text;
            name.Clicked += akcja;
            return name;
        }
        /*
        private void ButtonCancelEventHandler(object sender, EventArgs e)
        {
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
        }
        */
        private TapGestureRecognizer ButtonCancelTapGesture()
        {
            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
            };
            return tap;
        }

        /*
        private void ButtonSaveEventHandler(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Movies.InsertMovie(Title.Text.ToString(), PublicationDate.Date, GetIDFromDictionary(1,DictionaryFormat,Format), GetIDFromDictionary(1,DictionaryQuality,Quality), GetIDFromDictionary(1,DictionaryLanguage,Language),GetIDFromDictionary(1,DictionarySubbitles,Subbitles),StorageDevice.Text.ToString(),Description.Text.ToString(), false);
            App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
            App.LocalNotification("New movie added");
        }
        */
        private TapGestureRecognizer ButtonSaveTapGesture()
        {
            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) => {
                if (Title != null  && PublicationDate.Date != null && Format.SelectedItem != null && Quality.SelectedItem != null && Language.SelectedItem != null && Subbitles.SelectedItem != null && StorageDevice != null && Description != null)
                {
                    App.MyLittleHomeDB_Movies.InsertMovie(Title.Text.ToString(), PublicationDate.Date, GetIDFromDictionary(1, DictionaryFormat, Format), GetIDFromDictionary(1, DictionaryQuality, Quality), GetIDFromDictionary(1, DictionaryLanguage, Language), GetIDFromDictionary(1, DictionarySubbitles, Subbitles), StorageDevice.Text.ToString(), Description.Text.ToString(), false);
                    App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Movies.ViewMovies());
                    App.LocalNotification("New movie added");
                }
                else
                {
                    App.LocalNotification("Check the boxes");
                }

            };
            return tap;
        }
        /*
        private void ButtonSearchEventHandler(object sender, EventArgs e)
        {
            if(CrossConnectivity.Current.IsConnected == true)
            {
                App.MasterDetailPage.Detail.Navigation.PushModalAsync(new ViewHelp.ViewHelp_Movies.AddMovies.TMDb_Api.HelpTMDbView());
            }
            else
            {
                App.LocalNotification("Check the internet connections");
            }
            
        }
        */
        private TapGestureRecognizer ButtonSearchTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                if (CrossConnectivity.Current.IsConnected == true && Api.ApiKeySingleton.Instance.getTMDbApiKey() != "")
                {
                    App.MasterDetailPage.Detail.Navigation.PushModalAsync(new ViewHelp.ViewHelp_Movies.AddMovies.TMDb_Api.HelpTMDbView());
                }
                else
                {
                    App.LocalNotification("Check the internet connections or Api Key");
                }
            };
            return _tap;
        }

        public int GetIDFromDictionary(int Licznik, Dictionary<string, int> dictionary, Picker picker)//Licznik  = 1;
        {

            if (picker.SelectedIndex != -1)
            {
                string ItemName = picker.Items[picker.SelectedIndex];
                Licznik = dictionary[ItemName];
            }

            return Licznik;
        }
    }
}
