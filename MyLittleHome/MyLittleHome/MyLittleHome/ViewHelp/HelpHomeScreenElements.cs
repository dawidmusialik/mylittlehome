﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp
{
    public class HelpHomeScreenElements
    {
        private Helps.HelpItemDatabase _amountItemDatabase;

        public Label booksAmountName { get; set; }
        public Label booksAmountFormatName { get; set; }
        public Label booksAmount { get; set; }
        public Label booksAmountFormat { get; set; }
        
        public Label budgetAmountName { get; set; }
        public Label budgetAmountValueName { get; set; }
        public Label budgetAmount { get; set; }
        public Label budgetAmountValue { get; set; }

        public Label moviesAmountName { get; set; }
        public Label moviesAmountFormatName { get; set; }
        public Label moviesAmountLanguageName { get; set; }
        public Label moviesAmountQualityName { get; set; }
        public Label moviesAmountSubbitlesName { get; set; }
        public Label moviesAmount { get; set; }
        public Label moviesAmountFormat { get; set; }
        public Label moviesAmountLanguage { get; set; }
        public Label moviesAmountQuality { get; set; }
        public Label moviesAmountSubbitles { get; set; }

        public Label musicAmountName { get; set; }
        public Label musicAmountFormatName { get; set; }
        public Label musicAmount { get; set; }
        public Label musicAmountFormat { get; set; }

        public HelpHomeScreenElements()
        {
            _amountItemDatabase = new Helps.HelpItemDatabase();

            createElementsBooks();
            createElementsBudget();
            createElementsMovies();
            createElementsMusic();
        }

        public void refresItem()
        {
            setElementsBooks();
            setElementsBudget();
            setElementsMovies();
            setElementsMusic();
        }

        private void createElementsBooks()
        {
            booksAmount = new Label();
            booksAmountName = new Label();
            booksAmountName.Text = "Number of books:";

            booksAmountFormat = new Label();
            booksAmountFormatName = new Label();
            booksAmountFormatName.Text = "Number of format:";

            setElementsBooks();
        }

        private void setElementsBooks()
        {
            Helps.ItemBooksDatabase _amountBooks = _amountItemDatabase.getAmountOfBooks();
            booksAmount.Text = _amountBooks.amountBooks.ToString();
            booksAmountFormat.Text = _amountBooks.amountBooksFormat.ToString();
        }

        private void createElementsBudget()
        {
            budgetAmount = new Label();
            budgetAmountName = new Label();
            budgetAmountName.Text = "Number of budgets:";

            budgetAmountValue = new Label();
            budgetAmountValueName = new Label();
            budgetAmountValueName.Text = "Values of budgets:";

            setElementsBudget();
        }
        private void setElementsBudget()
        {
            Helps.ItemBudgetDatabase _amountBudgets = _amountItemDatabase.getAmountOfBudget();
            budgetAmount.Text = _amountBudgets.amountBudget.ToString();
            budgetAmountValue.Text = _amountBudgets.amountBudgetValue.ToString();
        }

        private void createElementsMovies()
        {
            moviesAmount = new Label();
            moviesAmountName = new Label();
            moviesAmountName.Text = "Number of movies";

            moviesAmountFormat = new Label();
            moviesAmountFormatName = new Label();
            moviesAmountFormatName.Text = "Numer of format:";

            moviesAmountLanguage = new Label();
            moviesAmountLanguageName = new Label();
            moviesAmountLanguageName.Text = "Number of language:";

            moviesAmountQuality = new Label();
            moviesAmountQualityName = new Label();
            moviesAmountQualityName.Text = "Number of quality:";

            moviesAmountSubbitles = new Label();
            moviesAmountSubbitlesName = new Label();
            moviesAmountSubbitlesName.Text = "Number of subbitles:";

            setElementsMovies();
        }
        private void setElementsMovies()
        {
            Helps.ItemMoviesDatabase _amountMovies = _amountItemDatabase.getAmountOfMovies();
            moviesAmount.Text = _amountMovies.amountMovies.ToString();
            moviesAmountFormat.Text = _amountMovies.amountMoviesFormat.ToString();
            moviesAmountLanguage.Text = _amountMovies.amountMoviesLanguage.ToString();
            moviesAmountQuality.Text = _amountMovies.amountMoviesQuality.ToString();
            moviesAmountSubbitles.Text = _amountMovies.amountMoviesSubbitles.ToString();
        }

        private void createElementsMusic()
        {
            musicAmount = new Label();
            musicAmountName = new Label();
            musicAmountName.Text = "Number of albums:";

            musicAmountFormat = new Label();
            musicAmountFormatName = new Label();
            musicAmountFormatName.Text = "Number of format:";

            setElementsMusic();
        }
        private void setElementsMusic()
        {
            Helps.ItemMusicDatabase _amountMusic = _amountItemDatabase.getAmountOfMusic();
            musicAmount.Text = _amountMusic.amountMusic.ToString();
            musicAmountFormat.Text = _amountMusic.amountMusicFormat.ToString();
        }
    }
}
