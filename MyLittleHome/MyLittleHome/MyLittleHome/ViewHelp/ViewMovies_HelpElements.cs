﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
    public class ViewMovies_HelpElements
    {

        //Elementy na dane z tabeli Movies
        private Label ID;
        private Label Title;
        private Label PublicationDate;
        private Label Format;
        private Label Quality;
        private Label Language;
        private Label Subbitles;
        private Label StorageDevice;
        private Label Description;
        //Elementy na nazwy danych z tabeli Movies
        private Label ID_Name;
        private Label Title_Name;
        private Label PublicationDate_Name;
        private Label Format_Name;
        private Label Quality_Name;
        private Label Language_Name;
        private Label Subbitles_Name;
        private Label StorageDevice_Name;
        private Label Description_Name;



        //Pobieranie Elementów 
        public Label Get_Elements(string name)
        {
            switch (name)
            {
                case "ID":
                    return ID;
                case "Title":
                    return Title;
                case "PublicationDate":
                    return PublicationDate;
                case "Format":
                    return Format;
                case "Quality":
                    return Quality;
                case "Language":
                    return Language;
                case "Subbitles":
                    return Subbitles;
                case "StorageDevice":
                    return StorageDevice;
                case "Description":
                    return Description;
                case "ID_Name":
                    return ID_Name;
                case "Title_Name":
                    return Title_Name;
                case "PublicationDate_Name":
                    return PublicationDate_Name;
                case "Format_Name":
                    return Format_Name;
                case "Quality_Name":
                    return Quality_Name;
                case "Language_Name":
                    return Language_Name;
                case "Subbitles_Name":
                    return Subbitles_Name;
                case "StorageDevice_Name":
                    return StorageDevice_Name;
                case "Description_Name":
                    return Description_Name;

                default:
                    return new Label();
            }

        }


        //Deklaracja funkcji stworzonych Elementy
        public void Elements()
        {
            //Elementy pobierające dane z tabeli Movies
            Elements_Create(ref ID, "ID");
            Elements_Create(ref Title, "Title");
            Elements_Create(ref PublicationDate, "PublicationDate");
            Elements_Create(ref Format, "Format");
            Elements_Create(ref Quality, "Quality");
            Elements_Create(ref Language, "Language");
            Elements_Create(ref Subbitles, "Subbitles");
            Elements_Create(ref StorageDevice, "StorageDevice");
            Elements_Create(ref Description, "Description");

            //Elementy nazw danych do tabeli Movies
            Elements_Name_Create(ref ID_Name, "ID:");
            Elements_Name_Create(ref Title_Name, "Title:");
            Elements_Name_Create(ref PublicationDate_Name, "Year:");
            Elements_Name_Create(ref Format_Name, "Format:");
            Elements_Name_Create(ref Quality_Name, "Quality:");
            Elements_Name_Create(ref Language_Name, "Language:");
            Elements_Name_Create(ref Subbitles_Name, "Subbitles:");
            Elements_Name_Create(ref StorageDevice_Name, "Storage Device:");
            Elements_Name_Create(ref Description_Name, "Description:");
        }


        //Funkcja tworząca elementy, które zawierają dane z tabeli Movies
        private Label Elements_Create(ref Label name, string property)
        {
            name = new Label();
            name.SetBinding(Label.TextProperty, property);
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
        //Funkcja tworząca nazwy elementów, które zawierają dane z tabeli Movies
        private Label Elements_Name_Create(ref Label name, string text)
        {
            name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }

    }
}
