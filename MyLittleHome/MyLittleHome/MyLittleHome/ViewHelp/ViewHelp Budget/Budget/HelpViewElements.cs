﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.Budget
{
    public class HelpViewElements
    {
        public static int budgetNumber = 0;
        public HelpViewElements()
        {
            createElements();
        }

        public Label labelName { get; private set; }
        public Label labelBudgetName { get; set; }

       // public Label labelDescription { get; private set; }
       // public Label labelBudgetDescription { get; set; }

        public Label labelStatus { get; private set; }
        public Label labelBudgetStatus { get; set; }

        public Label labelNumber { get; set; }
        public Label labelBudgetNumber { get; set; }

        private void createElements()
        {
            this.labelName = createLabelName("Name:");
            this.labelStatus = createLabelName("Status:");
           // this.labelDescription = createLabelName("Descriptions");
            this.labelBudgetName = createLabelBindning("budgetName");
            this.labelBudgetStatus = createLabelBindning("budgetStatus");
           // this.labelBudgetDescription = createLabelBindning("budgetDescription");
            this.labelNumber = createLabelName("Nr:");
            this.labelBudgetNumber = new Label();
        }

        private Label createLabelBindning(string property)
        {
            Label newLabel = new Label();
            newLabel.SetBinding(Label.TextProperty, new Binding(path: property));
            newLabel.HorizontalTextAlignment = TextAlignment.Center;
            newLabel.VerticalTextAlignment = TextAlignment.Center;
            return newLabel;
        }
        private Label createLabelName(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
    }
}
