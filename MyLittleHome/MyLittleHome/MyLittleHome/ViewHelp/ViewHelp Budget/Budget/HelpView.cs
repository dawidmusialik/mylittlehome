﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.Budget
{
    public class HelpView: ViewCell
    {
        HelpViewElements viewElements;

        public HelpView()
        {
            this.View = GridView();
        }

        private Grid GridView()
        {
            viewElements = new HelpViewElements();
            HelpViewElements.budgetNumber++;
            viewElements.labelBudgetNumber.Text = Convert.ToString(HelpViewElements.budgetNumber);
            
            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                  
                   
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                   new ColumnDefinition { Width = GridLength.Star},
                   new ColumnDefinition { Width = GridLength.Star},
                 


                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };



            grid.Children.Add(NumberinGrid(), 0, 1, 0, 1);
            grid.Children.Add(BudgetNameinGrid(), 1, 2, 0, 1);
            grid.Children.Add(BudgetStatusinGrid(), 2, 3, 0, 1);
           // grid.Children.Add(DescriptiontinGrid(), 0, 3, 1, 2);
       




            return grid;
        }

        private StackLayout NumberinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(viewElements.labelNumber);
            Stack.Children.Add(viewElements.labelBudgetNumber);

            return Stack;
        }
        private StackLayout BudgetNameinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(viewElements.labelName);
            Stack.Children.Add(viewElements.labelBudgetName);

            return Stack;
        }

        private StackLayout BudgetStatusinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(viewElements.labelStatus);
            Stack.Children.Add(viewElements.labelBudgetStatus);

            return Stack;
        }
        private StackLayout DescriptiontinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
           // Stack.Children.Add(viewElements.labelDescription);
           // Stack.Children.Add(viewElements.labelBudgetDescription);

            return Stack;
        }
    }
}
