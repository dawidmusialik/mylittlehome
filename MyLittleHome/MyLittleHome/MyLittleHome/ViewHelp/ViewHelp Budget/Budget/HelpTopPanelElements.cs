﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.Budget
{
    public class HelpTopPanelElements
    {

        public Label TitlePage { get; private set; }
       // public Button ButtonAddBudget { get; private set; }
       // public Button ButtonEditBudget { get; set; }
       public CustomButton ButtonAddBudget { get; private set; }

        public HelpTopPanelElements()
        {
            CreateElements();
        }

        private void CreateElements()
        {
            this.TitlePage = CreateLabel("My Budget");
          //  this.ButtonAddBudget = CreateButton("Add", ButtonAddBudgetEventHandler);
            this.ButtonAddBudget = new CustomButton(path: "iconAdd.png");
            this.ButtonAddBudget.Clicked(ButtonAddBudgetTapGesture());
         //   this.ButtonEditBudget = CreateButton("Edit Budget", null);
        }

        private Label CreateLabel(string tekst)
        {
            Label Title = new Label();
            Title.Text = tekst;
            Title.FontSize = 30;
            Title.TextColor = Color.FromHex("57FF9D");
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.VerticalOptions = LayoutOptions.Center;
            Title.HorizontalOptions = LayoutOptions.Center;
            return Title;
        }

        private Button CreateButton(string tekst, EventHandler akcja)
        {
            Button button = new Button();
            button.Text = tekst;
            button.Clicked += akcja;
            button.VerticalOptions = LayoutOptions.End;
            button.HorizontalOptions = LayoutOptions.End;

            return button;
        }

        /*
        private void ButtonAddBudgetEventHandler(object sender, EventArgs e)
        {
            App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Budget.ViewAddBudget());
        }
        */
        private TapGestureRecognizer ButtonAddBudgetTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MasterDetailPage.Detail.Navigation.PushAsync(new View.Budget.ViewAddBudget());
            };
            return _tap;
        }

    }
}
