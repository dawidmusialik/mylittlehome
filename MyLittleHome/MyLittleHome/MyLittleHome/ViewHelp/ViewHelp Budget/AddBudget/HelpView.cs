﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.AddBudget
{
    public class HelpView : StackLayout
    {
        HelpViewElements viewElements;
        public HelpView()
        {
            viewElements = new HelpViewElements();
            this.Children.Add(viewElements.Name);
            this.Children.Add(viewElements.budgetName);
            this.Children.Add(viewElements.Status);
            this.Children.Add(viewElements.budgetStatus);
            this.Children.Add(viewElements.Description);
            this.Children.Add(viewElements.budgetDescription);
            this.Children.Add(new BoxView());
            this.Children.Add(viewElements.saveNewBudget);


        }
    }
}
