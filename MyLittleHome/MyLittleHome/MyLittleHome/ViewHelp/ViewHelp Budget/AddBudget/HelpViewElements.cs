﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.AddBudget
{
    public class HelpViewElements
    {

        public Label Name { get; set; }
        public Label Description { get; set; }
        public Label Status { get; set; }


        public Entry budgetName { get; set; }
        public Editor budgetDescription { get; set; }
        public Entry budgetStatus { get; set; }

        //public Button saveNewBudget { get; set; }
        public CustomButton saveNewBudget { get; set; }

        public HelpViewElements()
        {
            createElements();
        }

        private void createElements()
        {
            Name = new Label();
            Name.Text = "Name:";

            Description = new Label();
            Description.Text = "Description:";

            Status = new Label();
            Status.Text = "Status";

            budgetName = new Entry();
            budgetName.Placeholder = "Write budget name";

            budgetDescription = new Editor();
            budgetDescription.BackgroundColor = Color.FromHex("CCCCCC");


            budgetStatus = new Entry();
            budgetStatus.Placeholder = "Write value of budget";

            //saveNewBudget = new Button();
            //saveNewBudget.Text = "Save";
            //saveNewBudget.Clicked += ButtonSaveEventHandler;
            saveNewBudget = new CustomButton(path: "iconSave.png");
            saveNewBudget.Clicked(ButtonSaveTapGesture());
            this.saveNewBudget.WidthRequest = 70;
            this.saveNewBudget.HeightRequest = 70;
        }
        /*
        private void ButtonSaveEventHandler(object sender, EventArgs e)
        {
            if (budgetStatus.Text != null)
            {
                App.MyLittleHomeDB_Budget.insertNewBudget(budgetName.Text, budgetDescription.Text, float.Parse(budgetStatus.Text));
                App.MasterDetailPage.Detail.Navigation.PopAsync();
                App.LocalNotification("Budget add");
            }
            else
            {
                App.MyLittleHomeDB_Budget.insertNewBudget(budgetName.Text, budgetDescription.Text);
                App.MasterDetailPage.Detail.Navigation.PopAsync();
                App.LocalNotification("Budget add");
            }
        }
        */
        private TapGestureRecognizer ButtonSaveTapGesture()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                var test = budgetName;
                if(budgetName.Text != null)
                {
                    if (budgetStatus.Text != null)
                    {
                        App.MyLittleHomeDB_Budget.insertNewBudget(budgetName.Text, budgetDescription.Text, float.Parse(budgetStatus.Text));
                        App.MasterDetailPage.Detail.Navigation.PopAsync();
                        App.LocalNotification("Budget add");
                    }
                    else
                    {
                        App.MyLittleHomeDB_Budget.insertNewBudget(budgetName.Text, budgetDescription.Text);
                        App.MasterDetailPage.Detail.Navigation.PopAsync();
                        App.LocalNotification("Budget add");
                    }
                }
                else
                {
                    App.LocalNotification("Check the budget");
                }
            };
            return _tap;
        }
    }
}
