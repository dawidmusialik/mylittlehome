﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;
using MyLittleHome.Component;

namespace MyLittleHome.ViewHelp.ViewHelp_Budget.EditBudget
{
    public class HelpViewElements
    {
        public Label labelName { get; set;}
        public Label labelDescription { get; set; }
        public Entry entryBudgetName { get; set; }
        public Editor editorBudgetDescription { get; set; }
        //public Button buttonSaveBudget { get; set; }
        public CustomButton buttonSaveBudget { get; set; }

        public HelpViewElements()
        {
            createElements();
        }


        private void createElements()
        {
            this.labelName = new Label();
            this.labelName.Text = "Budget name:";

            this.labelDescription = new Label();
            this.labelDescription.Text = "Description:";

            this.entryBudgetName = new Entry();

            this.editorBudgetDescription = new Editor();

           // this.buttonSaveBudget = new Button();
           // this.buttonSaveBudget.Text = "Save";

            this.buttonSaveBudget = new CustomButton(path: "iconSave.png");
            this.buttonSaveBudget.WidthRequest = 50;
            this.buttonSaveBudget.HeightRequest = 50;
        }
    }
}
