﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.EditBudget
{
    public class HelpView:StackLayout
    {
        HelpViewElements viewElements;
        public HelpView(DataBase.TableBudget _tableBudget)
        {
            viewElements = new HelpViewElements();

            this.Children.Add(viewElements.labelName);
            viewElements.entryBudgetName.Placeholder = _tableBudget.budgetName;
            this.Children.Add(viewElements.entryBudgetName);

            this.Children.Add(viewElements.labelDescription);
            viewElements.editorBudgetDescription.Text = _tableBudget.budgetDescription;
            this.Children.Add(viewElements.editorBudgetDescription);
            /*
            viewElements.buttonSaveBudget.Clicked += (sender, e) =>
            {
                string _localString = "";
                if(viewElements.entryBudgetName.Text == null)
                {
                    viewElements.entryBudgetName.Text = viewElements.entryBudgetName.Placeholder;
                }
                if(viewElements.editorBudgetDescription.Text == null)
                {
                    viewElements.editorBudgetDescription.Text = "";
                }
                if(viewElements.editorBudgetDescription.Text != null && viewElements.entryBudgetName.Text != null)
                {
                    App.MyLittleHomeDB_Budget.editBudget(_tableBudget.budgetID, viewElements.entryBudgetName.Text, viewElements.editorBudgetDescription.Text);
                    App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Budget.ViewBudget());
                    App.LocalNotification("Budget edited");
                }
            };
            */
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                string _localString = "";
                if (viewElements.entryBudgetName.Text == null)
                {
                    viewElements.entryBudgetName.Text = viewElements.entryBudgetName.Placeholder;
                }
                if (viewElements.editorBudgetDescription.Text == null)
                {
                    viewElements.editorBudgetDescription.Text = "";
                }
                if (viewElements.editorBudgetDescription.Text != null && viewElements.entryBudgetName.Text != null)
                {
                    App.MyLittleHomeDB_Budget.editBudget(_tableBudget.budgetID, viewElements.entryBudgetName.Text, viewElements.editorBudgetDescription.Text);
                    App.MasterDetailPage.Detail = new Component.CustomNavigationPage(new View.Budget.ViewBudget());
                    App.LocalNotification("Budget edited");
                }
            };


            viewElements.buttonSaveBudget.Clicked(_tap);
            


            //this.Children.Add(new BoxView());
            //this.Children.Add(new BoxView());
            //this.Children.Add(new BoxView());

            this.Children.Add(viewElements.buttonSaveBudget);
        }
    }
}
