﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.EditOperation
{
    public class HelpView:StackLayout
    {
        HelpViewElements viewElements;
        HelpBottomPanelElements viewBottomElements;
        DataBase.Budget.TableBudgetOperation _tableBudget;
        public HelpView(DataBase.Budget.TableBudgetOperation currentOperation)
        {
            _tableBudget = new DataBase.Budget.TableBudgetOperation();
            _tableBudget = currentOperation;
            viewElements = new HelpViewElements();
            viewBottomElements = new HelpBottomPanelElements();

            viewElements.operationDescription.Text = currentOperation.operationDescription;
            if(currentOperation.operationDeposit != 0)
            {
                viewElements.operationValue.Text = currentOperation.operationDeposit.ToString();

            }
            if (currentOperation.operationExpense != 0)
            {
                viewElements.operationValue.Text = currentOperation.operationExpense.ToString();
            }

            this.Children.Add(viewElements.ValueName);
            this.Children.Add(viewElements.operationValue);
            this.Children.Add(viewElements.DescriptionName);
            this.Children.Add(viewElements.operationDescription);

            StackLayout _bottomPanel = new StackLayout();
            _bottomPanel.Orientation = StackOrientation.Horizontal;
            _bottomPanel.VerticalOptions = LayoutOptions.Center;
            _bottomPanel.HorizontalOptions = LayoutOptions.Center;
            _bottomPanel.Children.Add(viewBottomElements.saveOperation);
            _bottomPanel.Children.Add(viewBottomElements.deleteOperation);

            //viewBottomElements.saveOperation.Clicked += saveOperationClicked;
            //viewBottomElements.deleteOperation.Clicked += deleteOperationClicked;
            viewBottomElements.deleteOperation.Clicked(deleteOperationClicked());
            viewBottomElements.saveOperation.Clicked(saveOperationClicked());


            this.Children.Add(_bottomPanel);
        }
        /*
        private void deleteOperationClicked(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Budget.deleteOperation(_tableBudget.operationID);
            App.MasterDetailPage.Detail.Navigation.PopAsync();
            App.LocalNotification("Operation deleted");
        }
        */
        private TapGestureRecognizer deleteOperationClicked()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_Budget.deleteOperation(_tableBudget.operationID);
                App.MasterDetailPage.Detail.Navigation.PopAsync();
                App.LocalNotification("Operation deleted");
            };
            return _tap;
        }
        /*
        private void saveOperationClicked(object sender, EventArgs e)
        {
            App.MyLittleHomeDB_Budget.editOperation(_tableBudget.operationID, viewElements.operationDescription.Text, float.Parse(viewElements.operationValue.Text));
            App.MasterDetailPage.Detail.Navigation.PopAsync();
            App.LocalNotification("Operation updated");
        }
        */
        private TapGestureRecognizer saveOperationClicked()
        {
            TapGestureRecognizer _tap = new TapGestureRecognizer();
            _tap.Tapped += (sender, e) => {
                App.MyLittleHomeDB_Budget.editOperation(_tableBudget.operationID, viewElements.operationDescription.Text, float.Parse(viewElements.operationValue.Text));
                App.MasterDetailPage.Detail.Navigation.PopAsync();
                App.LocalNotification("Operation updated");
            };
            return _tap;
        }
    }
}
