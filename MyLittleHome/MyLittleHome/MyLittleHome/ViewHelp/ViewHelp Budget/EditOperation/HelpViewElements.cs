﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp.ViewHelp_Budget.EditOperation
{
   public class HelpViewElements
    {
        public Label ValueName { get; set; }
        public Label DescriptionName { get; set; }

        public Entry operationValue { get; set; }
        public Editor operationDescription { get; set; }

       
        public HelpViewElements()
        {
            createElements();
        }

        private void createElements()
        {
            this.ValueName = new Label();
            this.ValueName.Text = "Value:";

            this.DescriptionName = new Label();
            this.DescriptionName.Text = "Description";

            this.operationValue = new Entry();

            this.operationDescription = new Editor();

        }
    }
}
