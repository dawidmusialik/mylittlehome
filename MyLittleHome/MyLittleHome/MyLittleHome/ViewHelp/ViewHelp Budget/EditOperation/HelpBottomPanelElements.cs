﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.EditOperation
{
    public class HelpBottomPanelElements
    {
        //public Button saveOperation { get; set; }
        //public Button deleteOperation { get; set; }
        public CustomButton saveOperation { get; set; }
        public CustomButton deleteOperation { get; set; }

        public HelpBottomPanelElements()
        {
            createElements();
        }

        private void createElements()
        {

            //this.saveOperation = new Button();
            //this.saveOperation.Text = "Save";

            //this.deleteOperation = new Button();
            //this.deleteOperation.Text = "Delete";

            this.saveOperation = new CustomButton(path: "iconSave.png");
            this.saveOperation.HeightRequest = 40;
            this.saveOperation.WidthRequest = 40;

            this.deleteOperation = new CustomButton(path: "iconDelete.png");
            this.deleteOperation.HeightRequest = 40;
            this.deleteOperation.WidthRequest = 40;
        }
    }
}
