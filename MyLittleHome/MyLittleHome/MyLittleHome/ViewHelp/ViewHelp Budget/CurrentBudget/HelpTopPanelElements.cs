﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.CurrentBudget
{
   public class HelpTopPanelElements
    {
        public Label labelOperations { get; private set; }
        public Label labelBudgetOperations { get; set; }

        public Label labelDescription { get; private set; }
        public Label labelBudgetDescription { get; set; }

        public Label labelStatus { get; private set; }
        public Label labelBudgetStatus { get; set; }

        //public Button buttonDeleteBudget { get; set; }
        public CustomButton buttonDeleteBudget { get; set; }



        public HelpTopPanelElements()
        {
            createElements();
        }
        private void createElements()
        {
            this.labelStatus = createLabelName("Status:");
            this.labelOperations = createLabelName("Operations:");
            this.labelDescription = createLabelName("Descriptions:");
            this.labelBudgetStatus = new Label();
            this.labelBudgetDescription = new Label();
           this.labelBudgetOperations = new Label();
            // this.buttonDeleteBudget = new Button();
            // this.buttonDeleteBudget.Text = "Delete";
            this.buttonDeleteBudget = new CustomButton(path: "iconDelete.png");
            this.buttonDeleteBudget.WidthRequest = 40;
            this.buttonDeleteBudget.HeightRequest = 40;
        }

        private Label createLabelName(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
    }
}
