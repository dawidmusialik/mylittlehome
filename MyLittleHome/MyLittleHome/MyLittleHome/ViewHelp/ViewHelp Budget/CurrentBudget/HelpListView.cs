﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.CurrentBudget
{
   public class HelpListView:ViewCell
    {
        HelpListViewElements viewElements;
        public HelpListView()
        {
            this.View = GridView();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (((DataBase.Budget.TableBudgetOperation)BindingContext).operationDeposit != 0)
            {
                viewElements.labelOperationValue.Text = ((DataBase.Budget.TableBudgetOperation)BindingContext).operationDeposit.ToString();
            }
            if (((DataBase.Budget.TableBudgetOperation)BindingContext).operationExpense != 0)
            {
                viewElements.labelOperationValue.Text = ((DataBase.Budget.TableBudgetOperation)BindingContext).operationExpense.ToString();
            }
        }
        
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
        }

        private Grid GridView()
        {
            viewElements = new HelpListViewElements();
            HelpListViewElements.operationNumber++;
            viewElements.labelOperationNumber.Text = Convert.ToString(HelpListViewElements.operationNumber);

            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },
                   new RowDefinition { Height = GridLength.Auto },


                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                   new ColumnDefinition { Width = GridLength.Star},
                   new ColumnDefinition { Width = GridLength.Star},



                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };



            grid.Children.Add(NumberinGrid(), 0, 1, 0, 1);
            grid.Children.Add(BudgetOperationValueinGrid(), 2, 3, 0, 1);
            //grid.Children.Add(DescriptiontinGrid(), 0, 3, 1, 2);
            grid.Children.Add(viewElements.labelDescription, 0, 1, 1, 3);
            grid.Children.Add(viewElements.labelOperationDescription, 1, 3, 1, 3);

            // grid.Children.Add(DescriptiontinGrid(), 0, 3, 1, 2);

            return grid;
        }
        private StackLayout NumberinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(viewElements.labelNumber);
            Stack.Children.Add(viewElements.labelOperationNumber);

            return Stack;
        }
        private StackLayout BudgetOperationValueinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            //  Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(viewElements.labelOperation);
            Stack.Children.Add(viewElements.labelOperationValue);

            return Stack;
        }
        
        private StackLayout DescriptiontinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Blue;
             Stack.Children.Add(viewElements.labelDescription);
             Stack.Children.Add(viewElements.labelOperationDescription);

            return Stack;
        }
    }
}
