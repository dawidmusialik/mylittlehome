﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.CurrentBudget
{
    public class HelpListViewElements
    {
        public static int operationNumber = 0;

        public Label labelNumber { get; set; }
        public Label labelOperationNumber { get; set; }

        public Label labelOperation { get; set; }
        public Label labelOperationValue { get; set; }

        public Label labelDescription { get; set; }
        public Label labelOperationDescription { get; set; }

        public HelpListViewElements()
        {
            createElements();
        }
        private void createElements()
        {
            this.labelNumber = createLabelName("Nr:");
            this.labelOperationNumber = new Label();
            this.labelOperation = createLabelName("Value:");
            this.labelOperationValue = new Label();
            this.labelDescription = createLabelName("Description:");
            this.labelOperationDescription = createLabelBindning("operationDescription");
        }
        private Label createLabelBindning(string property)
        {
            Label newLabel = new Label();
            newLabel.SetBinding(Label.TextProperty, new Binding(path: property));
            newLabel.HorizontalTextAlignment = TextAlignment.Center;
            newLabel.VerticalTextAlignment = TextAlignment.Center;
            return newLabel;
        }
        private Label createLabelName(string text)
        {
            Label name = new Label();
            name.Text = text;
            name.TextColor = Color.Red;
            name.FontSize = 17;
            name.HorizontalTextAlignment = TextAlignment.Center;
            name.VerticalTextAlignment = TextAlignment.Center;
            return name;
        }
    }
}
