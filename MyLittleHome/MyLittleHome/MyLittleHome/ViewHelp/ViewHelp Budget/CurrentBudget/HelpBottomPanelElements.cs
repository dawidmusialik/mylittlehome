﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MyLittleHome.Component;
namespace MyLittleHome.ViewHelp.ViewHelp_Budget.CurrentBudget
{
    public class HelpBottomPanelElements
    {
        public Label labelEntryName { get; set; }
        public Entry entryValue { get; set; }
        public Label labelEditorName { get; set; }
        public Entry entryDescription { get; set; }
        //public Button sendOperations { get; set; }
        public CustomButton sendOperations { get; set; }
        public HelpBottomPanelElements()
        {
            createElements();
        }


        private void createElements()
        {
            labelEntryName = new Label();
            labelEntryName.Text = "Value:";
            labelEntryName.HorizontalTextAlignment = TextAlignment.Center;
            labelEntryName.VerticalTextAlignment = TextAlignment.Center;

            entryValue = new Entry();
            entryValue.Placeholder = "Set operation value";
          /*  entryValue.TextChanged += (sender, e) => {
                if(e.NewTextValue.Length > 0)
                {
                    sendOperations.IsEnabled = true;
                }
                if (e.NewTextValue.Length == 0)
                {
                    sendOperations.IsEnabled = false;
                }
            };
            */
            labelEditorName = new Label();
            labelEditorName.Text = "Description:";
            labelEditorName.HorizontalTextAlignment = TextAlignment.Center;
            labelEditorName.VerticalTextAlignment = TextAlignment.Center;

            entryDescription = new Entry();
            entryDescription.Placeholder = "Set operation description";

            //sendOperations = new Button();
            //sendOperations.Text = "Send";
            sendOperations = new CustomButton(path: "iconSend.png");
           // sendOperations.IsEnabled = false;
            sendOperations.HeightRequest = 30;
            sendOperations.WidthRequest = 60;
        }

    }
}
