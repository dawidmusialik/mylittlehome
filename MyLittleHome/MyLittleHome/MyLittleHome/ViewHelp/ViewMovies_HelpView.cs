﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyLittleHome.ViewHelp
{
	public class ViewMovies_HelpView : ContentPage
	{
        private ListView ListaView;
        ViewMovies_HelpElements Elements = new ViewMovies_HelpElements();

        
        //Główny widok listy
        public StackLayout Listaview()
        {
            StackLayout Stack = new StackLayout();

            ListaView = new ListView();

            ListaView.ItemsSource = App.MyLittleHomeDB_Movies.GetMovies() ;
            ListaView.ItemTemplate = new DataTemplate(() => AllRows());
            ListaView.IsPullToRefreshEnabled = true;
            ListaView.RefreshCommand = LoadTaskCommand;
            ListaView.RowHeight = 265;

            Stack.VerticalOptions = LayoutOptions.FillAndExpand;

            //Elementy wyglądu
            // Stack.Children.Add(TitlePage());
            //Stack.Children.Add(ButtonAddBook());
            Stack.Children.Add(GridTittleAndButton());
            Stack.Children.Add(ListaView);
            Stack.BackgroundColor = Color.FromHex("ABABAB");
            return Stack;
        }

        private ViewCell AllRows()
        {
            ViewCell AllVIew = new ViewCell
            {
                View = new StackLayout
                {
                    Children =
                    {
                        GridView(),
                    }
                }
            };

            return AllVIew;
        }

        private Grid GridView()
        {
            Elements.Elements();
            Grid grid = new Grid
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 40 },
                   new RowDefinition { Height = 140 },
                  // new RowDefinition { Height = 30 },
                  // new RowDefinition { Height = 40 },
                 //  new RowDefinition { Height = 40 },
                  // new RowDefinition { Height = 48 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto },
                  //  new ColumnDefinition { Width = GridLength.Star },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("BDBDBD"),
            };
            


            grid.Children.Add((TitleinGrid()), 0, 5, 0, 1);

            grid.Children.Add((IDinGrid()), 0, 1, 1,2);
            grid.Children.Add(PublicationDateinGrid(), 1, 2, 1, 2);
            grid.Children.Add(FormatinGrid(), 2, 3, 1,2);
            
            grid.Children.Add((QualityinGrid()), 0,1, 2,3);
            grid.Children.Add((LanguageinGrid()), 1,2, 2, 3);
            grid.Children.Add((SubbitlesinGrid()),2,3,2,3);
            
            grid.Children.Add(StorageDeviceinGrid(), 3, 5, 1,3);
            
            grid.Children.Add(DescriptioninGrid(), 0, 5, 3, 4);
            
            return grid;
        }

        private StackLayout IDinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
              Stack.BackgroundColor = Color.Blue;
            Stack.Children.Add(Elements.Get_Elements("ID_Name"));
            Stack.Children.Add(Elements.Get_Elements("ID"));

            return Stack;
        }

        private StackLayout TitleinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Horizontal;
            Stack.HorizontalOptions = LayoutOptions.CenterAndExpand;
            Stack.VerticalOptions = LayoutOptions.CenterAndExpand;
            //   Stack.BackgroundColor = Color.FromHex("6684FF");
            Stack.Children.Add(Elements.Get_Elements("Title_Name"));
            Stack.Children.Add(Elements.Get_Elements("Title"));

            return Stack;
        }

        private StackLayout PublicationDateinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.Sienna;
            Stack.Children.Add(Elements.Get_Elements("PublicationDate_Name"));
            Stack.Children.Add(Elements.Get_Elements("PublicationDate"));

            return Stack;
        }
        private StackLayout FormatinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.Get_Elements("Format_Name"));
            Stack.Children.Add(Elements.Get_Elements("Format"));

            return Stack;
        }

        private StackLayout QualityinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.Get_Elements("Quality_Name"));
            Stack.Children.Add(Elements.Get_Elements("Quality"));

            return Stack;
        }

        private StackLayout LanguageinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            //  Stack.BackgroundColor = Color.RosyBrown;
            Stack.Children.Add(Elements.Get_Elements("Language_Name"));
            Stack.Children.Add(Elements.Get_Elements("Language"));

            return Stack;
        }

        private StackLayout SubbitlesinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
             // Stack.BackgroundColor = Color.Black;
            Stack.Children.Add(Elements.Get_Elements("Subbitles_Name"));
            Stack.Children.Add(Elements.Get_Elements("Subbitles"));

            return Stack;
        }

        private StackLayout StorageDeviceinGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.VerticalOptions = LayoutOptions.Center;
            //  Stack.BackgroundColor = Color.PowderBlue;
            Stack.Children.Add(Elements.Get_Elements("StorageDevice_Name"));
            Stack.Children.Add(Elements.Get_Elements("StorageDevice"));

            return Stack;
        }
        private StackLayout DescriptioninGrid()
        {
            StackLayout Stack = new StackLayout();
            Stack.Orientation = StackOrientation.Vertical;
            Stack.BackgroundColor = Color.FromHex("CCCCCC");
            Stack.Children.Add(Elements.Get_Elements("Description_Name"));
            Stack.Children.Add(Elements.Get_Elements("Description"));

            return Stack;
        }

        //Widok pierwszego grida z tytułem i przyciskiem dodania nowego filmu
        private Grid GridTittleAndButton()
        {
            Grid Gridview = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = 35 },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto },
                },
                Padding = 0,
                RowSpacing = 1,
                ColumnSpacing = 0,

            };
            Gridview.Children.Add(TitlePage(), 0, 4, 0, 1);
            Gridview.Children.Add(ButtonAddBook(), 4, 5, 0, 1);

            return Gridview;
        }

        private Label TitlePage()
        {
            Label Title = new Label();
            Title.Text = " My Movies";
            Title.FontSize = 30;
            Title.TextColor = Color.FromHex("57FF9D");
            //Title.BackgroundColor = Color.Blue;
            Title.Margin = new Thickness(90, 2, 0, 0);
            Title.HorizontalTextAlignment = TextAlignment.Center;
            Title.VerticalOptions = LayoutOptions.CenterAndExpand;
            Title.HorizontalOptions = LayoutOptions.CenterAndExpand;
            return Title;
        }
        private Button ButtonAddBook()
        {
            Button AddBook = new Button();
            AddBook.Text = "Add Movie";
            AddBook.VerticalOptions = LayoutOptions.End;
            AddBook.HorizontalOptions = LayoutOptions.End;
            AddBook.TextColor = Color.Red;
            // AddBook.BackgroundColor = Color.Yellow;

            AddBook.Clicked += (object sender, EventArgs e) =>
            {
                //App.MasterDetailPage.Detail = new NavigationPage(new View.ViewBooks_AddNewBook());
            };
            return AddBook;
        }

        //Odświeżanie widoku
        private async Task ReoladTask()
        {
            try
            {
                ListaView.IsRefreshing = true;
                ListaView.ItemsSource = App.MyLittleHomeDB_Movies.GetMovies();
                ListaView.ItemTemplate = new DataTemplate(() => AllRows());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ListaView.IsRefreshing = false;
            }
        }

        Command loadTaskCommand;
        public Command LoadTaskCommand => loadTaskCommand ?? (loadTaskCommand = new Command(async () => await ReoladTask()));
        //Koniec odświeżania widoku
    }
}
