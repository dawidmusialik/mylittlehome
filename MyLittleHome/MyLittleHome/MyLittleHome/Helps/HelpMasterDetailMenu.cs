﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyLittleHome.Helps
{
    public class HelpMasterDetailMenu
    {
        private Label Name_Label = new Label();
        private string Text_Label;
        private ContentPage nazwa;

        void Init(string Text_label, ContentPage name)
        {
            Text_Label = Text_label;
            nazwa = name;
        }
        //Konstruktor domyślny
        public HelpMasterDetailMenu()
        {
            Init("Blank Page", new View.ViewBlankPage());
        }
        //Konstruktor wpisujący nazwę menu i wybierający widok
        public HelpMasterDetailMenu(string Text_label, ContentPage name)
        {
            Init(Text_label, name);
            Create_item();
        }
        public HelpMasterDetailMenu(string Text_label, ContentPage name, Color color)
        {
            Init(Text_label, name);
            Create_item(color);
        }



        //Stworzenie Label i jego obsługa
        void Create_item()
        {
            TapGestureRecognizer openThis = new TapGestureRecognizer();
            openThis.Tapped += (sender, e) =>
            {
                App.MasterDetailPage.Detail = new NavigationPage(nazwa);
                App.MasterDetailPage.IsPresented = false;
            };


            Name_Label = new Label { Text = Text_Label, TextColor = Color.Gray, };
            Name_Label.GestureRecognizers.Add(openThis);

        }

        //Stworzenie Label i jego obsługa v2
        void Create_item(Color color) //Przeciążona funkcja zmieniająca klor textu
        {
            this.Create_item();
            Name_Label.TextColor = color;
        }

        //Pobranie Label
        public Label Get_Label()
        {
            return Name_Label;
        }
    }
}
