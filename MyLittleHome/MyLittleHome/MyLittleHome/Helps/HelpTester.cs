﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.Helps
{
    public class HelpTester
    {
       

        //Dodanie książek
        private Button addBooks;
        public Button AddBooks()
        {
            addBooks = new Button
            {
                Text = "Dodaj 3 Książki",
            };
            addBooks.Clicked += (object sender, EventArgs e) =>
            {
                App.MyLittleHomeDB_Books.InsertBooks("Książka tytuł 1", "Dawid Musialik", Convert.ToDateTime("2015-10-2"), 1, "Laptop", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut iaculis cursus rutrum. Donec maximus lorem at lectus accumsan pellentesque. Nulla at ultrices dolor, vitae malesuada orci. Nulla porttitor ligula urna, ut egestas lectus porttitor ac. Phasellus blandit, mauris ac ultricies elementum, velit enim feugiat metus, sed vulputate lacus mauris at lectus. Aliquam erat volutpat. Vestibulum eu vestibulum nibh. Nulla facilisi. Praesent risus tortor, sodales sed luctus sed, feugiat in eros. Duis nec felis velit. Fusce leo nisi, tincidunt ut laoreet egestas, mattis interdum magna. Praesent vitae augue felis. Donec lacinia eleifend rutrum. Ut pulvinar nisl odio, ac efficitur lacus finibus quis. Vivamus id mauris et libero elementum condimentum quis ut ligula.",false);
                App.MyLittleHomeDB_Books.InsertBooks("Książka tytuł 2", "Dawid Musialik", Convert.ToDateTime("2015-1-20"), 2, "PC", "Nulla vitae molestie ante. Donec nisl tortor, maximus vel luctus sed, posuere a ante. Nam porttitor, justo nec convallis euismod, nibh purus congue sem, id sagittis nibh tortor non est. Integer hendrerit sapien neque, sit amet malesuada odio posuere sit amet. Mauris interdum eget dui a bibendum. Suspendisse dapibus quis enim id laoreet. Sed elementum aliquet mauris a interdum. Integer sed imperdiet ex. Donec pharetra condimentum facilisis. Pellentesque venenatis arcu sit amet mauris euismod consequat. Aliquam tempus libero vitae tellus porttitor, a bibendum augue tempor. Sed convallis nec ipsum eu tempor. Duis euismod, orci id cursus congue, tortor mi varius leo, sit amet ornare turpis erat ac est. Quisque blandit augue et ante rutrum, at pretium augue tempus. Pellentesque euismod neque massa, eget pretium lacus congue sit amet. Praesent eu urna sit amet sem hendrerit porta.",true);
                App.MyLittleHomeDB_Books.InsertBooks("Książka tytuł 3", "Dawid Musialik", Convert.ToDateTime("2015-5-13"), 3, "Telefon Komórkowy", "Nam laoreet congue lacus et porta. Sed elit diam, fermentum eget sem id, elementum vehicula ex. Proin nec lacus pulvinar, pretium nulla in, pulvinar lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque vitae gravida ante. Vestibulum a fringilla lacus. Ut gravida euismod libero. Curabitur aliquam eros vel condimentum pharetra.",false);
                App.LocalNotification("Trzy książki dodano");
            };

            return addBooks;
        }
        //Dodanie formatu książki
        private Button addBooksFormat;
        public Button AddBooksFormat()
        {
            addBooksFormat = new Button
            {
                Text = "Dodaj 3 formaty książek",
            };
            addBooksFormat.Clicked += (object sender, EventArgs e) =>
            {
                App.MyLittleHomeDB_BooksFormat.InsertBooksFormat("Mobi");
                App.MyLittleHomeDB_BooksFormat.InsertBooksFormat("Epub");
                App.MyLittleHomeDB_BooksFormat.InsertBooksFormat("PDF");
               App.LocalNotification("Trzy formaty książek dodano");
            };

            return addBooksFormat;
        }

        //Usunięcie tabeli książek i dodanie nowej
        private Button deleteBooksTable;
        public Button DeleteBooksTable()
        {
            deleteBooksTable = new Button
            {
                Text = "Usuń tabelę z książkami",
            };
            deleteBooksTable.Clicked += (object sender, EventArgs e) =>
            {
                App.MyLittleHomeDB_Books.DeleteBooks();
                App.LocalNotification("Tabela książki została usunięta");
            };

            return deleteBooksTable;
        }

        //Usunięcie tabeli książek i dodanie nowej
        private Button deleteBooksFormatTable;
        public Button DeleteBooksFormatTable()
        {
            deleteBooksFormatTable = new Button
            {
                Text = "Usuń tabelę z formatami książek",
            };
            deleteBooksFormatTable.Clicked += (object sender, EventArgs e) =>
            {
                App.MyLittleHomeDB_BooksFormat.DeleteBooksFormat();
                App.LocalNotification("Tabela formaty książek została usunięta");
            };

            return deleteBooksFormatTable;
        }


        //Dodanie Filmu
        private Button AddMovies;
        public Button addMovies()
        {
            AddMovies = new Button
            {
                Text = "Dodaj 2 filmy",
            };
            AddMovies.Clicked += (object sender, EventArgs e) =>
            {
                App.MyLittleHomeDB_Movies.InsertMovie("Test 1", Convert.ToDateTime("2015-5-13"), 1, 1, 1, 1, "Laptop","Mój test opisu1111", false);
                App.MyLittleHomeDB_Movies.InsertMovie("Test 2", Convert.ToDateTime("2017-3-3"), 3,2, 2, 1, "Laptop", "Mój test opisu2222", true);
                App.MyLittleHomeDB_Movies.InsertMovie("Test 3", Convert.ToDateTime("2016-9-22"), 2, 3, 3, 2, "PC", "Mój test opisu3333", true);


                App.LocalNotification("Film został dodany");    
            };

            return AddMovies; ;
        }

    }
}
