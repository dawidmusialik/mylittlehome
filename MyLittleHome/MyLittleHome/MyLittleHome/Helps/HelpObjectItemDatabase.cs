﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.Helps
{
    public class ItemBooksDatabase
     {
        public int amountBooks { get; set; }
        public int amountBooksFormat { get; set; }
     }

    public class ItemBudgetDatabase
    {
        public int amountBudget { get; set; }
        public float amountBudgetValue { get; set; }
    }

    public class ItemMoviesDatabase
    {
        public int amountMovies { get; set; }
        public int amountMoviesFormat { get; set; }
        public int amountMoviesLanguage { get; set; }
        public int amountMoviesQuality { get; set; }
        public int amountMoviesSubbitles { get; set; }
    }

    public class ItemMusicDatabase
    {
        public int amountMusic { get; set; }
        public int amountMusicFormat { get; set; }
    }

}
