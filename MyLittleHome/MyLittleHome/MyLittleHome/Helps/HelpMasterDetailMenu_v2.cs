﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace MyLittleHome.Helps
{
    public class HelpMasterDetailMenu_v2
    {
        private Label _labelName;
        private Image _iconImage;
        private Grid _viewGrid;
        private ContentPage _viewContent;
        
        //Konstruktor wpisujący nazwę menu, wybierający widok
        public HelpMasterDetailMenu_v2(string tekst, ContentPage name)//W obecnej wersji nie używane
        {
            Init(tekst, name);
            createItem();
        }
        //Konstruktor wpisujący nazwę menu wraz z grafiką, wybierający widok
        public HelpMasterDetailMenu_v2(string tekst, ContentPage name, Image image)
        {
            Init(tekst, name, image);
            createItemWithImage();
        }

        //Inicjowanie zmiennych
        void Init(string Text_label, ContentPage name) //W obecnej wersji nie używane
        {
            _labelName = new Label();
            _labelName.Text = Text_label;

            _viewContent = new ContentPage();
            _viewContent = name;
        }
        void Init(string tekst, ContentPage name, Image image)
        {
            _labelName = new Label();
            _labelName.Text = tekst;

            _viewContent = new ContentPage();
            _viewContent = name;

            _iconImage = new Image();
            _iconImage = image;
        }
        //Stworzenie elementu menu + obsługa kliknięcia

        void createItem()//W obecnej wersji nie używane
        {
            TapGestureRecognizer openThis = new TapGestureRecognizer();
            openThis.Tapped += (sender, e) =>
            {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(_viewContent);
                App.MasterDetailPage.IsPresented = false;
            };

            _viewGrid = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                },

            };
            _labelName.TextColor = Color.Gray;
            _labelName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            _labelName.VerticalOptions = LayoutOptions.CenterAndExpand;
            _labelName.HorizontalTextAlignment = TextAlignment.Center;
            _labelName.VerticalTextAlignment = TextAlignment.Center;

            _viewGrid.Children.Add(_labelName, 0, 2, 0, 1);

            _viewGrid.GestureRecognizers.Add(openThis);
        }

        //Stworzenie elementu menu wraz z ikoną + obsługa kliknięcia
        void createItemWithImage()
        {
            TapGestureRecognizer openThis = new TapGestureRecognizer();
            openThis.Tapped += (sender, e) =>
            {
                App.MasterDetailPage.Detail = new Component.CustomNavigationPage(_viewContent);
                App.MasterDetailPage.IsPresented = false;
            };

            _viewGrid = new Grid()
            {
                RowDefinitions =
                {
                   new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                },

            };
            _labelName.TextColor = Color.Gray;
            _labelName.HorizontalOptions = LayoutOptions.CenterAndExpand;
            _labelName.VerticalOptions = LayoutOptions.CenterAndExpand;
            _labelName.HorizontalTextAlignment = TextAlignment.Center;
            _labelName.VerticalTextAlignment = TextAlignment.Center;
            
            _iconImage.HeightRequest = 40;
            _iconImage.WidthRequest = 40;

            _viewGrid.Children.Add(_iconImage,0,1,0,1);
            _viewGrid.Children.Add(_labelName,1,2,0,1);
            
            _viewGrid.GestureRecognizers.Add(openThis);
        }
       
        //Pobranie elementu menu
        public Grid getGrid()
        {
            return _viewGrid;
        }
    }
}
