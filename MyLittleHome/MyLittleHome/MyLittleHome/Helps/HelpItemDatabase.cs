﻿using MyLittleHome.DataBase;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.Helps
{
    public class HelpItemDatabase
    {
        private SQLiteConnection db;

        public HelpItemDatabase()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
        }

        public ItemBooksDatabase getAmountOfBooks()
        {
            ItemBooksDatabase _localAmount = new ItemBooksDatabase(); 
           var tableBooks = db.GetAllWithChildren<TableBooks>();
           var tableBooksFormat = db.GetAllWithChildren<TableBooksFormat>();
            
            _localAmount.amountBooks = tableBooks.Count;
            _localAmount.amountBooksFormat = tableBooksFormat.Count;

            return _localAmount;
        }

        public ItemBudgetDatabase getAmountOfBudget()
        {
            ItemBudgetDatabase _localAmount = new ItemBudgetDatabase();
            var tableBudget = db.GetAllWithChildren<TableBudget>();
            
            float _localStatusOfAllBudget = 0;
            foreach(var item in tableBudget)
            {
                _localStatusOfAllBudget += item.budgetStatus;
            }

            _localAmount.amountBudget = tableBudget.Count;
            _localAmount.amountBudgetValue = _localStatusOfAllBudget;

            return _localAmount;
        }

        public ItemMoviesDatabase getAmountOfMovies()
        {
            ItemMoviesDatabase _localAmount = new ItemMoviesDatabase();
            var tableMovies = db.GetAllWithChildren<TableMovies>();
            var tableMoviesFormat = db.GetAllWithChildren<TableMoviesFormat>();
            var tableMoviesLanguage = db.GetAllWithChildren<TableMoviesLanguage>();
            var tableMoviesQuality = db.GetAllWithChildren<TableMoviesQuality>();
            var tableMoviesSubbitles = db.GetAllWithChildren<TableMoviesSubbitles>();

            _localAmount.amountMovies = tableMovies.Count;
            _localAmount.amountMoviesFormat = tableMoviesFormat.Count;
            _localAmount.amountMoviesLanguage = tableMoviesLanguage.Count;
            _localAmount.amountMoviesQuality = tableMoviesQuality.Count;
            _localAmount.amountMoviesSubbitles = tableMoviesSubbitles.Count;

            return _localAmount;
        }

        public ItemMusicDatabase getAmountOfMusic()
        {
            ItemMusicDatabase _localAmount = new ItemMusicDatabase();
            var tableMusic = db.GetAllWithChildren<TableMusic>();
            var tableMusicFormat = db.GetAllWithChildren<TableMusicFormat>();

            _localAmount.amountMusic = tableMusic.Count;
            _localAmount.amountMusicFormat = tableMusicFormat.Count;

            return _localAmount;
        }
    }
}
