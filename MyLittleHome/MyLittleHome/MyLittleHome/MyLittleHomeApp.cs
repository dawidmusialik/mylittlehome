﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

//Folders
using MyLittleHome.View;
using System.Threading.Tasks;

namespace MyLittleHome
{
    
    public partial class App : Application
    {
        //System kontroli wersji SVN

        //Obiekt MasterDatailPage 
        public static MasterDetailPage MasterDetailPage;

        //Obiekt Bazy Danych
        // public static DataBase.HelpDataBase MyLittleHomeDB = new DataBase.HelpDataBase();
        public static DataBase.HelpBooksFormat MyLittleHomeDB_BooksFormat = new DataBase.HelpBooksFormat();
        public static DataBase.HelpBooksLoan MyLittleHomeDB_BooksLoan = new DataBase.HelpBooksLoan();
        public static DataBase.HelpBooks MyLittleHomeDB_Books = new DataBase.HelpBooks(); //Obiekt pomocy do tabeli Books

        public static DataBase.HelpMoviesFormat MyLittleHomeDB_MoviesFormat = new DataBase.HelpMoviesFormat();
        public static DataBase.HelpMoviesLanguage MyLittleHomeDB_MoviesLanguage = new DataBase.HelpMoviesLanguage();
        public static DataBase.HelpMoviesQuality MyLittleHomeDB_MoviesQuality = new DataBase.HelpMoviesQuality();
        public static DataBase.HelpMoviesSubbitles MyLittleHomeDB_MoviesSubbitles = new DataBase.HelpMoviesSubbitles();
        public static DataBase.HelpMoviesLoan MyLittleHomeDB_MoviesLoan = new DataBase.HelpMoviesLoan();
        public static DataBase.HelpMovies MyLittleHomeDB_Movies = new DataBase.HelpMovies();

        public static DataBase.HelpMusicFormat MyLittleHomeDB_MusicFormat = new DataBase.HelpMusicFormat();
        public static DataBase.HelpMusicLoan MyLittleHomeDB_MusicLoan = new DataBase.HelpMusicLoan();
        public static DataBase.HelpMusic MyLittleHomeDB_Music = new DataBase.HelpMusic();//Obiekt pomocy do tabeli Books

        public static DataBase.Budget.HelpBudget MyLittleHomeDB_Budget = new DataBase.Budget.HelpBudget();

        
        public App()
        {
            /* Testy
            var budget1 = MyLittleHomeDB_Budget.getBudget(1);
            var budget1operation = MyLittleHomeDB_Budget.getOperation(1);
            var budget2 = MyLittleHomeDB_Budget.getBudget(2);
            var budget2operation = MyLittleHomeDB_Budget.getOperation(2);
            */

            //Nowy obiekt MasterDetailPage
            MasterDetailPage = new MasterDetailPage()
            {
                //Master - Lista elementów w wyświetlanym menu
                Master = new ViewMasterDetailMenu(),
                //Detail - Strona startowa, NavigationPage - tutaj zmienia się podgląd widoków.
                Detail = new Component.CustomNavigationPage(new ViewHomeScreen()),
               
                };
            // Przesłanie widoku MasterDetailPage do ekranu smartfona.
            MainPage = MasterDetailPage;
        }
        

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        //Lokalne powiadomienie (Toast, wywoływane w Droid'zie / Na razie działa tylko na systemie Android)
        public static void LocalNotification(string message)
        {
            DependencyService.Get<Interface.IDialog>().ShowDialog(message);
        }
    }
}
