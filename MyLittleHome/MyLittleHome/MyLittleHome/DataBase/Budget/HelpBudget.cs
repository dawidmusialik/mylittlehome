﻿using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyLittleHome.DataBase.Budget
{
    public class HelpBudget
    {
        private SQLiteConnection db;

        public HelpBudget()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        private void CreateTable()
        {
            try
            {

                var TableBudgetInfo = db.GetTableInfo("TableBudget");
                var TableBudgetOperationInfo = db.GetTableInfo("TableBudgetOperation");

                if (TableBudgetInfo.Count == 0 && TableBudgetOperationInfo.Count == 0)
                {
                    db.CreateTable<TableBudget>();
                    db.CreateTable<TableBudgetOperation>();
                    createTestBudget();

                }
                
               
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void createTestBudget()
        {
            insertNewBudget("Home", "Expenses house 2017");
            insertNewBudget("Car", "Car maintenance costs");
            insertNewBudget("Account", "My secret savings account");

            insertNewOperation(getBudgetID("Home"), 200, "An old bookcase was sold");
            insertNewOperation(getBudgetID("Home"), -100, "New dishes");
            insertNewOperation(getBudgetID("Home"), 300, "A couch was sold");

            insertNewOperation(getBudgetID("Car"), 600, "Transfer from the savings account");
            insertNewOperation(getBudgetID("Car"), -50, "Tires swap");

            insertNewOperation(getBudgetID("Account"), 2000, "Save");
            insertNewOperation(getBudgetID("Account"), -600, "On car");
            insertNewOperation(getBudgetID("Account"), 50, "From grandparents");
            insertNewOperation(getBudgetID("Account"), -250, "New headphones");


        }

        public void insertNewBudget(string name, string descriptions)
        {
            db.InsertWithChildren(new TableBudget() { budgetDescription = descriptions, budgetName = name });
            var table = db.GetAllWithChildren<TableBudget>();
        }
        public void insertNewBudget(string name, string descriptions, float value)
        {
            db.InsertWithChildren(new TableBudget() { budgetDescription = descriptions, budgetName = name, budgetStatus = value });
            var table = db.GetAllWithChildren<TableBudget>();
        }

        public void editBudget(int budgetid, string name, string description)
        {
            var table = db.GetAllWithChildren<TableBudget>();
            foreach(var item in table)
            {
                if(item.budgetID == budgetid)
                {
                    item.budgetName = name;
                    item.budgetDescription = description;
                    db.UpdateWithChildren(item);
                }
            }

        }

        public int getBudgetID(string nameBudget)
        {
            var table = db.GetAllWithChildren<TableBudget>();
            foreach (var item in table)
            {
                if (item.budgetName == nameBudget)
                {
                    return item.budgetID;
                }
            }
            return 1;

        }

        public void insertNewOperation(int budgetID, float value, string description)
        {
            if (value < 0)
            {
                db.InsertWithChildren(new TableBudgetOperation() { tableBudgetID = budgetID, operationDescription = description, operationExpense = value });
                var table = db.GetAllWithChildren<TableBudget>();
                foreach (var item in table)
                {
                    if (item.budgetID == budgetID)
                    {
                        item.budgetStatus += value;
                        db.UpdateWithChildren(item);
                    }
                }

            }
            if (value >= 0)
            {
                db.InsertWithChildren(new TableBudgetOperation() { tableBudgetID = budgetID, operationDescription = description, operationDeposit = value });
                var table = db.GetAllWithChildren<TableBudget>();
                foreach (var item in table)
                {
                    if (item.budgetID == budgetID)
                    {
                        item.budgetStatus += value;
                        db.UpdateWithChildren(item);
                    }
                }
            }

        }

        public dynamic getBudgetWithOperation(int idBudget)
        {
            /*
            string _queryStringBudgetStatus = "SELECT SUM(TestTableOperation.operationDeposit + TestTableOperation.operationExpense) FROM TestTableOperation WHERE budgetID = ?";
            SQLiteCommand queryBudgetStatus = db.CreateCommand(_queryStringBudgetStatus, idBudget);
            var BudgetStatus = queryBudgetStatus.ExecuteScalar<float>();
            */

            string _queryStringBudgetWithOperations = "SELECT * FROM TableBudget INNER JOIN TestTableOperation ON TableBudget.budgetID = TestTableOperation.budgetID WHERE TableBudget.budgetID = ?";
            SQLiteCommand query = db.CreateCommand(_queryStringBudgetWithOperations, idBudget);
            var BudgetWithOperations = query.ExecuteQuery<DataBase.Budget.ObjectBudgetWithOperation>();

            // BudgetWithOperations.First().budgetStatus = BudgetStatus;

            return BudgetWithOperations;
        }
        public List<TableBudget> getBudgetList()
        {
            string _queryStringBudget = "SELECT * FROM TableBudget";
            SQLiteCommand queryBudget = db.CreateCommand(_queryStringBudget);
            var Budget = queryBudget.ExecuteQuery<TableBudget>();
            
            return Budget;
        }
        public TableBudget getBudget(int idBudget)
        {
            /*
            string _queryStringBudgetStatus = "SELECT SUM(TestTableOperation.operationDeposit + TestTableOperation.operationExpense) FROM TestTableOperation WHERE budgetID = ?";
            SQLiteCommand queryBudgetStatus = db.CreateCommand(_queryStringBudgetStatus, idBudget);
            var BudgetStatus = queryBudgetStatus.ExecuteScalar<float>();
            */

            string _queryStringBudget = "SELECT * FROM TableBudget WHERE budgetID = ?";
            SQLiteCommand queryBudget = db.CreateCommand(_queryStringBudget, idBudget);
            var Budget = queryBudget.ExecuteQuery<TableBudget>();

            //((TestTableBudget)Budget.First()).budgetStatus = BudgetStatus;

            return Budget.Where(x=>x.budgetID == idBudget).FirstOrDefault();
        }
        public List<TableBudgetOperation> getOperation(int idBudget)
        {
            string _queryStringOperation = "SELECT * FROM TableBudgetOperation WHERE tableBudgetID = ?";
            SQLiteCommand queryOperation = db.CreateCommand(_queryStringOperation, idBudget);
            var BudgetStatus = queryOperation.ExecuteQuery<TableBudgetOperation>();



            return BudgetStatus;
        }

        public void editOperation(int operationID, string description, float value)
        {
            var tableOperation = db.GetAllWithChildren<DataBase.Budget.TableBudgetOperation>();
            var tableBudget = db.GetAllWithChildren<DataBase.TableBudget>();

            foreach (var item in tableOperation)
            {
                if(item.operationID == operationID)
                {
                    foreach(var item2 in tableBudget)
                    {
                        if(item2.budgetID == item.tableBudgetID)
                        {
                            item.operationDescription = description;
                            if (value < 0)
                            {
                                if(item.operationDeposit != 0)
                                {
                                    item2.budgetStatus = item2.budgetStatus - item.operationDeposit;
                                }
                                if(item.operationExpense !=0)
                                {
                                    item2.budgetStatus = item2.budgetStatus - item.operationExpense;
                                }
                                item.operationExpense = 0;
                                item.operationDeposit = 0;
                                item.operationExpense = value;
                                item2.budgetStatus = item2.budgetStatus + value;

                            }
                            if (value >= 0)
                            {
                                if (item.operationDeposit != 0)
                                {
                                    item2.budgetStatus = item2.budgetStatus - item.operationDeposit;
                                }
                                if (item.operationExpense != 0)
                                {
                                    item2.budgetStatus = item2.budgetStatus - item.operationExpense;
                                }
                                item.operationDeposit = 0;
                                item.operationExpense = 0;
                                item.operationDeposit = value;
                                item2.budgetStatus = item2.budgetStatus + value;
                            }
                            db.UpdateWithChildren(item);
                            db.UpdateWithChildren(item2);
                        }
                    }

                    

                }
            }

        }

        public void deleteOperation(int operationID)
        {
            var tableOperation = db.GetAllWithChildren<DataBase.Budget.TableBudgetOperation>();
            var tableBudget = db.GetAllWithChildren<DataBase.TableBudget>();

            foreach (var item in tableOperation)
            {
                if(item.operationID == operationID)
                {
                    foreach(var item2 in tableBudget)
                    {
                        if(item2.budgetID == item.tableBudgetID)
                        {
                            if(item.operationDeposit != 0)
                            {
                                item2.budgetStatus = item2.budgetStatus - item.operationDeposit;
                            }
                            if(item.operationExpense != 0)
                            {
                                item2.budgetStatus = item2.budgetStatus - item.operationExpense;
                            }
                            db.UpdateWithChildren(item2);
                            db.Delete(item);
                        }
                    }
                }
            }
        }

        public void deleteBudget(int budgetID)
        {
            var tableBudget = db.GetAllWithChildren<DataBase.TableBudget>();
            foreach(var item in tableBudget)
            {
                if(item.budgetID == budgetID)
                {
                    db.Delete(item);
                }
            }

            var tableOperation = db.GetAllWithChildren<DataBase.Budget.TableBudgetOperation>();
            foreach(var item2 in tableOperation)
            {
                if(item2.tableBudgetID == budgetID)
                {
                    db.Delete(item2);
                }
            }
        }
    }
}
