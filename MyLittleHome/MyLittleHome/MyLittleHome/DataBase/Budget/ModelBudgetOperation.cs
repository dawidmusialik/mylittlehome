﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.DataBase
{
    public class ModelBudgetOperation
    {

        public float operationExpense { get; set; } //Wydatek

        public float operationDeposit { get; set; } //Wpłata

        public string operationDescription { get; set; } //Opis transakcji, np. czy kupujemy chleb czy tankujemy samochód

    }
}
