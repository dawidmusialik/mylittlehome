﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.DataBase.Budget
{
    [Table("TableBudgetOperation")]
    public class TableBudgetOperation
    {
        [PrimaryKey, AutoIncrement]
        public int operationID { get; set; }

        [ForeignKey(typeof(TableBudget))]
        public int tableBudgetID { get; set; }

        public float operationExpense { get; set; } //Wydatek

        public float operationDeposit { get; set; } //Wpłata

        public string operationDescription { get; set; } //Opis transakcji, np. czy kupujemy chleb czy tankujemy samochód
    }
}
