﻿using SQLite.Net;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.DataBase
{
    [Table("TableBudget")]
    public class TableBudget
    {
        [PrimaryKey, AutoIncrement]
        public int budgetID { get; set; } //Id operacji

        public string budgetName { get; set; }
        public string budgetDescription { get; set; }
        public float budgetStatus { get; set; }
            
    }
    
}
