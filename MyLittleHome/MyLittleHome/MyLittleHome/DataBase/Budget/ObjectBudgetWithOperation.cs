﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLittleHome.DataBase.Budget
{
   public class ObjectBudgetWithOperation
    {
        public int budgetTestTableID { get; set; } //Id operacji

        public string budgetName { get; set; }

        public string budgetDescription { get; set; }

        public float budgetStatus { get; set; }

        public int operationTestTableID { get; set; }
        
        public float operationExpense { get; set; } //Wydatek

        public float operationDeposit { get; set; } //Wpłata

        public string operationDescription { get; set; } //Opis transakcji, np. czy kupujemy chleb czy tankujemy samochód

        
    }
}
