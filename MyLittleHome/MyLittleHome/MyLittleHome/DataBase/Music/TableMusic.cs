﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableMusic")]
    public class TableMusic
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id albumu z muzyką

        public string AlbumName { get; set; } //Tytuł albumu

        public string Author { get; set; } //Nazwa autora

        public int NumberOfSongs { get; set; } // Ilość piosenek na płycie

        public DateTime PublicationDate { get; set; } //Data publikacji

        public int IDFormat { get; set; } //Mp3, VAW itd.

        public string StorageDevice { get; set; } // Pamięć-Nośnik na którym znajduje się film

        public string Description { get; set; } //Opis

        public bool Loan { get; set; }

        private string LoanText;
        public string LoanToSting
        {
            get { return LoanText; }
            set { LoanText = Loan ? "Yes" : "No"; }
        }

    }

    public class TableMusicWithConnectedTable : TableMusic
    {
        public string MusicFormat { get; set; }
    }
}
