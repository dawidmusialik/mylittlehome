﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableMusicLoan")]
    public class TableMusicLoan
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id rekordu
        public int IDMusic { get; set; }//Id książki

        public DateTime DateLoan { get; set; } //Data pożyczenia
        public DateTime DateReceipt { get; set; }//Data zwrotu
        public string Who { get; set; } //Komu książka została wypożyczona
    }
}
