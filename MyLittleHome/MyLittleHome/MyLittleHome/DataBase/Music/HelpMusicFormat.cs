﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;
namespace MyLittleHome.DataBase
{
    public class HelpMusicFormat
    {
        private SQLiteConnection db;

        public HelpMusicFormat()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableMusicFormatInfo = db.GetTableInfo("TableMusicFormat");
                if (TableMusicFormatInfo.Count == 0)
                {
                    db.CreateTable<TableMusicFormat>();
                    AddBasicMusicFormat();
                }
                Console.WriteLine("Tabela Books Format została utworzona");
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicMusicFormat()
        {
            this.InsertMusicFormat("MP3");
            this.InsertMusicFormat("AZW");
        }




        //Metoda umieszczająca nową pozycję książki w bazie danych
        public void InsertMusicFormat(string format)
        {
            try
            {
                TableMusicFormat items = new TableMusicFormat();

                items.Format = format;

                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool CheckInsertMusicFormat(string format)
        {
            var table = db.Table<TableMusicFormat>();


            foreach (var item in table)
            {
                if (item.Format == format)
                {
                    return false;
                }
            }
            return true;

        }

        //Metoda pobierająca wszystkie formaty książek z bazy danych
        public Dictionary<string, int> GetMusicFormatDictionary()
        {
            var table = db.Table<TableMusicFormat>();

            Dictionary<string, int> MusicFormat = new Dictionary<string, int>();

            foreach (var item in table)
            {
                MusicFormat.Add(item.Format, item.ID);
            }

            return MusicFormat;

        }

        //Do ItemSource
        internal TableQuery<TableMusicFormat> GetMusicFormatItemSource()
        {
            var table = db.Table<TableMusicFormat>();
            return table;
        }

        //Metoda pobierająca konkretny format po id
        public string GetDirectMusicFormat(int id)
        {
            var table = db.Table<TableMusicFormat>();

            TableMusicFormat items = new TableMusicFormat();

            string format = "";

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    format = item.Format;
                }
            }

            return format;

        }



        public void DeleteOneMusicFormat(int id)
        {
            var table = db.Table<TableMusicFormat>();

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    App.LocalNotification("Format deleted");

                }
            }
        }



        //Metoda usuwająca tablicę Books i tworząca ją na nowo (wyczyszczenie tablicy)
        public void DeleteBooksFormat()
        {
            db.DropTable<TableBooksFormat>();
            CreateTable();
        }

    }
}
