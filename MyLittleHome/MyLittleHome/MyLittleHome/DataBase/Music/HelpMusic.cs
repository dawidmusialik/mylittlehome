﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;

namespace MyLittleHome.DataBase
{
    public class HelpMusic
    {
        private SQLiteConnection db;

        public HelpMusic()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {

                var TableMusicInfo = db.GetTableInfo("TableMusic");
                if (TableMusicInfo.Count == 0)
                {
                    db.CreateTable<TableMusic>();
                    AddBasicMusic();
                }
                Console.WriteLine("Tabela Music została utworzona");

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicMusic()
        {
           this.InsertMusic("Music album 1", "Author 1", 21 , Convert.ToDateTime("2016-3-12"),  "Laptop", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut iaculis cursus rutrum. Donec maximus lorem at lectus accumsan pellentesque. Nulla at ultrices dolor, vitae malesuada orci. Nulla porttitor ligula urna, ut egestas lectus porttitor ac. Phasellus blandit, mauris ac ultricies elementum, velit enim feugiat metus, sed vulputate lacus mauris at lectus. Aliquam erat volutpat. Vestibulum eu vestibulum nibh. Nulla facilisi. Praesent risus tortor, sodales sed luctus sed, feugiat in eros. Duis nec felis velit. Fusce leo nisi, tincidunt ut laoreet egestas, mattis interdum magna. Praesent vitae augue felis. Donec lacinia eleifend rutrum. Ut pulvinar nisl odio, ac efficitur lacus finibus quis. Vivamus id mauris et libero elementum condimentum quis ut ligula.",1, false);
            this.InsertMusic("Music album 2", "Author 1", 23, Convert.ToDateTime("2014-11-05"), "Laptop", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut iaculis cursus rutrum. Donec maximus lorem at lectus accumsan pellentesque. Nulla at ultrices dolor, vitae malesuada orci. Nulla porttitor ligula urna, ut egestas lectus porttitor ac. Phasellus blandit, mauris ac ultricies elementum, velit enim feugiat metus, sed vulputate lacus mauris at lectus. Aliquam erat volutpat. Vestibulum eu vestibulum nibh. Nulla facilisi. Praesent risus tortor, sodales sed luctus sed, feugiat in eros. Duis nec felis velit. Fusce leo nisi, tincidunt ut laoreet egestas, mattis interdum magna. Praesent vitae augue felis. Donec lacinia eleifend rutrum. Ut pulvinar nisl odio, ac efficitur lacus finibus quis. Vivamus id mauris et libero elementum condimentum quis ut ligula.", 1, false);
           
        }


        //Metoda umieszczająca nową pozycję książki w bazie danych
        public void InsertMusic(string albumname,string author, int numberofsongs, DateTime publicationdate, string storagedevice, string desctiption, int format, bool loan)
        {
            try
            {
                TableMusic items = new TableMusic();

                items.AlbumName = albumname;
                items.Author = author;
                items.Description = desctiption;
                items.IDFormat = format;
                items.NumberOfSongs = numberofsongs;
                items.PublicationDate = publicationdate;
                items.StorageDevice = storagedevice;
                items.Loan = loan;

                db.Insert(items);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //Metoda umieszczająca nową pozycję książki w bazie danych
        public void UpgradeMusic(int id, string albumname, string author, int numberofsongs, DateTime publicationdate, string storagedevice, string desctiption, int format, bool loan)
        {
            try
            {
                var table = db.Table<TableMusic>();
                TableMusic items = new TableMusic();
                foreach (var item in table)
                {
                    if (item.ID == id)
                    {
                        items.ID = id;
                        items.AlbumName = albumname;
                        items.Author = author;
                        items.Description = desctiption;
                        items.IDFormat = format;
                        items.NumberOfSongs = numberofsongs;
                        items.PublicationDate = publicationdate;
                        items.StorageDevice = storagedevice;
                        items.Loan = loan;
                        db.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
 
        public List<TableMusicWithConnectedTable> GetMusic()
        {
            SQLiteCommand query = db.CreateCommand("SELECT c.*, qt.Format AS MusicFormat FROM TableMusic c INNER JOIN TableMusicFormat qt ON c.IDFormat = qt._Id ");


            var table = query.ExecuteQuery<TableMusicWithConnectedTable>();

            return table;

        }
        public List<TableMusicWithConnectedTable> GetMusicByID(int id)
        {
            SQLiteCommand query = db.CreateCommand("SELECT c.*, qt.Format AS MusicFormat FROM TableMusic c INNER JOIN TableMusicFormat qt ON c.IDFormat = qt._Id WHERE c._Id = ? ", id);

            var table = query.ExecuteQuery<TableMusicWithConnectedTable>();

            return table;

        }



        public bool CheckIDFormatMusic_In_TableMusic(int id)
        {
            var table = db.Table<TableMusic>();

            foreach (var item in table)
            {
                if (item.IDFormat == id)
                {
                    App.LocalNotification("Nie można usunąć formatu, jest on obecnie w użyciu");
                    return true;
                }

            }
            return false;
        }


        public void DeleteOneMusic(int id)
        {
            var table = db.Table<TableMusic>();
            var table2 = db.Table<TableMusicLoan>();
            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    if (App.MyLittleHomeDB_MusicLoan.CheckIDMusicinMusicLoan(item.ID) == true)
                    {
                        App.MyLittleHomeDB_MusicLoan.DeleteOneMusicLoan(item.ID);
                    }
                }
            }
        }


        //Metoda usuwająca tablicę Books i tworząca ją na nowo (wyczyszczenie tablicy)
        public void DeleteMusic()
        {
            db.DropTable<TableMusic>();
            CreateTable();
        }

    }
}
