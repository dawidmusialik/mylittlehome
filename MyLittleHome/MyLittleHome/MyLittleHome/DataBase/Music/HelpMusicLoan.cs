﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;

namespace MyLittleHome.DataBase
{
    public class HelpMusicLoan
    {
        private SQLiteConnection db;

        public HelpMusicLoan()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableMusicLoanInfo = db.GetTableInfo("TableMusicLoan");
                if (TableMusicLoanInfo.Count == 0)
                {
                    db.CreateTable<TableMusicLoan>();
                }
                Console.WriteLine("Tabela Music Loan została utworzona");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        public void CreateMusicLoan(int id)
        {
            try
            {
                TableMusicLoan items = new TableMusicLoan();
                items.IDMusic = id;
                items.DateLoan = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                items.DateReceipt = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void UpdateMusicLoan(int id, DateTime dateloan, DateTime datereceipt, string who)
        {
            try
            {
                TableMusicLoan items = new TableMusicLoan();
                var table = db.Table<TableMusicLoan>();

                foreach (var item in table)
                {
                    if (item.IDMusic == id)
                    {
                        item.DateLoan = dateloan;
                        item.DateReceipt = datereceipt;
                        item.Who = who;
                        db.Update(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Clear(int id)
        {
            try
            {
                TableMusicLoan items = new TableMusicLoan();
                var table = db.Table<TableMusicLoan>();

                foreach (var item in table)
                {
                    if (item.ID == id)
                    {
                        item.DateLoan = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                        item.DateReceipt = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                        item.Who = "";
                        db.Update(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        //Metoda pobierająca konkretny format po id
        public List<TableMusicLoan> GetDirectMusicLoan(int id)
        {
            SQLiteCommand query = db.CreateCommand("SELECT * FROM TableMusicLoan WHERE IDMusic = ? ", id);

            var table = query.ExecuteQuery<TableMusicLoan>();

            return table;

        }
        public bool CheckIDMusicinMusicLoan(int id)
        {
            var table = db.Table<TableMusicLoan>();

            foreach (var item in table)
            {
                if (item.IDMusic == id)
                {
                    return true;
                }

            }
            return false;
        }

        public void DeleteOneMusicLoan(int id)
        {
            var table = db.Table<TableMusicLoan>();

            foreach (var item in table)
            {
                if (item.IDMusic == id)
                {
                    db.Delete(item);
                }
            }
        }

        public void DeleteMusicLoan()
        {
            db.DropTable<TableMusicLoan>();
            CreateTable();
        }

    }
}
