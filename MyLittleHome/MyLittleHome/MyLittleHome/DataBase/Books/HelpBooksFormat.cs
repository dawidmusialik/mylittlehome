﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;
namespace MyLittleHome.DataBase
{
    public class HelpBooksFormat
    {
        private SQLiteConnection db;

        public HelpBooksFormat()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableBooksFormatInfo = db.GetTableInfo("TableBooksFormat");
                if (TableBooksFormatInfo.Count == 0) { db.CreateTable<TableBooksFormat>(); AddBasicBooksFormat(); }
                Console.WriteLine("Tabela Books Format została utworzona");
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicBooksFormat()
        {
            this.InsertBooksFormat("Mobi");
            this.InsertBooksFormat("Epub");
            this.InsertBooksFormat("PDF");
        }




        //Metoda umieszczająca nową pozycję książki w bazie danych
        public void InsertBooksFormat(string format)
        {
            try
            {
                TableBooksFormat items = new TableBooksFormat();

                items.Format = format;

                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool CheckInsertBooksFormat(string format)
        {
            var table = db.Table<TableBooksFormat>();


            foreach (var item in table)
            {
                if (item.Format == format)
                {
                    return false;
                }
            }
            return true;

        }

        //Metoda pobierająca wszystkie formaty książek z bazy danych
        public Dictionary<string, int> GetBooksFormatDictionary()
        {
            var table = db.Table<TableBooksFormat>();

            Dictionary<string, int> BooksFormat = new Dictionary<string, int>();

            foreach (var item in table)
            {
                BooksFormat.Add(item.Format, item.ID);
            }

            return BooksFormat;

        }

        //Do ItemSource
        internal TableQuery<TableBooksFormat> GetBooksFormatItemSource()
        {
            var table = db.Table<TableBooksFormat>();
            return table;
        }

        //Metoda pobierająca konkretny format po id
        public string GetDirectBooksFormat(int id)
        {
            var table = db.Table<TableBooksFormat>();

            TableBooksFormat items = new TableBooksFormat();

            string format = "";

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    format = item.Format;
                }
            }

            return format;

        }



        public void DeleteOneBooksFormat(int id)
        {
            var table = db.Table<TableBooksFormat>();

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    App.LocalNotification("Format deleted");

                }
            }
        }



        //Metoda usuwająca tablicę Books i tworząca ją na nowo (wyczyszczenie tablicy)
        public void DeleteBooksFormat()
        {
            db.DropTable<TableBooksFormat>();
            CreateTable();
        }

    }
}
