﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableBooksLoan")]
    public class TableBooksLoan
    {
        [PrimaryKey,AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id rekordu
        public int IDBooks { get; set; }//Id książki

        public DateTime DateLoan { get; set; } //Data pożyczenia
        public DateTime DateReceipt { get; set; }//Data zwrotu
        public string Who { get; set; } //Komu książka została wypożyczona
    }
}
