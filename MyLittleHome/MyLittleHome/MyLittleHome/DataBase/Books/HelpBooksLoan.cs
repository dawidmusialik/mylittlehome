﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;

namespace MyLittleHome.DataBase
{
    public class HelpBooksLoan
    {
        private SQLiteConnection db;

        public HelpBooksLoan()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableBooksLoanInfo = db.GetTableInfo("TableBooksLoan");
                if (TableBooksLoanInfo.Count == 0)
                {
                    db.CreateTable<TableBooksLoan>();
                }
                Console.WriteLine("Tabela Books Format została utworzona");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        public void CreateBooksLoan(int id)
        {
            try
            {
                TableBooksLoan items = new TableBooksLoan();
                items.IDBooks = id;
                items.DateLoan = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                items.DateReceipt = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void UpdateBooksLoan(int id, DateTime dateloan, DateTime datereceipt, string who)
        {
            try
            {
                TableBooksLoan items = new TableBooksLoan();
                var table = db.Table<TableBooksLoan>();

                foreach (var item in table)
                {
                    if (item.IDBooks == id)
                    {
                        item.DateLoan = dateloan;
                        item.DateReceipt = datereceipt;
                        item.Who = who;
                        db.Update(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Clear(int id)
        {
            try
            {
                TableBooksLoan items = new TableBooksLoan();
                var table = db.Table<TableBooksLoan>();

                foreach (var item in table)
                {
                    if (item.ID == id)
                    {
                        item.DateLoan = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                        item.DateReceipt = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                        item.Who = "";
                        db.Update(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        //Metoda pobierająca konkretny format po id
        public List<TableBooksLoan> GetDirectBooksLoan(int id)
        {
            SQLiteCommand query = db.CreateCommand("SELECT * FROM TableBooksLoan WHERE IDBooks = ? ", id);

            var table = query.ExecuteQuery<TableBooksLoan>();

            return table;

        }
        public bool CheckIDBooksinBooksLoan(int id)
        {
            var table = db.Table<TableBooksLoan>();

            foreach (var item in table)
            {
                if (item.IDBooks == id)
                {
                    return true;
                }

            }
            return false;
        }

        public void DeleteOneBooksLoan(int id)
        {
            var table = db.Table<TableBooksLoan>();

            foreach (var item in table)
            {
                if (item.IDBooks == id)
                {
                    db.Delete(item);
                }
            }
        }

        public void DeleteBooksLoan()
        {
            db.DropTable<TableBooksLoan>();
            CreateTable();
        }

    }
}
