﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;

namespace MyLittleHome.DataBase
{
    public class HelpBooks
    {
        private SQLiteConnection db;

        public HelpBooks()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {

                var TableBooksInfo = db.GetTableInfo("TableBooks");
                if (TableBooksInfo.Count == 0)
                {
                    db.CreateTable<TableBooks>();
                    AddBasicBooks();
                }
                Console.WriteLine("Tabela Books została utworzona");

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicBooks()
        {
            this.InsertBooks("Title 1", "Author 1", Convert.ToDateTime("2016-3-12"), 1, "Laptop", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut iaculis cursus rutrum. Donec maximus lorem at lectus accumsan pellentesque. Nulla at ultrices dolor, vitae malesuada orci. Nulla porttitor ligula urna, ut egestas lectus porttitor ac. Phasellus blandit, mauris ac ultricies elementum, velit enim feugiat metus, sed vulputate lacus mauris at lectus. Aliquam erat volutpat. Vestibulum eu vestibulum nibh. Nulla facilisi. Praesent risus tortor, sodales sed luctus sed, feugiat in eros. Duis nec felis velit. Fusce leo nisi, tincidunt ut laoreet egestas, mattis interdum magna. Praesent vitae augue felis. Donec lacinia eleifend rutrum. Ut pulvinar nisl odio, ac efficitur lacus finibus quis. Vivamus id mauris et libero elementum condimentum quis ut ligula.", false);
            this.InsertBooks("Title 2", "Author 2", Convert.ToDateTime("2015-1-20"), 2, "PC", "Nulla vitae molestie ante. Donec nisl tortor, maximus vel luctus sed, posuere a ante. Nam porttitor, justo nec convallis euismod, nibh purus congue sem, id sagittis nibh tortor non est. Integer hendrerit sapien neque, sit amet malesuada odio posuere sit amet. Mauris interdum eget dui a bibendum. Suspendisse dapibus quis enim id laoreet. Sed elementum aliquet mauris a interdum. Integer sed imperdiet ex. Donec pharetra condimentum facilisis. Pellentesque venenatis arcu sit amet mauris euismod consequat. Aliquam tempus libero vitae tellus porttitor, a bibendum augue tempor. Sed convallis nec ipsum eu tempor. Duis euismod, orci id cursus congue, tortor mi varius leo, sit amet ornare turpis erat ac est. Quisque blandit augue et ante rutrum, at pretium augue tempus. Pellentesque euismod neque massa, eget pretium lacus congue sit amet. Praesent eu urna sit amet sem hendrerit porta.", true);
            App.MyLittleHomeDB_BooksLoan.CreateBooksLoan(2);
            this.InsertBooks("Title 3", "Author 3", Convert.ToDateTime("2015-5-13"), 3, "Mobile phone", "Nam laoreet congue lacus et porta. Sed elit diam, fermentum eget sem id, elementum vehicula ex. Proin nec lacus pulvinar, pretium nulla in, pulvinar lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque vitae gravida ante. Vestibulum a fringilla lacus. Ut gravida euismod libero. Curabitur aliquam eros vel condimentum pharetra.", false);



        }


        //Metoda umieszczająca nową pozycję książki w bazie danych
        public void InsertBooks(string title, string author, DateTime publicationdate, int idtableformat, string storagedevice, string description, bool loan)
        {
            try
            {
                TableBooks items = new TableBooks();

                items.Title = title;
                items.Author = author;
                items.PublicationDate = publicationdate.Date;
                items.IDTableBooksFormat = idtableformat;
                items.StorageDevice = storagedevice;
                items.Description = description;
                items.Loan = loan;

                db.Insert(items);

               // App.MyLittleHomeDB_BooksLoan.CreateBooksLoan(items.ID);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //Metoda umieszczająca nową pozycję książki w bazie danych
        public void UpgradeBooks(int id, string title, string author, DateTime publicationdate, int idtableformat, string storagedevice, string description, bool loan)
        {
            try
            {
                var table = db.Table<TableBooks>();
                TableBooks items = new TableBooks();
                foreach (var item in table)
                {
                    if (item.ID == id)
                    {
                        items.ID = id;
                        items.Title = title;
                        items.Author = author;
                        items.PublicationDate = publicationdate;
                        items.IDTableBooksFormat = idtableformat;
                        items.StorageDevice = storagedevice;
                        items.Description = description;
                        items.Loan = loan;
                        db.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /*
                //Metoda pobierająca wszystkie książki z bazy danych
                  public TableQuery<TableBooks> GetBooks()
                  {
                      var table = db.Table<TableBooks>();
                      return table;
                  }
                */
        public List<TableBooksWithTableBooksFormat> GetBooks()
        {
            SQLiteCommand query = db.CreateCommand("SELECT c.*, qt.Format AS BooksFormat FROM TableBooks c INNER JOIN TableBooksFormat qt ON c.IDTableBooksFormat = qt._Id ");


            var table = query.ExecuteQuery<TableBooksWithTableBooksFormat>();

            return table;

        }
        public List<TableBooksWithTableBooksFormat> GetBooksByID(int id)
        {
            SQLiteCommand query = db.CreateCommand("SELECT c.*, qt.Format AS BooksFormat FROM TableBooks c INNER JOIN TableBooksFormat qt ON c.IDTableBooksFormat = qt._Id WHERE c._Id = ? ", id);

            var table = query.ExecuteQuery<TableBooksWithTableBooksFormat>();

            return table;

        }



        public bool CheckIDFormatBooks_In_TableBooks(int id)
        {
            var table = db.Table<TableBooks>();

            foreach (var item in table)
            {
                if (item.IDTableBooksFormat == id)
                {
                    App.LocalNotification("Nie można usunąć formatu, jest on obecnie w użyciu");
                    return true;
                }

            }
            return false;
        }


        public void DeleteOneBooks(int id)
        {
            var table = db.Table<TableBooks>();
            var table2 = db.Table<TableBooksLoan>();
            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    if(App.MyLittleHomeDB_BooksLoan.CheckIDBooksinBooksLoan(item.ID)==true)
                    {
                        App.MyLittleHomeDB_BooksLoan.DeleteOneBooksLoan(item.ID);
                    }
                }
            }
        }


        //Metoda usuwająca tablicę Books i tworząca ją na nowo (wyczyszczenie tablicy)
        public void DeleteBooks()
        {
            db.DropTable<TableBooks>();
            CreateTable();
        }

    }
}
