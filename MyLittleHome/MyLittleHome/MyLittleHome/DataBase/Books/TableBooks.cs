﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableBooks")]
    public class TableBooks
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id Książki

        public string Title { get; set; } //Tytuł ksiązki

        public string Author { get; set; } //Nazwa autora

        public DateTime PublicationDate { get; set; } //Data publikacji

        public int IDTableBooksFormat { get; set; } //Papier, ebook, audiobook

        public string StorageDevice { get; set; } // Pamięć-Nośnik na którym znajduje się książka

        public string Description { get; set; } //Opis

        public bool Loan { get; set; } //Czy ksiażka została wypożyczona 

        private string LoanText;
        public string LoanToSting
        {
            get { return LoanText; }
            set { LoanText = Loan ? "Yes" : "No"; }
        }

    }


    public class TableBooksWithTableBooksFormat : TableBooks
    {
        public string BooksFormat { get; set; }
    }
}
