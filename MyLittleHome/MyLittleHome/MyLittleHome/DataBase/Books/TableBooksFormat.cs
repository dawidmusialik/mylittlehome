﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableBooksFormat")]
    class TableBooksFormat
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id Książki

        public string Format { get; set; } //Tytuł ksiązki

    }
}
