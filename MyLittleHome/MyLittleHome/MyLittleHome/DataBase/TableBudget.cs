﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MyLittleHome.DataBase
{
    [Table("TableBudget")]
    public class TableBudget
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id operacji

        public float BudgetStatus { get; set; } //Stan budżetu

        public float Expense { get; set; } //Wydatek

        public float Deposit { get; set; } //Wpłata

        public string OperationDescription { get; set; } //Opis transakcji, np. czy kupujemy chleb czy tankujemy samochód
    }
}
