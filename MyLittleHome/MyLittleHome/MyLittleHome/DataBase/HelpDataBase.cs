﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.IO;
using Xamarin.Forms;
using System.Linq;

namespace MyLittleHome.DataBase
{
    public class HelpDataBase
    {
        private string dbPath;
        private SQLiteConnection db;

        public HelpDataBase()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnection();

            // CreateAllTable();
           DeleteAllTable();
        }



        //Tworzenie tabeli Books, Budget,Movies,Music
        public void CreateAllTable()
        {
            try
            {
                var TableBooksFormatInfo = db.GetTableInfo("TableBooksFormat");
                if (TableBooksFormatInfo.Count == 0) 
                    {
                    db.CreateTable<TableBooksFormat>();
                    App.MyLittleHomeDB_BooksFormat.AddBasicBooksFormat();
                }
                Console.WriteLine("Tabela Books Format została utworzona");

                var TableBooksLoanInfo = db.GetTableInfo("TableBooksLoan");
                if (TableBooksLoanInfo.Count == 0) {
                    db.CreateTable<TableBooksLoan>();
                }
                Console.WriteLine("Tabela Books Loan została utworzona");

                var TableBooksInfo = db.GetTableInfo("TableBooks");
                if (TableBooksInfo.Count == 0) {
                    db.CreateTable<TableBooks>();
                    App.MyLittleHomeDB_Books.AddBasicBooks();
                }
                Console.WriteLine("Tabela Books została utworzona");



                var TableBudgetInfo = db.GetTableInfo("TableBudget");
                if (TableBudgetInfo.Count == 0) { db.CreateTable<TableBudget>(); }
                Console.WriteLine("Tabela Budget została utworzona");
                
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        //Usunięcie wszystkich tabel i utworzenie ich na nowo (czyszczenie bazy danych)
        public void DeleteAllTable()
        {
            //string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "MyLittleHomeDB.db3");
            //  var db = new SQLiteConnection(dbPath);

            db.DropTable<TableBooks>();
            db.DropTable<TableBooksFormat>();
            db.DropTable<TableBooksLoan>();
            Console.WriteLine("Tabela Books została usunięta");

            db.DropTable<TableBudget>();
            Console.WriteLine("Tabela Budget została usunięta");

            db.DropTable<TableMovies>();
            Console.WriteLine("Tabela Movies została usunięte");

            db.DropTable<TableMusic>();
            Console.WriteLine("Tabela Music została usunięta");

          // CreateAllTable();
            Console.WriteLine("Tabele zostały utworzona");

        }



       

    }

}