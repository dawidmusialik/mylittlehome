﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;

namespace MyLittleHome.DataBase
{
    public class HelpMoviesLoan
    {
        private SQLiteConnection db;

        public HelpMoviesLoan()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableBooksLoanInfo = db.GetTableInfo("TableMoviesLoan");
                if (TableBooksLoanInfo.Count == 0)
                {
                    db.CreateTable<TableMoviesLoan>();
                }
                Console.WriteLine("Tabela movies loan została utworzona");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        public void CreateMoviesLoan(int id)
        {
            try
            {
                TableMoviesLoan items = new TableMoviesLoan();
                items.IDMovies = id;
                items.DateLoan = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                items.DateReceipt = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void UpdateMoviesLoan(int id, DateTime dateloan, DateTime datereceipt, string who)
        {
            try
            {
                TableMoviesLoan items = new TableMoviesLoan();
                var table = db.Table<TableMoviesLoan>();

                foreach (var item in table)
                {
                    if (item.IDMovies == id)
                    {
                        item.DateLoan = dateloan;
                        item.DateReceipt = datereceipt;
                        item.Who = who;
                        db.Update(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Clear(int id)
        {
            try
            {
                var table = db.Table<TableMoviesLoan>();

                foreach (var item in table)
                {
                    if (item.ID == id)
                    {
                        item.DateLoan = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                        item.DateReceipt = new DateTime(year: DateTime.Now.Year, month: 1, day: 1);
                        item.Who = "";
                        db.Update(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        //Metoda pobierająca konkretny format po id
        public List<TableMoviesLoan> GetDirectMoviesLoan(int id)
        {
            var tabletest = db.Table<TableMoviesLoan>();
            foreach(var item in tabletest)
            {
                var test = item;
            }
            SQLiteCommand query = db.CreateCommand("SELECT * FROM TableMoviesLoan WHERE IDMovies = ?", id);

            var table = query.ExecuteQuery<TableMoviesLoan>();

            return table;

        }
        public bool CheckIDMoviesinMoviesLoan(int id)
        {
            var table = db.Table<TableMoviesLoan>();

            foreach (var item in table)
            {
                if (item.IDMovies == id)
                {
                    return true;
                }

            }
            return false;
        }
        public void DeleteOneMoviesLoan(int id)
        {
            var table = db.Table<TableMoviesLoan>();

            foreach (var item in table)
            {
                if (item.IDMovies == id)
                {
                    db.Delete(item);
                }
            }
        }

        public void DeleteMoviesLoan()
        {
            db.DropTable<TableMoviesLoan>();
            CreateTable();
        }

    }
}
