﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using Xamarin.Forms;

namespace MyLittleHome.DataBase
{
    public class HelpMovies
    {
        private SQLiteConnection db;

        public HelpMovies()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {

                var TableMoviestInfo = db.GetTableInfo("TableMovies");
                if (TableMoviestInfo.Count == 0)
                {
                    db.CreateTable<TableMovies>();
                    AddBasicMovies();
                }
                Console.WriteLine("Tabela Movies została utworzona");

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void AddBasicMovies()
        {
            this.InsertMovie("Movies 1", Convert.ToDateTime("2015-5-13"), 1, 1, 1, 1, "Laptop", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut iaculis cursus rutrum. Donec maximus lorem at lectus accumsan pellentesque. Nulla at ultrices dolor, vitae malesuada orci. Nulla porttitor ligula urna, ut egestas lectus porttitor ac. Phasellus blandit, mauris ac ultricies elementum, velit enim feugiat metus, sed vulputate lacus mauris at lectus. Aliquam erat volutpat. Vestibulum eu vestibulum nibh. Nulla facilisi. Praesent risus tortor, sodales sed luctus sed, feugiat in eros. Duis nec felis velit. Fusce leo nisi, tincidunt ut laoreet egestas, mattis interdum magna. Praesent vitae augue felis. Donec lacinia eleifend rutrum. Ut pulvinar nisl odio, ac efficitur lacus finibus quis. Vivamus id mauris et libero elementum condimentum quis ut ligula.", false);

            this.InsertMovie("Movies 2", Convert.ToDateTime("2017-3-3"), 3, 2, 2, 1, "Płyta", "Nam laoreet congue lacus et porta. Sed elit diam, fermentum eget sem id, elementum vehicula ex. Proin nec lacus pulvinar, pretium nulla in, pulvinar lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque vitae gravida ante. Vestibulum a fringilla lacus. Ut gravida euismod libero. Curabitur aliquam eros vel condimentum pharetra.", true);
            App.MyLittleHomeDB_MoviesLoan.CreateMoviesLoan(2);
            this.InsertMovie("Movies 3", Convert.ToDateTime("2016-9-22"), 2, 3, 3, 2, "PC", "Nulla vitae molestie ante. Donec nisl tortor, maximus vel luctus sed, posuere a ante. Nam porttitor, justo nec convallis euismod, nibh purus congue sem, id sagittis nibh tortor non est. Integer hendrerit sapien neque, sit amet malesuada odio posuere sit amet. Mauris interdum eget dui a bibendum. Suspendisse dapibus quis enim id laoreet. Sed elementum aliquet mauris a interdum. Integer sed imperdiet ex. Donec pharetra condimentum facilisis. Pellentesque venenatis arcu sit amet mauris euismod consequat. Aliquam tempus libero vitae tellus porttitor, a bibendum augue tempor. Sed convallis nec ipsum eu tempor. Duis euismod, orci id cursus congue, tortor mi varius leo, sit amet ornare turpis erat ac est. Quisque blandit augue et ante rutrum, at pretium augue tempus. Pellentesque euismod neque massa, eget pretium lacus congue sit amet. Praesent eu urna sit amet sem hendrerit porta", false);

            //string description = "Nam laoreet congue lacus et porta. Sed elit diam, fermentum eget sem id, elementum vehicula ex. Proin nec lacus pulvinar, pretium nulla in, pulvinar lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque vitae gravida ante. Vestibulum a fringilla lacus. Ut gravida euismod libero. Curabitur aliquam eros vel condimentum pharetra.";
            //System.Random format;
            //System.Random quality;
            //System.Random language;
            //System.Random subbitles;

            //for (int i = 1; i<26; i++)
            //{
            //    format = new Random(0);
            //    quality = new Random(0);
            //    language = new Random(0);
            //    subbitles = new Random(0);

            //    InsertMovie("Movie " + i.ToString()+1, Convert.ToDateTime("2015-5-13"), format.Next(0, 5), quality.Next(0, 3), language.Next(0, 2), subbitles.Next(0, 3), "Device " + i.ToString(), description, false);
            //    Device.StartTimer(new TimeSpan(0, 0, 0, 1), () => { return true; });
            //}

        }


        //Metoda umieszczająca nową pozycję filmu w bazie danych
        public void InsertMovie(string title, DateTime publicationdate, int idformat, int idquality, int idlanguage, int idsubbitles, string storagedevice, string description, bool loan)
        {
            try
            {
                TableMovies items = new TableMovies();

                items.Title = title;
                items.PublicationDate = publicationdate;
                items.IDFormat = idformat;
                items.IDQuality = idquality;
                items.IDLanguage = idquality;
                items.IDSubbitles = idsubbitles;
                items.StorageDevice = storagedevice;
                items.Description = description;
                items.Loan = loan;

                db.Insert(items);
               // App.MyLittleHomeDB_MoviesLoan.CreateMoviesLoan(items.ID);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void UpgradeMovies(int id, string title, DateTime publicationdate, int idformat, int idquality, int idlanguage, int idsubbitles, string storagedevice, string description, bool loan)
        {
            try
            {
                var table = db.Table<TableMovies>();
                TableMovies items = new TableMovies();
                foreach (var item in table)
                {
                    if (item.ID == id)
                    {
                        items.ID = id;
                        items.Title = title;
                        items.PublicationDate = publicationdate;
                        items.IDFormat = idformat;
                        items.IDQuality = idquality;
                        items.IDLanguage = idquality;
                        items.IDSubbitles = idsubbitles;
                        items.StorageDevice = storagedevice;
                        items.Description = description;
                        items.Loan = loan;
                        db.Update(items);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //Metoda pobierająca wszystkie filmy z bazy danych
        /* public TableQuery<TableMovies> GetMovies()
         {
             var table = db.Table<TableMovies>();
             return table;
         }*/
        public List<TableMoviesWithConettedTable> GetMovies()
        {
            /*
            SQLiteCommand QueryFormat = db.CreateCommand("SELECT c.*, ctF.Format AS FormatText FROM TableMovies c INNER JOIN TableMoviesFormat ctF ON c.IDFormat = ctF._Id ");
            SQLiteCommand QueryLanguage = db.CreateCommand("SELECT c.*, ctL.Language AS LanguageText FROM TableMovies c INNER JOIN TableMoviesLanguage ctL ON c.IDLanguage = ctL._Id ");
            SQLiteCommand QueryQuality = db.CreateCommand("SELECT c.*, ctQ.Quality AS QualityText FROM TableMovies c INNER JOIN TableMoviesQuality ctQ ON c.IDQuality = ctQ._Id ");
            SQLiteCommand QuerySubbitles = db.CreateCommand("SELECT c.*, ctS.Subbitles AS SubbitlesText FROM TableMovies c INNER JOIN TableMoviesSubbitles ctS ON c.IDSubbitles = ctS._Id ");
            */
            SQLiteCommand QueryConnectingTable = db.CreateCommand("" +
                "SELECT c.*, ctF.Format AS FormatText, ctL.Language AS LanguageText, ctQ.Quality AS QualityText, ctS.Subbitles AS SubbitlesText " +
                "FROM TableMovies c " +
                "INNER JOIN TableMoviesFormat ctF ON c.IDFormat = ctF._Id " +
                "INNER JOIN TableMoviesLanguage ctL ON c.IDLanguage = ctL._Id " +
                "INNER JOIN TableMoviesQuality ctQ ON c.IDQuality = ctQ._Id " +
                "INNER JOIN TableMoviesSubbitles ctS ON c.IDSubbitles = ctS._Id");
            
            List<TableMoviesWithConettedTable> table;
           
            table = QueryConnectingTable.ExecuteQuery<TableMoviesWithConettedTable>();
            
            return table;
        }
        public List<TableMoviesWithConettedTable> GetMoviesById(int id)
        {
            SQLiteCommand query = db.CreateCommand("" +
                            "SELECT c.*, ctF.Format AS FormatText, ctL.Language AS LanguageText, ctQ.Quality AS QualityText, ctS.Subbitles AS SubbitlesText " +
                            "FROM TableMovies c " +
                            "INNER JOIN TableMoviesFormat ctF ON c.IDFormat = ctF._Id " +
                            "INNER JOIN TableMoviesLanguage ctL ON c.IDLanguage = ctL._Id " +
                            "INNER JOIN TableMoviesQuality ctQ ON c.IDQuality = ctQ._Id " +
                            "INNER JOIN TableMoviesSubbitles ctS ON c.IDSubbitles = ctS._Id "+
                            "WHERE c._Id = ?", id);

            var table = query.ExecuteQuery<TableMoviesWithConettedTable>();

            return table;

        }

        public bool CheckIDFormatMovies_In_TableMovies(int id)
        {
            var table = db.Table<TableMovies>();

            foreach (var item in table)
            {
                if (item.IDFormat == id)
                {
                    App.LocalNotification("Nie można usunąć formatu, jest on obecnie w użyciu");
                    return true;
                }

            }
            return false;
        }
        public bool CheckIDLanguageMovies_In_TableMovies(int id)
        {
            var table = db.Table<TableMovies>();

            foreach (var item in table)
            {
                if (item.IDLanguage == id)
                {
                    App.LocalNotification("Nie można usunąć języka, jest on obecnie w użyciu");
                    return true;
                }

            }
            return false;
        }
        public bool CheckIDQualityMovies_In_TableMovies(int id)
        {
            var table = db.Table<TableMovies>();

            foreach (var item in table)
            {
                if (item.IDQuality == id)
                {
                    App.LocalNotification("Nie można usunąć jakości, jest ona obecnie w użyciu");
                    return true;
                }

            }
            return false;
        }
        public bool CheckIDSubbitlesMovies_In_TableMovies(int id)
        {
            var table = db.Table<TableMovies>();

            foreach (var item in table)
            {
                if (item.IDSubbitles == id)
                {
                    App.LocalNotification("Nie można usunąć napisów, są one obecnie w użyciu");
                    return true;
                }

            }
            return false;
        }
        public void DeleteOneMovies(int id)
        {
            var table = db.Table<TableMovies>();

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    if (App.MyLittleHomeDB_MoviesLoan.CheckIDMoviesinMoviesLoan(item.ID) == true)
                    {
                        App.MyLittleHomeDB_MoviesLoan.DeleteOneMoviesLoan(item.ID);
                    }
                }
            }
        }

        //Metoda usuwająca tablicę Films i tworząca ją na nowo (wyczyszczenie tablicy)
        public void DeleteMovies()
        {
            db.DropTable<TableMovies>();
            CreateTable();
        }
    }
}
