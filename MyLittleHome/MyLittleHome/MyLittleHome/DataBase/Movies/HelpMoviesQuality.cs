﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;

namespace MyLittleHome.DataBase
{
    public class HelpMoviesQuality
    {
        private SQLiteConnection db;

        public HelpMoviesQuality()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableMoviesQualityInfo = db.GetTableInfo("TableMoviesQuality");
                if (TableMoviesQualityInfo.Count == 0)
                {
                    db.CreateTable<TableMoviesQuality>();
                    AddBasicMoviesQuality();
                }
                Console.WriteLine("Tabela Books Format została utworzona");
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicMoviesQuality()
        {
            this.InsertMoviesQuality("1080p");
            this.InsertMoviesQuality("720p");
            this.InsertMoviesQuality("480p");
            this.InsertMoviesQuality("360p");
        }


        public bool CheckInsertMoviesQuality(string quality)
        {
            var table = db.Table<TableMoviesQuality>();


            foreach (var item in table)
            {
                if (item.Quality == quality)
                {
                    App.LocalNotification("Dana jakość filmu już istnieje!");
                    return false;
                }
            }
            return true;

        }

        public void InsertMoviesQuality(string format)
        {
            try
            {
                TableMoviesQuality items = new TableMoviesQuality();

                items.Quality = format;

                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Dictionary<string, int> GetMoviesQualityDictionary()
        {
            var table = db.Table<TableMoviesQuality>();

            Dictionary<string, int> MoviesQuality = new Dictionary<string, int>();

            foreach (var item in table)
            {
                MoviesQuality.Add(item.Quality, item.ID);
            }

            return MoviesQuality;

        }

        //Do ItemSource
        internal TableQuery<TableMoviesQuality> GetMoviesQualityItemSource()
        {
            var table = db.Table<TableMoviesQuality>();
            return table;
        }

        //Metoda pobierająca konkretny format po id
        public string GetDirectMoviesQuality(int id)
        {
            var table = db.Table<TableMoviesQuality>();

            TableMoviesQuality items = new TableMoviesQuality();

            string quality = "";

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    quality = item.Quality;
                }
            }

            return quality;

        }

        public void DeleteOneMoviesQuality(int id)
        {
            var table = db.Table<TableMoviesQuality>();

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    App.LocalNotification("Quality deleted");

                }
            }
        }

        public void DeleteMoviesQuality()
        {
            db.DropTable<TableMoviesQuality>();
            CreateTable();
        }

    }
}
