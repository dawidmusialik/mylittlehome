﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableMoviesSubbitles")]
    class TableMoviesSubbitles
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id filmu

        public string Subbitles { get; set; } //Format filmu

    }
}
