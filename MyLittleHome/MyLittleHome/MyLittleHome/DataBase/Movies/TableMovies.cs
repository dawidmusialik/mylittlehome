﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableMovies")]
    public class TableMovies
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id filmu

        public string Title { get; set; } //Tytuł

        public DateTime PublicationDate { get; set; } //Rok produkcji 

        public int IDFormat { get; set; } // DVD - RMVB - x264 itd.

        public int IDQuality { get; set; } //360p - 480p - 720p itd.

        public int IDLanguage { get; set; } //PL - ENG idp.

        public int IDSubbitles { get; set; } //Tak - Nie

        public string StorageDevice { get; set; } // Pamięć-Nośnik na którym znajduje się film

        public string Description { get; set; } //Opis

        public bool Loan { get; set; }

        private string LoanText;
        public string LoanToSting
        {
            get { return LoanText; }
            set { LoanText = Loan ? "Yes" : "No"; }
        }

    }

    public class TableMoviesWithConettedTable: TableMovies
    {
        public string FormatText { get; set; }
        public string QualityText { get; set; }
        public string LanguageText { get; set; }
        public string SubbitlesText { get; set; }
    }

}


