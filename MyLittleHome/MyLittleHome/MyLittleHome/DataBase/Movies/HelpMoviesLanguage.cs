﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;

namespace MyLittleHome.DataBase
{
    public class HelpMoviesLanguage
    {
        private SQLiteConnection db;

        public HelpMoviesLanguage()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableMoviesLanguageInfo = db.GetTableInfo("TableMoviesLanguage");
                if (TableMoviesLanguageInfo.Count == 0)
                {
                    db.CreateTable<TableMoviesLanguage>();
                    AddBasicMoviesLanguage ();
                }
                Console.WriteLine("Tabela Movie Language została utworzona");
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicMoviesLanguage()
        {
            this.InsertMoviesLanguage("Polish");
            this.InsertMoviesLanguage("English");
            this.InsertMoviesLanguage("Deutsch");
      
        }

        public bool CheckInsertMoviesLanguage(string language)
        {
            var table = db.Table<TableMoviesLanguage>();


            foreach (var item in table)
            {
                if (item.Language == language)
                {
                    App.LocalNotification("Dany język filmu już istnieje!");
                    return false;
                }
            }
            return true;

        }

        public void InsertMoviesLanguage(string format)
        {
            try
            {
                TableMoviesLanguage items = new TableMoviesLanguage();

                items.Language = format;

                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Dictionary<string, int> GetMoviesLanguageDictionary()
        {
            var table = db.Table<TableMoviesLanguage>();

            Dictionary<string, int> MoviesLanguage = new Dictionary<string, int>();

            foreach (var item in table)
            {
                MoviesLanguage.Add(item.Language, item.ID);
            }

            return MoviesLanguage;

        }

        //Do ItemSource
        internal TableQuery<TableMoviesLanguage> GetMoviesLanguageItemSource()
        {
            var table = db.Table<TableMoviesLanguage>();
            return table;
        }

        //Metoda pobierająca konkretny format po id
        public string GetDirectMoviesLanguage(int id)
        {
            var table = db.Table<TableMoviesLanguage>();

            TableMoviesLanguage items = new TableMoviesLanguage();

            string language = "";

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    language = item.Language;
                }
            }

            return language;

        }

        public void DeleteOneMoviesLanguage(int id)
        {
            var table = db.Table<TableMoviesLanguage>();

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    App.LocalNotification("Language deleted");

                }
            }
        }

        public void DeleteMoviesLanguage()
        {
            db.DropTable<TableMoviesLanguage>();
            CreateTable();
        }

    }
}
