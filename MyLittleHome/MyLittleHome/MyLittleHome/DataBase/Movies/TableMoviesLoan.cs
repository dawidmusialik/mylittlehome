﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableMoviesLoan")]
    public class TableMoviesLoan
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id książki i wypożyczenia
        public int IDMovies { get; set; }
        public DateTime DateLoan { get; set; } //Data pożyczenia
        public DateTime DateReceipt { get; set; }//Data zwrotu
        public string Who { get; set; } //Komu książka została wypożyczona
    }
}
