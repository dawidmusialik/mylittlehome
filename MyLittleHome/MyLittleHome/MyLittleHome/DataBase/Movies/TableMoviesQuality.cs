﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLite.Net.Attributes;

namespace MyLittleHome.DataBase
{
    [Table("TableMoviesQuality")]
    class TableMoviesQuality
    {
        [PrimaryKey, AutoIncrement, Column("_Id")]
        public int ID { get; set; } //Id jakości

        public string Quality { get; set; } //Jakość ksiązki

    }
}
