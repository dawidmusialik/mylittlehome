﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;

namespace MyLittleHome.DataBase
{
    public class HelpMoviesSubbitles
    {
        private SQLiteConnection db;

        public HelpMoviesSubbitles()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableMoviesSubbitlesInfo = db.GetTableInfo("TableMoviesSubbitles");
                if (TableMoviesSubbitlesInfo.Count == 0)
                { 
                    db.CreateTable<TableMoviesSubbitles>();
                    AddBasicMoviesSubbitles();
                }
                Console.WriteLine("Tabela Books Format została utworzona");
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicMoviesSubbitles()
        {
            this.InsertMoviesSubbitles("None");
            this.InsertMoviesSubbitles("Polish");
            this.InsertMoviesSubbitles("English");
            this.InsertMoviesSubbitles("Deutsch");
        }

        public bool CheckInsertMoviesSubbitles(string subbitles)
        {
            var table = db.Table<TableMoviesSubbitles>();


            foreach (var item in table)
            {
                if (item.Subbitles == subbitles)
                {
                    App.LocalNotification("Dane napisy filmu już istnieje!");
                    return false;
                }
            }
            return true;

        }

        public void InsertMoviesSubbitles(string format)
        {
            try
            {
                TableMoviesSubbitles items = new TableMoviesSubbitles();

                items.Subbitles = format;

                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Dictionary<string, int> GetMoviesFormatSubbitles()
        {
            var table = db.Table<TableMoviesSubbitles>();

            Dictionary<string, int> MoviesSubbitles = new Dictionary<string, int>();

            foreach (var item in table)
            {
                MoviesSubbitles.Add(item.Subbitles, item.ID);
            }

            return MoviesSubbitles;

        }

        //Do ItemSource
        internal TableQuery<TableMoviesSubbitles> GetMoviesSubbitlesItemSource()
        {
            var table = db.Table<TableMoviesSubbitles>();
            return table;
        }

        //Metoda pobierająca konkretny format po id
        public string GetDirectMoviesSubbitles(int id)
        {
            var table = db.Table<TableMoviesSubbitles>();

            TableMoviesSubbitles items = new TableMoviesSubbitles();

            string format = "";

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    format = item.Subbitles;
                }
            }

            return format;

        }

        public void DeleteOneMoviesSubbitles(int id)
        {
            var table = db.Table<TableMoviesSubbitles>();

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    App.LocalNotification("Subbitles deleted");

                }
            }
        }

        public void DeleteMoviesSubbitles()
        {
            db.DropTable<TableMoviesSubbitles>();
            CreateTable();
        }

    }
}
