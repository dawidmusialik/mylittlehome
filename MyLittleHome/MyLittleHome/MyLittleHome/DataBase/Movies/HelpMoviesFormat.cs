﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;

namespace MyLittleHome.DataBase
{
    public class HelpMoviesFormat
    {
        private SQLiteConnection db;

        public HelpMoviesFormat()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnectionNet();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {
                var TableMoviesFormatInfo = db.GetTableInfo("TableMoviesFormat");
                if (TableMoviesFormatInfo.Count == 0)
                {
                    db.CreateTable<TableMoviesFormat>();
                    AddBasicMoviesFormat();
                }
                Console.WriteLine("Tabela Books Format została utworzona");
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void AddBasicMoviesFormat()
        {
            this.InsertMoviesFormat("x264");
            this.InsertMoviesFormat("x265");
            this.InsertMoviesFormat("DVD");
            this.InsertMoviesFormat("BluRay");
            this.InsertMoviesFormat("RMVB");
            this.InsertMoviesFormat("Divx");
        }

        public bool CheckInsertMoviesFormat(string format)
        {
            var table = db.Table<TableMoviesFormat>();


            foreach (var item in table)
            {
                if (item.Format == format)
                {
                    App.LocalNotification("This format is exist!");
                    return false;
                }
            }
            return true;

        }

        public void InsertMoviesFormat(string format)
        {
            try
            {
                TableMoviesFormat items = new TableMoviesFormat();

                items.Format = format;

                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Dictionary<string, int> GetMoviesFormatDictionary()
        {
            var table = db.Table<TableMoviesFormat>();

            Dictionary<string, int> MoviesFormat = new Dictionary<string, int>();

            foreach (var item in table)
            {
                MoviesFormat.Add(item.Format, item.ID);
            }

            return MoviesFormat;

        }

        //Do ItemSource
        internal TableQuery<TableMoviesFormat> GetMoviesFormatItemSource()
        {
            var table = db.Table<TableMoviesFormat>();
            return table;
        }

        //Metoda pobierająca konkretny format po id
        public string GetDirectMoviesFormat(int id)
        {
            var table = db.Table<TableMoviesFormat>();

            TableMoviesFormat items = new TableMoviesFormat();

            string format = "";

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    format = item.Format;
                }
            }

            return format;

        }

        public void DeleteOneMoviesFormat(int id)
        {
            var table = db.Table<TableMoviesFormat>();

            foreach (var item in table)
            {
                if (item.ID == id)
                {
                    db.Delete(item);
                    App.LocalNotification("Format deleted");

                }
            }
        }

        public void DeleteMoviesFormat()
        {
            db.DropTable<TableMoviesFormat>();
            CreateTable();
        }

    }
}
