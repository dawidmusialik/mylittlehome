﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.IO;
using Xamarin.Forms;

namespace MyLittleHome.DataBase
{
    public class HelpMovies
    {
        private SQLiteConnection db;

        public HelpMovies()
        {
            db = DataBaseConnectionSingleton.Instance.GetConnection();
            CreateTable();
        }

        public void CreateTable()
        {
            try
            {

                var TableMoviestInfo = db.GetTableInfo("TableMovies");
                if (TableMoviestInfo.Count == 0) { db.CreateTable<TableMovies>(); }
                Console.WriteLine("Tabela Movies została utworzona");

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


        //Metoda umieszczająca nową pozycję filmu w bazie danych
        public void InsertMovie(string title, int publicationdate, string format,string quality, string language, string subbitles, string storagedevice, string description)
        {
            try
            {
                TableMovies items = new TableMovies();

                items.Title = title;
                items.PublicationDate = publicationdate;
                items.Format = format;
                items.Quality = quality;
                items.Language = language;
                items.Subbitles = subbitles;
                items.StorageDevice = storagedevice;
                items.Description = description;

                db.Insert(items);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //Metoda pobierająca wszystkie filmy z bazy danych
        public TableQuery<TableMovies> GetMovies()
        {
            var table = db.Table<TableMovies>();
            return table;
        }


        //Metoda usuwająca tablicę Films i tworząca ją na nowo (wyczyszczenie tablicy)
        public void DeleteMovies()
        {
            db.DropTable<TableMovies>();
            CreateTable();
        }
    }
}
