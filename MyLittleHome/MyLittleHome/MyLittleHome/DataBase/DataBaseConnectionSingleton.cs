﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net;
using System.IO;
using SQLite.Net.Interop;

namespace MyLittleHome.DataBase
{
    //Test
    public sealed class DataBaseConnectionSingleton {

        private static DataBaseConnectionSingleton _Instance = null;
        private static readonly object PadLock = new object();
        private SQLite.Net.SQLiteConnection DataBaseConnectionNet;
        private ISQLitePlatform DatabaseNetPlatform;

        private DataBaseConnectionSingleton() {

            //SQLite-Net PCL
            DatabaseNetPlatform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
            DataBaseConnectionNet = new SQLite.Net.SQLiteConnection(DatabaseNetPlatform, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "database.db3"));
        }

        public static DataBaseConnectionSingleton Instance {
            get {
                lock (PadLock) {
                    if (_Instance == null) {
                        _Instance = new DataBaseConnectionSingleton();
                    }
                    return _Instance;
                }
            }
        }
        public SQLite.Net.SQLiteConnection GetConnectionNet()
        {
            return DataBaseConnectionNet;
        }

    }
}
